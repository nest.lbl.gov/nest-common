#!/bin/sh
#
# This is a test script that runs asynchronously in Bethany and ends successfully.
#
# usage: ${0} input output
#
# input  : A file containing key-value pairs that are the inputs to this script
# output : A file into which to write key-value pairs that are the outputs of this script.
#

if [ "X" == "X${1}" ] ; then
  echo "Inputs file is missing" >&2
  exit 1
fi

if [ "X" == "X${2}" ] ; then
  echo "Outputs file is missing" >&2
  exit 2
fi

echo "-- Begin input file --"
while IFS='' read -r line || [[ -n "$line" ]] ; do
    echo "Text read from file: $line"
done < "$1"
echo "-- End input file --"

echo "Failed by design" >&2

exit 1
