package gov.lbl.nest.common.external;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.testing.UnitTestUtility;

/**
 * This class test that a {@link Command} instance fulfills its requirements.
 *
 * @author patton
 */
@DisplayName("Command class")
public class CommandTests {

    /**
     * The name of the resource contains example content for files.
     */
    private static final String EXAMPLE_RESOURCE = "example.content";

    /**
     * File used as input to external commands.
     */
    private static final File INPUT_FILE = new File(EXAMPLE_RESOURCE);

    /**
     * File used as input to external commands.
     */
    private static final File OUTPUT_FILE = new File("output.txt");

    /**
     * The array for files used in tests.
     */
    private static final File[] FILES = new File[] { INPUT_FILE,
                                                     OUTPUT_FILE };

    /**
     * Test that a {@link Command} executes correctly when provided with an
     * {@link File} for stderr.
     *
     * @throws InterruptedException
     *             when test is interrupted
     * @throws IOException
     *             when an IO issue is found
     */
    @Test
    @DisplayName("Error Execution")
    void errorExecution() throws IOException,
                          InterruptedException {
        // TODO : Think up a meaningful test.
    }

    /**
     * Test that a {@link Command} executes correctly when provided with an
     * {@link File} for stdin.
     *
     * @throws InterruptedException
     *             when test is interrupted
     * @throws IOException
     *             when an IO issue is found
     */
    @Test
    @DisplayName("Input Execution")
    void inputExecution() throws IOException,
                          InterruptedException {
        final Command instanceToTest = new Command(new String[] { "tee",
                                                                  OUTPUT_FILE.getName() });
        UnitTestUtility.createFileFromResource(getClass(),
                                               INPUT_FILE);
        final int result = instanceToTest.execute(null,
                                                  null,
                                                  INPUT_FILE);
        assertEquals(0,
                     result,
                     "External command did not succeed");
        assertTrue(OUTPUT_FILE.exists(),
                   "External command failed to execute correctly");
        assertTrue(UnitTestUtility.identical(getClass(),
                                             EXAMPLE_RESOURCE,
                                             OUTPUT_FILE),
                   "External command failed to execute correctly");
    }

    /**
     * Test that a {@link Command} executes correctly when provided with an
     * {@link File} for stdout.
     *
     * @throws InterruptedException
     *             when test is interrupted
     * @throws IOException
     *             when an IO issue is found
     */
    @Test
    @DisplayName("Output Execution")
    void outputExecution() throws IOException,
                           InterruptedException {
        UnitTestUtility.createFileFromResource(getClass(),
                                               INPUT_FILE);
        final Command instanceToTest = new Command(new String[] { "cat",
                                                                  INPUT_FILE.getName() });
        final int result = instanceToTest.execute(OUTPUT_FILE);
        assertEquals(0,
                     result,
                     "External command did not succeed");
        assertTrue(OUTPUT_FILE.exists(),
                   "External command failed to execute correctly");
        assertTrue(UnitTestUtility.identical(getClass(),
                                             EXAMPLE_RESOURCE,
                                             OUTPUT_FILE),
                   "External command failed to execute correctly");
    }

    @BeforeEach
    void setUp() throws Exception {
        UnitTestUtility.deleteFiles(FILES);
    }

    /**
     * Test that a {@link Command} executes correctly when provided with no direct
     * input and outputs.
     *
     * @throws InterruptedException
     *             when test is interrupted
     * @throws IOException
     *             when an IO issue is found
     */
    @Test
    @DisplayName("Simple Execution")
    void simpleExecution() throws IOException,
                           InterruptedException {
        final Command instanceToTest = new Command(new String[] { "touch",
                                                                  OUTPUT_FILE.getName() });
        final int result = instanceToTest.execute();
        assertEquals(0,
                     result,
                     "External command did not succeed");
        assertTrue(OUTPUT_FILE.exists(),
                   "External command failed to execute correctly");
    }

    /**
     * Test that a {@link Command} fails correctly when provided with no direct
     * input and outputs.
     *
     * @throws InterruptedException
     *             when test is interrupted
     */
    @Test
    @DisplayName("Simple Failure")
    void simpleFailure() throws InterruptedException {
        // "touch" is intentionally spelt incorrectly in order to create a
        // failure.
        final Command instanceToTest = new Command(new String[] { "toch",
                                                                  OUTPUT_FILE.getName() });
        try {
            instanceToTest.execute();
            fail("External command did not throw expected exception");
        } catch (IOException e) {
            // Expected so do nothing
        }
    }

    /**
     * Test that when a {@link Command} executes correctly that the stderr content
     * is correct.
     *
     * @throws InterruptedException
     *             when test is interrupted
     * @throws IOException
     *             when an IO issue is found
     */
    @Test
    @DisplayName("StdErr Output")
    void stdErrOutput() throws IOException,
                        InterruptedException {
        // Need to think of good test.
    }

    /**
     * Test that when a {@link Command} executes correctly that the stdout content
     * is correct.
     *
     * @throws InterruptedException
     *             when test is interrupted
     * @throws IOException
     *             when an IO issue is found
     */
    @Test
    @DisplayName("StdOut Output")
    void stdOutOutput() throws IOException,
                        InterruptedException {
        UnitTestUtility.createFileFromResource(getClass(),
                                               INPUT_FILE);
        final Command instanceToTest = new Command(new String[] { "cat",
                                                                  INPUT_FILE.getName() });
        final int result = instanceToTest.execute();
        assertEquals(0,
                     result,
                     "External command did not succeed");
        assertTrue(UnitTestUtility.identical(getClass(),
                                             EXAMPLE_RESOURCE,
                                             instanceToTest.getStdOut()),
                   "External command failed to execute correctly");
    }

    @AfterEach
    void tearDown() throws Exception {
        UnitTestUtility.deleteFiles(FILES);
    }

}
