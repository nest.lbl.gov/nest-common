package gov.lbl.nest.common.queued;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.util.Date;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * This class defines the common tests that any {@link QueueableImpl} class or
 * subclass must pass.
 *
 * @author patton
 */
public class QueueableImplTests {

    /**
     * The {@link QueueableImpl} instance being tested.
     */
    QueueableImpl testInstance;

    @Test
    @DisplayName("Construction")
    void construction() {
        final long windowOpen = new Date().getTime();
        testInstance = getTestInstance();
        final long windowClosed = new Date().getTime();
        assertNotNull(testInstance);
        final long whenCreated = (testInstance.getWhenCreated()).getTime();
        assertTrue(windowOpen <= whenCreated && whenCreated <= windowClosed);
    }

    @Test
    @DisplayName("Durations")
    void durations() {
        finished();
        final long whenCreated = (testInstance.getWhenCreated()).getTime();
        final long whenStarted = (testInstance.getWhenStarted()).getTime();
        final long whenFinished = (testInstance.getWhenFinished()).getTime();
        final Duration executionDuration = testInstance.getExecutionDuration();
        assertEquals(Duration.ofMillis(whenFinished - whenStarted),
                     executionDuration);
        final Duration totalDuration = testInstance.getTotalDuration();
        assertEquals(Duration.ofMillis(whenFinished - whenCreated),
                     totalDuration);
        final Duration waitDuration = testInstance.getWaitDuration();
        assertEquals(Duration.ofMillis(whenStarted - whenCreated),
                     waitDuration);
    }

    @Test
    @DisplayName("Finished")
    void finished() {
        started();
        final long windowOpen = new Date().getTime();
        testInstance.setWhenFinished(new Date());
        final long windowClosed = new Date().getTime();
        assertNotNull(testInstance);
        final long whenFinished = (testInstance.getWhenFinished()).getTime();
        assertTrue(windowOpen <= whenFinished && whenFinished <= windowClosed);
    }

    /**
     * Returns the {@link QueueableImpl} instance to be tested.
     *
     * @return the {@link QueueableImpl} instance to be tested.
     */
    protected QueueableImpl getTestInstance() {
        return new QueueableImpl();
    }

    @Test
    @DisplayName("Started")
    void started() {
        construction();
        final long windowOpen = new Date().getTime();
        testInstance.setWhenStarted(new Date());
        final long windowClosed = new Date().getTime();
        assertNotNull(testInstance);
        final long whenStarted = (testInstance.getWhenStarted()).getTime();
        assertTrue(windowOpen <= whenStarted && whenStarted <= windowClosed);
    }
}
