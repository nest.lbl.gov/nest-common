package gov.lbl.nest.common.xml.item;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXParseException;

import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.common.watching.Digest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;

/**
 * This class test that an {@link Item} instance fulfills its requirements.
 *
 * @author patton
 */
@DisplayName("Item class")
public class ItemTests {

    /**
     * The format for date and time queries.
     */
    private static final String DATE_AND_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    private static final DateFormat DATE_AND_TIME_FORMATTER = new SimpleDateFormat(DATE_AND_TIME_FORMAT);

    /**
     * The name of the file contains an example date item.
     */
    private static final File DATE_SINGLE_FILE = new File("date.xml");

    /**
     * The name of the resource contains an example date item.
     */
    private static final String DATE_SINGLE_NAME = "date_item";

    /**
     * The name of the resource contains an example date item.
     */
    private static final String DATE_SINGLE_NAME_1 = "date_item_1";

    /**
     * The name of the resource contains an example date item.
     */
    private static final String DATE_SINGLE_RESOURCE = "date.xml";

    /**
     * The {@link Date} instance that should be read from the resource.
     */
    private static final Date TEST_DATE;

    /**
     * The {@link Date} instance that should be read from the resource.
     */
    private static final Date TEST_DATE_1;

    static {
        Date testDate;
        Date testDate1;
        try {
            testDate = DATE_AND_TIME_FORMATTER.parse("2021-12-22T23:53:10Z");
            testDate1 = DATE_AND_TIME_FORMATTER.parse("2021-12-22T23:54:34Z");
        } catch (ParseException e) {
            testDate = null;
            testDate1 = null;
            e.printStackTrace();
        }
        TEST_DATE = testDate;
        TEST_DATE_1 = testDate1;
    }

    /**
     * The name of the file contains an example date item.
     */
    private static final File DATE_ARRAY_FILE = new File("date_array.xml");

    /**
     * The name of the resource contains an example date item.
     */
    private static final String DATE_ARRAY_NAME = "date_array";

    /**
     * The name of the resource contains an example date item.
     */
    private static final String DATE_ARRAY_RESOURCE = "date_array.xml";

    /**
     * The name of the file contains an example date item.
     */
    private static final File DATE_DICTIONARY_FILE = new File("date_dictionary.xml");

    /**
     * The name of the resource contains an example date item.
     */
    private static final String DATE_DICTIONARY_NAME = "date_dictionary";

    /**
     * The name of the resource contains an example date item.
     */
    private static final String DATE_DICTIONARY_RESOURCE = "date_dictionary.xml";

    /**
     * The name of the resource contains an example date item.
     */
    private static final String DATE_DICTIONARY_RESOURCE_ALTERNATE = "date_dictionary_alternate.xml";

    /**
     * The name of the file contains an example string item.
     */
    private static final File STRING_SINGLE_FILE = new File("string.xml");

    /**
     * The name of the file contains an example string item.
     */
    private static final String STRING_SINGLE_NAME = "string_item";

    /**
     * The name of the file contains an example string item.
     */
    private static final String STRING_SINGLE_NAME_1 = "string_item_1";

    /**
     * The name of the resource contains an example string item.
     */
    private static final String STRING_SINGLE_RESOURCE = "string.xml";

    /**
     * The name of the file contains an example string item.
     */
    private static final File STRING_ARRAY_FILE = new File("string_array.xml");

    /**
     * The name of the file contains an example string item.
     */
    private static final String STRING_ARRAY_NAME = "string_array";

    /**
     * The name of the resource contains an example string item.
     */
    private static final String STRING_ARRAY_RESOURCE = "string_array.xml";

    /**
     * The name of the file contains an example string item.
     */
    private static final File STRING_DICTIONARY_FILE = new File("string_dictionary.xml");

    /**
     * The name of the resource contains an example string item.
     */
    private static final String STRING_DICTIONARY_NAME = "string_dictionary";

    /**
     * The name of the resource contains an example string item.
     */
    private static final String STRING_DICTIONARY_RESOURCE = "string_dictionary.xml";

    /**
     * The name of the resource contains an example string item.
     */
    private static final String STRING_DICTIONARY_RESOURCE_ALTERNATE = "string_dictionary_alternate.xml";

    /**
     * The {@link String} instance that should be read from the resource.
     */
    private static final String TEST_STRING = "This is a String";

    /**
     * The {@link String} instance that should be read from the resource.
     */
    private static final String TEST_STRING_1 = "This is another String";

    /**
     * The array for files used in tests.
     */
    private static final File[] FILES = new File[] { DATE_SINGLE_FILE,
                                                     DATE_ARRAY_FILE,
                                                     DATE_DICTIONARY_FILE,
                                                     STRING_SINGLE_FILE,
                                                     STRING_ARRAY_FILE,
                                                     STRING_DICTIONARY_FILE };

    /**
     * Creates a {@link Items} from the specified {@link InputStream}.
     *
     * @param inputStream
     *            the {@link InputStream} from which the {@link Items} should be
     *            created.
     *
     * @return the {@link Items} created from the specified {@link InputStream}.
     *
     * @throws ItemException
     *             when the {@link InputStream} does not contain a {@link Digest}
     *             instance.
     */
    private static Item readItem(InputStream inputStream) throws ItemException {
        try {
            JAXBContext content = JAXBContext.newInstance(Item.class);
            Unmarshaller unmarshaller = content.createUnmarshaller();
            return (Item) unmarshaller.unmarshal(inputStream);
        } catch (UnmarshalException e) {
            final Throwable cause = e.getCause();
            if (null != cause && cause instanceof SAXParseException) {
                // Means that a Digest was not returned.
                return null;
            }
            throw new ItemException(e);
        } catch (JAXBException e) {
            throw new ItemException(e);
        }
    }

    /**
     * Test that an {@link Item} containing a {@link Date} instance can be read
     * correctly.
     *
     * @throws ItemException
     *             when the {@link InputStream} does not contain a {@link Item}
     *             instance.
     * @throws FileNotFoundException
     *             when the test resource does not exist.
     */
    @Test
    @DisplayName("Read Date")
    public void readDate() throws FileNotFoundException,
                           ItemException {
        final Item item = readItem(UnitTestUtility.getResourceAsStream(getClass(),
                                                                       DATE_SINGLE_RESOURCE));
        assertEquals(DATE_SINGLE_NAME,
                     item.getName());
        assertEquals(TEST_DATE,
                     item.getValue(),
                     "Date Item did not read the correct value");
    }

    /**
     * Test that an {@link Item} containing a {@link Date} instance can be read
     * correctly.
     *
     * @throws ItemException
     *             when the {@link InputStream} does not contain a {@link Item}
     *             instance.
     * @throws FileNotFoundException
     *             when the test resource does not exist.
     */
    @Test
    @DisplayName("Read Date Array")
    public void readDateArray() throws FileNotFoundException,
                                ItemException {
        final Item item = readItem(UnitTestUtility.getResourceAsStream(getClass(),
                                                                       DATE_ARRAY_RESOURCE));
        assertEquals(DATE_ARRAY_NAME,
                     item.getName());
        @SuppressWarnings("unchecked")
        final List<Date> dates = (List<Date>) item.getValue();
        final List<Date> expected = new ArrayList<>();
        expected.add(TEST_DATE);
        expected.add(TEST_DATE_1);
        assertEquals(expected.size(),
                     dates.size());
        final Iterator<Date> expectedDates = expected.iterator();
        for (Date date : dates) {
            assertEquals(expectedDates.next(),
                         date,
                         "Date array did not read the correct value");
        }
    }

    /**
     * Test that an {@link Item} containing named {@link Date} instances can be read
     * correctly.
     *
     * @throws ItemException
     *             when the {@link InputStream} does not contain a {@link Item}
     *             instance.
     * @throws FileNotFoundException
     *             when the test resource does not exist.
     */
    @Test
    @DisplayName("Read Date Dictionary")
    public void readDateDictionary() throws FileNotFoundException,
                                     ItemException {
        final Item item = readItem(UnitTestUtility.getResourceAsStream(getClass(),
                                                                       DATE_DICTIONARY_RESOURCE));
        assertEquals(DATE_DICTIONARY_NAME,
                     item.getName());
        final Object value = item.getValue();
        assertTrue(value instanceof Map<?, ?>);
        @SuppressWarnings("unchecked")
        final Map<String, Object> dates = (Map<String, Object>) value;
        final Map<String, Date> expected = new HashMap<>();
        expected.put("date_item",
                     TEST_DATE);
        expected.put("date_item_1",
                     TEST_DATE_1);
        assertEquals(expected.size(),
                     dates.size());
        for (String key : dates.keySet()) {
            assertEquals(expected.get(key),
                         dates.get(key),
                         "Date dictionary did not read the correct values");
        }
    }

    /**
     * Test that an {@link Item} containing a {@link String} instance can be read
     * correctly.
     *
     * @throws ItemException
     *             when the {@link InputStream} does not contain a {@link Item}
     *             instance.
     * @throws FileNotFoundException
     *             when the test resource does not exist.
     */
    @Test
    @DisplayName("Read String")
    public void readString() throws FileNotFoundException,
                             ItemException {
        final Item item = readItem(UnitTestUtility.getResourceAsStream(getClass(),
                                                                       STRING_SINGLE_RESOURCE));
        assertEquals(STRING_SINGLE_NAME,
                     item.getName());
        assertEquals(TEST_STRING,
                     item.getValue(),
                     "String Item did not read the correct value");
    }

    /**
     * Test that an {@link Item} containing a {@link String} instance can be read
     * correctly.
     *
     * @throws ItemException
     *             when the {@link InputStream} does not contain a {@link Item}
     *             instance.
     * @throws FileNotFoundException
     *             when the test resource does not exist.
     */
    @Test
    @DisplayName("Read String Array")
    public void readStringArray() throws FileNotFoundException,
                                  ItemException {
        final Item item = readItem(UnitTestUtility.getResourceAsStream(getClass(),
                                                                       STRING_ARRAY_RESOURCE));
        assertEquals(STRING_ARRAY_NAME,
                     item.getName());
        @SuppressWarnings("unchecked")
        final List<String> strings = (List<String>) item.getValue();
        final List<String> expected = new ArrayList<>();
        expected.add(TEST_STRING);
        expected.add(TEST_STRING_1);
        assertEquals(expected.size(),
                     strings.size());
        final Iterator<String> expectedStrings = expected.iterator();
        for (String date : strings) {
            assertEquals(expectedStrings.next(),
                         date,
                         "String array did not read the correct value");
        }
    }

    /**
     * Test that an {@link Item} containing named {@link String} instances can be
     * read correctly.
     *
     * @throws ItemException
     *             when the {@link InputStream} does not contain a {@link Item}
     *             instance.
     * @throws FileNotFoundException
     *             when the test resource does not exist.
     */
    @Test
    @DisplayName("Read String Dictionary")
    public void readStringDictionary() throws FileNotFoundException,
                                       ItemException {
        final Item item = readItem(UnitTestUtility.getResourceAsStream(getClass(),
                                                                       STRING_DICTIONARY_RESOURCE));
        assertEquals(STRING_DICTIONARY_NAME,
                     item.getName());
        final Object value = item.getValue();
        assertTrue(value instanceof Map<?, ?>);
        @SuppressWarnings("unchecked")
        final Map<String, Object> strings = (Map<String, Object>) value;
        final Map<String, String> expected = new HashMap<>();
        expected.put("string_item",
                     TEST_STRING);
        expected.put("string_item_1",
                     TEST_STRING_1);
        assertEquals(expected.size(),
                     strings.size());
        for (String key : strings.keySet()) {
            assertEquals(expected.get(key),
                         strings.get(key),
                         "String dictionary did not read the correct values");
        }
    }

    @BeforeEach
    void setUp() throws Exception {
        UnitTestUtility.deleteFiles(FILES);
        // Reference files are all in PST, so use that timezone for the tests
        TimeZone.setDefault(TimeZone.getTimeZone("Z"));
    }

    @AfterEach
    void tearDown() throws Exception {
        UnitTestUtility.deleteFiles(FILES);
    }

    /**
     * Test that a {@link Item} instance containing a {@link Date} can be written to
     * a file.
     *
     * @throws IOException
     *             when the file can not be written.
     * @throws ItemException
     *             when the {@link File} does not contain the expected {@link Item}
     *             instance.
     */
    @Test
    @DisplayName("Write Date")
    public void writeDate() throws IOException,
                            ItemException {
        writeItem(new Item(DATE_SINGLE_NAME,
                           TEST_DATE),
                  new FileWriter(DATE_SINGLE_FILE));
        assertTrue(UnitTestUtility.identical(Items.class,
                                             DATE_SINGLE_RESOURCE,
                                             DATE_SINGLE_FILE),
                   "Date Item did not write the correct value");
    }

    /**
     * Test that a {@link Item} instance containing an array of {@link Date}
     * instance can be written to a file.
     *
     * @throws IOException
     *             when the file can not be written.
     * @throws ItemException
     *             when the {@link File} does not contain the expected {@link Item}
     *             instance.
     */
    @Test
    @DisplayName("Write Date Array")
    public void writeDateArray() throws IOException,
                                 ItemException {
        final List<Date> array = new ArrayList<>(2);
        array.add(TEST_DATE);
        array.add(TEST_DATE_1);
        writeItem(new Item(DATE_ARRAY_NAME,
                           array),
                  new FileWriter(DATE_ARRAY_FILE));
        assertTrue(UnitTestUtility.identical(Items.class,
                                             DATE_ARRAY_RESOURCE,
                                             DATE_ARRAY_FILE),
                   "Date array did not write the correct value");
    }

    /**
     * Test that a {@link Item} instance containing a dictionary of {@link Date}
     * instance can be written to a file.
     *
     * @throws IOException
     *             when the file can not be written.
     * @throws ItemException
     *             when the {@link File} does not contain the expected {@link Item}
     *             instance.
     */
    @Test
    @DisplayName("Write Date Dictionary")
    public void writeDateDictionary() throws IOException,
                                      ItemException {
        final Map<String, Date> map = new HashMap<>(2);
        map.put(DATE_SINGLE_NAME,
                TEST_DATE);
        map.put(DATE_SINGLE_NAME_1,
                TEST_DATE_1);
        writeItem(new Item(DATE_DICTIONARY_NAME,
                           map),
                  new FileWriter(DATE_DICTIONARY_FILE));
        assertTrue(UnitTestUtility.identical(Items.class,
                                             DATE_DICTIONARY_RESOURCE,
                                             DATE_DICTIONARY_FILE)
                   || UnitTestUtility.identical(Items.class,
                                                DATE_DICTIONARY_RESOURCE_ALTERNATE,
                                                DATE_DICTIONARY_FILE),
                   "Date dictionary did not write the correct value");
    }

    /**
     * Writes this object out to the specified {@link Writer}.
     *
     * @param items
     *            the {@link Items} instance to be written.
     * @param writer
     *            the {@link Writer}into which to write this object.
     *
     * @throws ItemException
     *             when the {@link Digest} can not be written.
     */
    private void writeItem(Item item,
                           Writer writer) throws ItemException {
        try {
            JAXBContext context = JAXBContext.newInstance(item.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                                   true);
            marshaller.marshal(item,
                               writer);
        } catch (JAXBException e) {
            throw new ItemException(e);
        }
    }

    /**
     * Test that a {@link Item} instance containing a {@link String} can be written
     * to a file.
     *
     * @throws IOException
     *             when the file can not be written.
     * @throws ItemException
     *             when the {@link File} does not contain the expected {@link Item}
     *             instance.
     */
    @Test
    @DisplayName("Write String")
    public void writeString() throws IOException,
                              ItemException {
        writeItem(new Item(STRING_SINGLE_NAME,
                           TEST_STRING),
                  new FileWriter(STRING_SINGLE_FILE));
        assertTrue(UnitTestUtility.identical(Items.class,
                                             STRING_SINGLE_RESOURCE,
                                             STRING_SINGLE_FILE),
                   "String Item did not write the correct value");
    }

    /**
     * Test that a {@link Item} instance containing an array of {@link String}
     * instance can be written to a file.
     *
     * @throws IOException
     *             when the file can not be written.
     * @throws ItemException
     *             when the {@link File} does not contain the expected {@link Item}
     *             instance.
     */
    @Test
    @DisplayName("Write String Array")
    public void writeStringArray() throws IOException,
                                   ItemException {
        final List<String> array = new ArrayList<>(2);
        array.add(TEST_STRING);
        array.add(TEST_STRING_1);
        writeItem(new Item(STRING_ARRAY_NAME,
                           array),
                  new FileWriter(STRING_ARRAY_FILE));
        assertTrue(UnitTestUtility.identical(Items.class,
                                             STRING_ARRAY_RESOURCE,
                                             STRING_ARRAY_FILE),
                   "String Array did not write the correct value");
    }

    /**
     * Test that a {@link Item} instance containing a dictionary of {@link String}
     * instance can be written to a file.
     *
     * @throws IOException
     *             when the file can not be written.
     * @throws ItemException
     *             when the {@link File} does not contain the expected {@link Item}
     *             instance.
     */
    @Test
    @DisplayName("Write String Dictionary")
    public void writeStringDictionary() throws IOException,
                                        ItemException {
        final Map<String, String> map = new HashMap<>(2);
        map.put(STRING_SINGLE_NAME,
                TEST_STRING);
        map.put(STRING_SINGLE_NAME_1,
                TEST_STRING_1);
        writeItem(new Item(STRING_DICTIONARY_NAME,
                           map),
                  new FileWriter(STRING_DICTIONARY_FILE));
        assertTrue(UnitTestUtility.identical(Items.class,
                                             STRING_DICTIONARY_RESOURCE,
                                             STRING_DICTIONARY_FILE)
                   || UnitTestUtility.identical(Items.class,
                                                STRING_DICTIONARY_RESOURCE_ALTERNATE,
                                                STRING_DICTIONARY_FILE),
                   "String dictionary did not write the correct value");
    }

}
