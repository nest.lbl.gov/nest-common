package gov.lbl.nest.common.tasks;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;

/**
 * This class defines the tests that the {@link ActiveDeque} class must pass.
 *
 * @author patton
 */
@DisplayName("ActiveDeque class")
public class ActiveDequeTests extends
                              AbstractActiveCollectionTests<Element<MockInspection>, MockInspection> {

    @Override
    protected void activateElement(Element<MockInspection> element) {
        ((MockElement) element).setActive(true);
    }

    @Override
    protected ActiveCollection<Element<MockInspection>, MockInspection> getTestInstance() {
        return new ActiveDeque<>();
    }

    @Override
    protected List<MockElement> getUnorderedElements() {
        @SuppressWarnings("serial")
        final List<MockElement> result = new ArrayList<>() {
            {
                add(new MockElement(true));
                add(new MockElement(false));
                add(new MockElement(true));
            }
        };
        return result;
    }
}
