package gov.lbl.nest.common.tasks;

import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.MockTask.Type;

/**
 * This class defines the tests that the {@link FifoProducer} class must pass.
 *
 * @author patton
 */
public class FifoProducerTests extends
                               AbstractProducerTests<MockTask> {

    private FifoProducer<MockTask> testInstance;

    @Override
    protected List<? extends MockTask> addTasks(Producer<MockTask> producer,
                                                int count) {
        assertSame(testInstance,
                   producer);
        final List<MockTask> result = new ArrayList<>();
        for (int index = 0;
             count != index;
             ++index) {
            final MockTask task = new MockTaskImpl(Type.TIMED,
                                                   0);
            testInstance.add(task);
            result.add(task);
        }
        return result;
    }

    @Override
    protected Producer<MockTask> getTestInstance() {
        testInstance = new FifoProducer<>();
        return testInstance;
    }

}
