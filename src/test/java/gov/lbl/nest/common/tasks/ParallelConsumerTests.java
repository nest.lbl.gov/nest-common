package gov.lbl.nest.common.tasks;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.DisplayName;

import gov.lbl.nest.common.tasks.Consumer.Producer;

/**
 * This class defines the tests that the {@link ParallelConsumerTests} class
 * must pass.
 *
 * @author patton
 */
@DisplayName("ParallelConsumer class")
public class ParallelConsumerTests extends
                                   AbstractConsumerTests {

    private ParallelConsumer testInstance;

    @Override
    protected void decreaseExecutionLimit(int count) {
        for (int index = 0;
             count != index;
             ++index) {
            testInstance.removeThread();
        }
    }

    @Override
    protected Consumer getTestInstance(int initialCount,
                                       Producer<MockTask> producer) {
        testInstance = new ParallelConsumer("Test",
                                            initialCount,
                                            producer,
                                            new MockExecutor());
        // Allow minimum, not zero, pull attempts
        testInstance.setPullWaitAttempts(1);
        testInstance.setPullWaitTime(1);
        return testInstance;
    }

    @Override
    protected void increaseExecutionLimit(int count) {
        for (int index = 0;
             count != index;
             ++index) {
            testInstance.addThread();
        }
    }

    @Override
    protected void waitForActiveCount(int target) throws InterruptedException {
        final AtomicInteger activeCount = testInstance.getActiveCount();
        if (target != activeCount.get())
            synchronized (activeCount) {
                while (target != activeCount.get()) {
                    activeCount.wait();
                }
            }
    }

}
