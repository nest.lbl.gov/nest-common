package gov.lbl.nest.common.tasks.ordinal;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;

import gov.lbl.nest.common.tasks.AbstractOrdinalActiveCollectionTests;

/**
 * This class defines the tests that the {@link RankedActiveCollection} class
 * must pass.
 *
 * @author patton
 */
@DisplayName("RankedActiveCollection class")
public class RankedActiveCollectionTests extends
                                         AbstractOrdinalActiveCollectionTests<MockRankedElement, MockRankedInspector> {

    @SuppressWarnings("serial")
    private static final List<? extends MockRankedElement> TASKS = new ArrayList<>() {
        {
            add(new MockRankedElement(1));
            add(new MockRankedElement(0));
            add(new MockRankedElement(2));
            add(new MockRankedElement(0));
            add(new MockRankedElement(1));
        }
    };

    /**
     * The order in which the element instance should be executed.
     */
    @SuppressWarnings("serial")
    private static final List<? extends MockRankedElement> EXPECTED_ORDER = new ArrayList<>() {
        {
            add(TASKS.get(2));
            add(TASKS.get(0));
            add(TASKS.get(4));
            add(TASKS.get(1));
            add(TASKS.get(3));
        }
    };

    @Override
    protected void activateElement(MockRankedElement element) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected List<? extends MockRankedElement> getOrderedElements() {
        return EXPECTED_ORDER;
    }

    @Override
    protected RankedActiveCollection<MockRankedElement, MockRankedInspector> getTestInstance() {
        return new RankedActiveCollection<>();
    }

    @Override
    protected List<? extends MockRankedElement> getUnorderedElements() {
        return TASKS;
    }

}
