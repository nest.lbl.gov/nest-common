package gov.lbl.nest.common.tasks;

/**
 * This class is used to inspect an {@link MockElement} instance.
 *
 * @author patton
 */
public class MockInspection {

    /**
     * Creates a instance of this class.
     *
     * @param element
     *            the {@link Element} this Object is inspecting.
     */
    MockInspection(MockElement element) {
    }
}
