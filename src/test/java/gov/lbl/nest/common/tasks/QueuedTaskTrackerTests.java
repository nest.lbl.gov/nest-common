package gov.lbl.nest.common.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.tasks.MockTask.Type;
import gov.lbl.nest.common.tasks.TaskTracker.StateChange;

/**
 * This class defines the common tests the {@link TaskTracker} class must pass.
 *
 * @author patton
 */
public class QueuedTaskTrackerTests extends
                                    AbstractTaskTrackerTests<MockQueueableTask> {

    private static final long STALLED_TIMEOUT = 5;

    @Override
    protected MockQueueableTask createTask(Type type) {
        return new MockQueueableTaskImpl(type);
    }

    @Test
    @DisplayName("Detect stalled task")
    void detectStalledTask() throws InterruptedException {
        keeperConstruction();
        final MockQueueableTask task = createTask(Type.FLAGGED);

        final MockQueueableTask wrappedTask = testInstance.trackTask(task,
                                                                     MockQueueableTask.class);
        wrappedTask.beginExecution();
        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                try {
                    wrappedTask.run();
                } finally {
                    wrappedTask.endExecution();
                }
            }
        };
        final Thread thread = new Thread(runnable);
        thread.start();
        while (!task.isStarted()) {
            Thread.sleep(1);
        }

        boolean done = false;
        while (!done) {
            List<MockQueueableTask> executing = testInstance.getExecuting();
            final Duration executionLimit = Duration.ofMillis(STALLED_TIMEOUT);
            for (MockQueueableTask executionTask : executing) {
                final Duration executionDuration = executionTask.getExecutionDuration();
                if (0 > executionLimit.compareTo(executionDuration)) {
                    thread.interrupt();
                    done = true;
                }
            }
        }

        while (!task.isFinished()) {
            Thread.sleep(1);
        }

        assertTrue((testInstance.getWaiting()).isEmpty());
        assertTrue((testInstance.getExecuting()).isEmpty());
        final List<MockQueueableTask> completed = testInstance.getTerminated(false);
        assertEquals(1,
                     completed.size());
        assertSame(task,
                   completed.get(0));
        assertEquals(1,
                     (testInstance.getTerminated()).size());
        assertTrue((testInstance.getTerminated()).isEmpty());
    }

    @Override
    protected TaskTracker<MockQueueableTask> getTestInstance() {
        if (null == testInstance) {
            testInstance = new TaskTracker<>();
        }
        return testInstance;
    }

    @Override
    protected TaskTracker<MockQueueableTask> getTestInstance(StateChange<MockQueueableTask> stateChange) {
        if (null == testInstance) {
            testInstance = new TaskTracker<>(stateChange);
        }
        return testInstance;
    }
}
