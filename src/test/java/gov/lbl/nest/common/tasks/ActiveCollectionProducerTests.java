package gov.lbl.nest.common.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.Consumer.Task;
import gov.lbl.nest.common.tasks.MockTask.Type;

/**
 * This class defines the tests that the {@link ActiveCollectionProducer} class
 * must pass.
 *
 * @author patton
 */
@DisplayName("ActiveCollectionProducer class")
public class ActiveCollectionProducerTests extends
                                           AbstractProducerTests<MockActiveTask> {

    /**
     * The number of attempts to see if consumption as settled.
     */
    private static final int ATTEMPTS_TO_SETTLE = 5;

    /**
     * The number of active tasks for each inactive one.
     */
    private static final int ACTIVE_FREQUENCY = 1;

    private ActiveCollectionProducer<MockActiveTask, Boolean> testInstance;

    private List<? extends MockActiveTask> addFlaggedTasks(Producer<MockActiveTask> producer,
                                                           int count) {
        assertSame(testInstance,
                   producer);
        final List<MockActiveTask> result = new ArrayList<>();
        for (int index = 0;
             count != index;
             ++index) {
            final MockActiveTask task = new MockActiveTask(Type.FLAGGED);
            testInstance.add(task);
            result.add(task);
        }
        return result;
    }

    @Override
    protected List<? extends MockActiveTask> addTasks(Producer<MockActiveTask> producer,
                                                      int count) {
        assertSame(testInstance,
                   producer);
        final List<MockActiveTask> result = new ArrayList<>();
        for (int index = 0;
             count != index;
             ++index) {
            final MockActiveTask task = new MockActiveTask(Type.TIMED,
                                                           0);
            (testInstance.getSupplier()).add(task);
            result.add(task);
        }
        return result;
    }

    private void completeTasks(final List<? extends MockTask> tasks,
                               int begin,
                               int end) throws InterruptedException {
        for (MockTask task : tasks.subList(begin,
                                           end)) {
            task.complete();
            synchronized (task) {
                while (!task.isFinished()) {
                    task.wait();
                }
            }
        }
    }

    @Override
    protected Producer<MockActiveTask> getTestInstance() {
        testInstance = new ActiveCollectionProducer<>(new ActiveDeque<MockActiveTask, Boolean>());
        return testInstance;
    }

    /**
     * Test that a {@link Producer} instance successfully servers out a collection
     * that contains active and inactive {@link Task} instances.
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName(value = "Only serve out active Tasks")
    void onlyServeOutActiveTasks() throws InterruptedException {
        getTestInstance();
        MockConsumer consumer = new MockConsumer(testInstance);
        final List<? extends MockActiveTask> tasks = addTasks(testInstance,
                                                              BASE_COUNT);

        // Deactive selected tasks.
        int count = 0;
        int inactive = 0;
        for (MockActiveTask task : tasks) {
            assertFalse(task.isFinished());
            if (0 == count % (ACTIVE_FREQUENCY + 1)) {
                task.setActive(false);
                ++inactive;
            }
            ++count;
        }

        // Consume active tasks.
        final int active = BASE_COUNT - inactive;
        consumer.consume(active);

        // Confirm inactive tasks have not been consumed.
        assertEquals(inactive,
                     testInstance.size());
        assertEquals(0,
                     testInstance.activeSize());
        final Iterator<? extends MockActiveTask> iterator2 = tasks.iterator();
        while (iterator2.hasNext()) {
            final MockActiveTask task = iterator2.next();
            if (task.isActive()) {
                synchronized (task) {
                    while (!task.isFinished()) {
                        task.wait();
                    }
                }
                iterator2.remove();
            } else {
                // Activate and inactive task.
                assertFalse(task.isFinished());
                task.setActive(true);
            }
        }
        assertEquals(inactive,
                     testInstance.inactiveSize());
        assertEquals(inactive,
                     tasks.size());

        // Reset Producer and consume the remaining tasks.
        testInstance.reset();
        assertEquals(inactive,
                     consumer.consume());
        assertEquals(0,
                     testInstance.size());
        for (MockTask task : tasks) {
            synchronized (task) {
                while (!task.isFinished()) {
                    task.wait();
                }
            }
        }
    }

    /**
     * Test that a {@link Producer} instance successfully servers out a collection
     * that contains active and inactive {@link Task} instances.
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName(value = "Restart Producer")
    void restartProducer() throws InterruptedException {
        getTestInstance();
        MockConsumer consumer = new MockConsumer(testInstance);
        final List<? extends MockActiveTask> tasks = addFlaggedTasks(testInstance,
                                                                     BASE_COUNT);

        // Deactive selected tasks.
        int count = 0;
        int inactive = 0;
        for (MockActiveTask task : tasks) {
            assertFalse(task.isFinished());
            if (0 == count % (ACTIVE_FREQUENCY + 1)) {
                task.setActive(false);
                ++inactive;
            }
            ++count;
        }

        // Consume half of the active tasks, stall the others.
        final int active = BASE_COUNT - inactive;
        consumer.consume(active);
        int removed = active / 2;
        final int stalled = active - removed;
        final List<MockActiveTask> complete = new ArrayList<>();
        final Iterator<? extends MockActiveTask> iterator = tasks.iterator();
        while (iterator.hasNext() && 0 != removed) {
            final MockActiveTask task = iterator.next();
            if (task.isActive()) {
                complete.add(task);
                iterator.remove();
                --removed;
            }
        }
        completeTasks(complete,
                      0,
                      complete.size());

        // Confirm inactive tasks have not been consumed.
        assertEquals(inactive,
                     testInstance.size());
        final List<MockActiveTask> incomplete = new ArrayList<>();
        final Iterator<? extends MockActiveTask> iterator2 = tasks.iterator();
        while (iterator2.hasNext()) {
            final MockActiveTask task = iterator2.next();
            assertFalse(task.isFinished());
            if (task.isActive()) {
                incomplete.add(task);
                iterator2.remove();
            } else {
                task.setActive(true);
            }
        }
        assertEquals(inactive,
                     testInstance.inactiveSize());
        assertEquals(inactive,
                     tasks.size());
        assertEquals(stalled,
                     incomplete.size());

        // Reset Producer and consume the remaining tasks.
        int attemptsLeft = ATTEMPTS_TO_SETTLE;
        while (stalled != consumer.getTotalThreadCount() || 0 != attemptsLeft) {
            Thread.sleep(1);
            --attemptsLeft;
        }
        assertEquals(stalled,
                     consumer.getTotalThreadCount());
        assertEquals(stalled,
                     (consumer.getExecutingCount()).get());
        testInstance.restart();
        assertEquals(stalled,
                     consumer.getTotalThreadCount());
        assertEquals(0,
                     (consumer.getExecutingCount()).get());
        assertEquals(inactive,
                     consumer.consume());
        assertEquals(0,
                     testInstance.size());
        completeTasks(tasks,
                      0,
                      tasks.size());

        for (MockTask task : tasks) {
            synchronized (task) {
                while (!task.isFinished()) {
                    task.wait();
                }
            }
        }

        // Confirm the stalled tasks have not finished.
        for (MockActiveTask task : incomplete) {
            assertFalse(task.isFinished());
        }

        // Let stalled task complete.
        completeTasks(incomplete,
                      0,
                      incomplete.size());
        attemptsLeft = ATTEMPTS_TO_SETTLE;
        while (0 != consumer.getTotalThreadCount() || 0 != attemptsLeft) {
            Thread.sleep(1);
            --attemptsLeft;
        }
        assertEquals(0,
                     consumer.getTotalThreadCount());
    }

}
