package gov.lbl.nest.common.tasks;

import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * This class implements the {@link MockTask} interface for tests.
 *
 * @author patton
 */
public class MockTaskImpl implements
                          MockTask {

    /**
     * Used to assign a unique number to each instance of this class.
     */
    private static final AtomicInteger counter = new AtomicInteger(0);

    /**
     * <code>true</code> if this Object can complete its execution.
     */
    private AtomicBoolean completing = new AtomicBoolean();

    /**
     * The {@link RuntimeException} instance, if any, thrown by this Object.
     */
    private final RuntimeException exception;

    /**
     * <code>true</code> if this Object has failed.
     */
    private AtomicBoolean failure = new AtomicBoolean();

    /**
     * The unique number assigned to this task.
     */
    private final int number;

    /**
     * The parameter used by this Object, dependent on what {@link Type} of
     * execution is happening.
     */
    private final int parameter;

    /**
     * <code>true</code> if this Object has finished successfully.
     */
    private AtomicBoolean success = new AtomicBoolean();

    /**
     * The date and time that this Object started running.
     */
    private AtomicReference<Date> whenStarted = new AtomicReference<>(null);

    /**
     * The type of execution this Object is doing.
     */
    private final Type type;

    /**
     * Creates an instance of this class.
     *
     * @param type
     *            the type of execution this Object is doing.
     */
    MockTaskImpl(Type type) {
        this(type,
             0,
             null);
    }

    /**
     * Creates an instance of this class.
     *
     * @param type
     *            the type of execution this Object is doing.
     * @param parameter
     *            the parameter used by this Object, dependent on what {@link Type}
     *            of execution is happening.
     */
    MockTaskImpl(Type type,
                 int parmeter) {
        this(type,
             parmeter,
             null);
    }

    private MockTaskImpl(Type type,
                         int parmeter,
                         RuntimeException exception) {
        number = counter.getAndIncrement();
        this.parameter = parmeter;
        this.exception = exception;
        this.type = type;
    }

    MockTaskImpl(Type type,
                 RuntimeException exception) {
        this(type,
             0,
             exception);
    }

    @Override
    public void beginExecution() {
        // Do nothing
    }

    @Override
    public void complete() {
        synchronized (this) {
            completing.set(true);
            this.notifyAll();
        }
    }

    @Override
    public void endExecution() {
        // Do nothing
    }

    /**
     * Returns the unique number assigned to this task.
     *
     * @return the unique number assigned to this task.
     */
    public int getNumber() {
        return number;
    }

    /**
     * Returns the date and time that this Object started running.
     *
     * @return the date and time that this Object started running.
     */
    Date getWhenStarted() {
        return whenStarted.get();
    }

    /**
     * Returns <code>true</code> if this Object can complete its execution.
     *
     * @return <code>true</code> if this Object can complete its execution.
     */
    boolean isCompleting() {
        return completing.get();
    }

    @Override
    public boolean isFailure() {
        return failure.get();
    }

    @Override
    public boolean isFinished() {
        synchronized (this) {
            return success.get() || failure.get();
        }
    }

    @Override
    public boolean isStarted() {
        return null != whenStarted.get();
    }

    /**
     * Returns <code>true</code> if this Object has finished successfully.
     *
     * @return <code>true</code> if this Object has finished successfully.
     */
    public boolean isSuccess() {
        return success.get();
    }

    @Override
    public void run() {
        synchronized (this) {
            setWhenStarted(new Date());

            switch (type) {
            case FLAGGED:
                this.notifyAll();
                try {
                    while (!isCompleting()) {
                        wait();
                    }
                } catch (InterruptedException e) {
                    // Do nothing
                }
                break;
            case TIMED:
                try {
                    Thread.sleep(parameter);
                } catch (InterruptedException e) {
                    // Do nothing
                }
                break;
            }
            if (null != exception) {
                failure.set(true);
                this.notifyAll();
                throw exception;
            }
            success.set(true);
            this.notifyAll();
        }
    }

    /**
     * Sets the date and time that this Object started running.
     *
     * @param dateTime
     *            the date and time that this Object started running.
     */
    protected void setWhenStarted(Date dateTime) {
        if (null != whenStarted.get()) {
            throw new IllegalStateException("Start timing is already yet set");
        }
        whenStarted.set(dateTime);
    }

    @Override
    public String toString() {
        return "MockTask [failure=" + failure
               + ", number="
               + number
               + ", success="
               + success
               + "]";
    }
}
