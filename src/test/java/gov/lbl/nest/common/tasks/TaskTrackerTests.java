package gov.lbl.nest.common.tasks;

import gov.lbl.nest.common.queued.QueueableImpl;
import gov.lbl.nest.common.tasks.MockTask.Type;
import gov.lbl.nest.common.tasks.TaskTracker.StateChange;

/**
 * This class defines the common tests the {@link TaskTracker} class must pass.
 *
 * @author patton
 */
public class TaskTrackerTests extends
                              AbstractTaskTrackerTests<MockTask> {

    /**
     * The {@link QueueableImpl} instance being tested.
     */
    TaskTracker<MockTask> testInstance;

    @Override
    protected MockTask createTask(Type type) {
        return new MockTaskImpl(type);
    }

    @Override
    protected TaskTracker<MockTask> getTestInstance() {
        if (null == testInstance) {
            testInstance = new TaskTracker<>();
        }
        return testInstance;
    }

    @Override
    protected TaskTracker<MockTask> getTestInstance(StateChange<MockTask> stateChange) {
        if (null == testInstance) {
            testInstance = new TaskTracker<>(stateChange);
        }
        return testInstance;
    }

}
