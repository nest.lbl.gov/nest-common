package gov.lbl.nest.common.tasks.ordinal;

import gov.lbl.nest.common.tasks.Element;

/**
 * This class is used to test that ranking of elements works correctly.
 *
 * @author patton
 *
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public class MockUninspectedRankedElement<S> implements
                                         RankedElement<S> {

    /**
     * The "rank" of this element, where object with a higher rank are preferred.
     */
    private final Integer depth;

    /**
     * The "inspection" that results from the inspecting of this Object.
     */
    private final S inspector;

    /**
     * Creates an instance of this class.
     *
     * @param depth
     *            the "rank" of this element, where object with a higher rank are
     *            preferred.
     * @param inspector
     *            the "inspection" that results from the inspecting of this Object.
     */
    public MockUninspectedRankedElement(Integer depth,
                                        S inspector) {
        this.depth = depth;
        this.inspector = inspector;
    }

    @Override
    public Integer getRank() {
        return depth;
    }

    @Override
    public S inspect() {
        return inspector;
    }

    @Override
    public boolean isActive() {
        return true;
    }

}
