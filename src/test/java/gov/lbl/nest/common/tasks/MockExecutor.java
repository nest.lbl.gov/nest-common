package gov.lbl.nest.common.tasks;

import java.util.concurrent.Executor;

/**
 * This class implements the {@link Executor} interface to simply supply
 * {@link Thread} instances on demand.
 *
 * @author patton
 */
public class MockExecutor implements
                          Executor {

    @Override
    public void execute(Runnable command) {
        final Thread thread = new Thread(command);
        thread.start();
    }

}
