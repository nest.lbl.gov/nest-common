package gov.lbl.nest.common.tasks;

import java.util.ArrayList;
import java.util.List;

/**
 * This class defines the tests that the {{@link OrdinalActiveCollection} using
 * using {@link ActiveDeque} instances must pass.
 *
 * @author patton
 */
public class OrdinalActiveDequeTests extends
                                     AbstractOrdinalActiveCollectionTests<MockElement, MockInspection> {

    @SuppressWarnings("serial")
    private static final List<MockElement> ELEMENTS = new ArrayList<>() {
        {
            add(new MockElement(3));
            add(new MockElement(1));
            add(new MockElement(4));
            add(new MockElement(0));
            add(new MockElement(2));
        }
    };

    /**
     * The order in which the element instance should be executed.
     */
    @SuppressWarnings("serial")
    private static final List<MockElement> EXPECTED_ORDER = new ArrayList<>() {
        {
            add(ELEMENTS.get(2));
            add(ELEMENTS.get(0));
            add(ELEMENTS.get(4));
            add(ELEMENTS.get(1));
            add(ELEMENTS.get(3));
        }
    };

    @Override
    protected void activateElement(MockElement element) {
        element.setActive(true);
    }

    @Override
    protected List<? extends MockElement> getOrderedElements() {
        return EXPECTED_ORDER;
    }

    @Override
    protected OrdinalActiveCollection<MockElement, MockInspection> getTestInstance() {
        return new OrdinalActiveCollection<>(new ActiveDequeFactory<MockElement, MockInspection>(),
                                             new MockOrdinal<>());
    }

    @Override
    protected List<? extends MockElement> getUnorderedElements() {
        return ELEMENTS;
    }

}
