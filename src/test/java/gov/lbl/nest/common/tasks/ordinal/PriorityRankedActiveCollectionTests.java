package gov.lbl.nest.common.tasks.ordinal;

import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.common.tasks.AbstractOrdinalActiveCollectionTests;

/**
 * This class defines the tests that the {@link PriorityRankedActiveCollection}
 * class using {@link MockPriorityRankedElement} and
 * {@link MockPriorityRankedInspector} classes must pass.
 *
 *
 * <pre>
 *                    Depth
 *            |  0   1   2   3   4
 *          ---------------------
 *          0 |  9  16   7  13  15
 *            |
 *          1 |  6   2  17   5  12
 *            |
 * priority 2 | 23  18  24   0   3
 *            |
 *          3 | 10  21   1  22   8
 *            |
 *          4 | 19   4  20  11  14
 * </pre>
 *
 * @author patton
 */
public class PriorityRankedActiveCollectionTests extends
                                                 AbstractOrdinalActiveCollectionTests<MockPriorityRankedElement, MockPriorityRankedInspector> {

    @SuppressWarnings("serial")
    private static final List<MockPriorityRankedElement> TASKS = new ArrayList<>() {
        {
            add(new MockPriorityRankedElement(2,
                                              3));
            add(new MockPriorityRankedElement(3,
                                              2));
            add(new MockPriorityRankedElement(1,
                                              1));
            add(new MockPriorityRankedElement(2,
                                              4));
            add(new MockPriorityRankedElement(4,
                                              1));

            add(new MockPriorityRankedElement(1,
                                              3));
            add(new MockPriorityRankedElement(1,
                                              0));
            add(new MockPriorityRankedElement(0,
                                              2));
            add(new MockPriorityRankedElement(3,
                                              4));
            add(new MockPriorityRankedElement(0,
                                              0));

            add(new MockPriorityRankedElement(3,
                                              0));
            add(new MockPriorityRankedElement(4,
                                              3));
            add(new MockPriorityRankedElement(1,
                                              4));
            add(new MockPriorityRankedElement(0,
                                              3));
            add(new MockPriorityRankedElement(4,
                                              4));

            add(new MockPriorityRankedElement(0,
                                              4));
            add(new MockPriorityRankedElement(0,
                                              1));
            add(new MockPriorityRankedElement(1,
                                              2));
            add(new MockPriorityRankedElement(2,
                                              1));
            add(new MockPriorityRankedElement(4,
                                              0));

            add(new MockPriorityRankedElement(4,
                                              2));
            add(new MockPriorityRankedElement(3,
                                              1));
            add(new MockPriorityRankedElement(3,
                                              3));
            add(new MockPriorityRankedElement(2,
                                              0));
            add(new MockPriorityRankedElement(2,
                                              2));
        }
    };

    /**
     * The order in which the element instance should be executed.
     */
    @SuppressWarnings("serial")
    private static final List<MockPriorityRankedElement> EXPECTED_ORDER = new ArrayList<>() {
        {
            add(TASKS.get(14));
            add(TASKS.get(11));
            add(TASKS.get(20));
            add(TASKS.get(4));
            add(TASKS.get(19));
            add(TASKS.get(8));
            add(TASKS.get(22));
            add(TASKS.get(1));
            add(TASKS.get(21));
            add(TASKS.get(10));
            add(TASKS.get(3));
            add(TASKS.get(0));
            add(TASKS.get(24));
            add(TASKS.get(18));
            add(TASKS.get(23));
            add(TASKS.get(12));
            add(TASKS.get(5));
            add(TASKS.get(17));
            add(TASKS.get(2));
            add(TASKS.get(6));
            add(TASKS.get(15));
            add(TASKS.get(13));
            add(TASKS.get(7));
            add(TASKS.get(16));
            add(TASKS.get(9));
        }
    };

    @Override
    protected void activateElement(MockPriorityRankedElement element) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected List<MockPriorityRankedElement> getOrderedElements() {
        return EXPECTED_ORDER;
    }

    @Override
    protected PriorityRankedActiveCollection<MockPriorityRankedElement, MockPriorityRankedInspector> getTestInstance() {
        return new PriorityRankedActiveCollection<>();
    }

    @Override
    protected List<MockPriorityRankedElement> getUnorderedElements() {
        return TASKS;
    }

}
