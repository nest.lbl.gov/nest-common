package gov.lbl.nest.common.tasks;

/**
 * This class implements the {@link OrdinalExtractor} interface to return the
 * rank of a {@link MockElement} instance.
 *
 * @author patton
 *
 * @param <E>
 *            the type of Object contained in this container.
 */
public class MockOrdinal<E extends MockElement> implements
                        OrdinalExtractor<E> {

    @Override
    public int getOrdinal(MockElement e) {
        return e.getOrdinal();
    }
}
