package gov.lbl.nest.common.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This class defines the tests that the {@link AbstractConsumerTests} class
 * must pass.
 *
 * @author patton
 *
 * @param <T>
 *            the type of the {@link MockTask} class being tested.
 */
public abstract class AbstractProducerTests<T extends MockTask> {

    /**
     * The base number of tasks to use in testing.
     */
    protected static final int BASE_COUNT = 8;

    /**
     * Adds the requested number of {@link Task} instances to the supplied
     * {@link Producer} instance.
     *
     * @param producer
     *            the {@link Producer} instance to which the {@link Task} instances
     *            should be added.
     * @param count
     *            the number of {@link Task} instances to add.
     *
     * @return the list of added {@link Task} instances.
     */
    protected abstract List<? extends MockTask> addTasks(Producer<T> producer,
                                                         int count);

    /**
     * Returns the instance to be tested.
     *
     * @return the {@link Producer} instance to be tested.
     */
    protected abstract Producer<T> getTestInstance();

    /**
     * Test that a {@link Producer} instance successfully servers out a collection
     * that only contains active {@link Task} instances.
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName(value = "Serve out Tasks")
    void serveOutTasks() throws InterruptedException {
        final Producer<T> testInstance = getTestInstance();
        MockConsumer consumer = new MockConsumer(testInstance);
        assertEquals(testInstance.getSupplier(),
                     consumer.getSupplier());
        final List<? extends MockTask> tasks = addTasks(testInstance,
                                                        BASE_COUNT);
        for (MockTask task : tasks) {
            assertFalse(task.isFinished());
        }
        consumer.consume(BASE_COUNT);
        assertEquals(0,
                     testInstance.size());
        for (MockTask task : tasks) {
            synchronized (task) {
                while (!task.isFinished()) {
                    task.wait();
                }
            }
        }
    }
}
