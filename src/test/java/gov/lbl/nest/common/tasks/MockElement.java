package gov.lbl.nest.common.tasks;

/**
 * This class implements the {@link Element} interface using the
 * {@link MockInspection} class as the inspector.
 *
 * @author patton
 *
 */
public class MockElement implements
                         Element<MockInspection> {

    /**
     * <code>true</code> if this element is "active".
     */
    private boolean active = true;

    /**
     * The {@link MockInspection} instance, if any, used by this class.
     */
    private MockInspection inspection;

    /**
     * Tthe ordinal to be used for ordering instances of this class.
     */
    private int ordinal;

    /**
     * Creates an instance of this class.
     *
     * @param active
     *            <code>true</code> if this Object is "active".
     */
    MockElement(boolean active) {
        this.active = active;
    }

    /**
     * Creates an instance of this class.
     *
     * @param ordinal
     *            the ordinal to be used for ordering instances of this class.
     */
    MockElement(int ordinal) {
        this.ordinal = ordinal;
    }

    /**
     * Returns the ordinal to be used for ordering instances of this class.
     *
     * @return the ordinal to be used for ordering instances of this class.
     */
    int getOrdinal() {
        return ordinal;
    }

    @Override
    public MockInspection inspect() {
        if (null == inspection) {
            inspection = new MockInspection(this);
        }
        return inspection;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    /**
     * Sets whether this Object is "active" or not.
     *
     * @param active
     *            <code>true</code> if this Object is "active".
     */
    void setActive(boolean active) {
        this.active = active;
    }

}
