package gov.lbl.nest.common.tasks;

import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.MockTask.Type;

/**
 * This class test that a {@link OpportunisticProducers} instance work correctly
 * when only the "splitting" half of the structure is specified.
 *
 * @author patton
 */
public class OpportunisticSingleProducerTests extends
                                              AbstractProducerTests<MockTask> {

    private FifoProducer<MockTask> doubleInbound;

    private Producer<MockTask> testInstance;

    @Override
    protected List<? extends MockTask> addTasks(Producer<MockTask> producer,
                                                int count) {
        assertSame(testInstance,
                   producer);
        final List<MockTask> result = new ArrayList<>();
        for (int index = 0;
             count != index;
             ++index) {
            final MockTask task = new MockTaskImpl(Type.TIMED,
                                                   0);
            doubleInbound.add(task);
            result.add(task);
        }
        return result;
    }

    @Override
    protected Producer<MockTask> getTestInstance() {
        doubleInbound = new FifoProducer<>();
        OpportunisticProducers<MockTask> producers = new OpportunisticProducers<>(doubleInbound,
                                                                                  null);
        testInstance = producers.getSingleProducer();
        return testInstance;
    }

}
