package gov.lbl.nest.common.tasks;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This class defines the tests that the {@link TimedTask} class must pass.
 *
 * @author patton
 */
@DisplayName("TimedTask class")
public class TimedTaskTests {

    @Test
    @DisplayName(value = "Basic Execution")
    void basicExecution() {
        final Date preCreate = new Date();
        final Task original = new MockTaskImpl(null);
        final TimedTask task = new TimedTask(original);
        final Date postCreate = new Date();
        assertNotNull(task.getWhenCreated());
        final long beforeCreate = (task.getWhenCreated()).getTime() - preCreate.getTime();
        assertTrue(0 <= beforeCreate);
        final long afterCreate = postCreate.getTime() - (task.getWhenCreated()).getTime();
        assertTrue(0 <= afterCreate);
        assertNull(task.getWhenStarted());
        assertNotNull(task.getWaitDuration());
        assertNull(task.getWhenFinished());
        assertNull(task.getExecutionDuration());
        assertNotNull(task.getTotalDuration());
        final Date preStart = new Date();
        task.beginExecution();
        final Date postStart = new Date();
        assertNotNull(task.getWhenStarted());
        final long beforeStart = (task.getWhenStarted()).getTime() - preStart.getTime();
        assertTrue(0 <= beforeStart);
        final long afterStart = postStart.getTime() - (task.getWhenStarted()).getTime();
        assertTrue(0 <= afterStart);

        assertNull(task.getWhenFinished());
        assertNotNull(task.getExecutionDuration());
        final Date preFinish = new Date();
        task.endExecution();
        final Date postFinish = new Date();
        assertNotNull(task.getWhenFinished());
        final long beforeFinish = (task.getWhenFinished()).getTime() - preFinish.getTime();
        assertTrue(0 <= beforeFinish);
        final long afterFinish = postFinish.getTime() - (task.getWhenFinished()).getTime();
        assertTrue(0 <= afterFinish);
    }
}
