package gov.lbl.nest.common.tasks;

import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This interface extends the {@link Task} interface in order to run tests.
 *
 * @author patton
 *
 */
public interface MockTask extends
                          Task {

    /**
     * This enumeration lists type of test being run.
     *
     * @author patton
     */
    enum Type {
               /**
                * The task is explicitly flagged to execute and complete.
                */
               FLAGGED,
               /**
                * The task is timed to execute and complete.
                */
               TIMED,
    }

    /**
     * Allows this Object to complete its execution.
     */
    void complete();

    /**
     * Returns <code>true</code> if this Object has failed.
     *
     * @return <code>true</code> if this Object has failed.
     */
    boolean isFailure();

    /**
     * Returns <code>true</code> if this Object has finished executed.
     *
     * @return <code>true</code> if this Object has finished executed.
     */
    boolean isFinished();

    /**
     * Returns <code>true</code> if this Object has started executed.
     *
     * @return <code>true</code> if this Object has started executed.
     */
    boolean isStarted();
}
