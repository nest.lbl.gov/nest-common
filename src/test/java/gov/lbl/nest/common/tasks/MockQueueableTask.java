package gov.lbl.nest.common.tasks;

import gov.lbl.nest.common.queued.Queued;

/**
 * This interface combines the {@link MockTask}, {@link QueueableTask} and
 * {@link Queued} interfaces in to a single one.
 *
 * @author patton
 */
public interface MockQueueableTask extends
                                   MockTask,
                                   QueueableTask,
                                   Queued {

}
