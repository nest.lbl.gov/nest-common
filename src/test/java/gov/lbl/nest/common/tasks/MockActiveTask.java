package gov.lbl.nest.common.tasks;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * This class implements the {@link ActiveTask} interface in conjunction with
 * the {@link MockTaskImpl} class.
 *
 * @author patton
 */
public class MockActiveTask extends
                            MockTaskImpl implements
                            ActiveTask<Boolean> {

    /**
     * <code>true</code> if this Object is "active".
     */
    private AtomicBoolean active = new AtomicBoolean(true);

    MockActiveTask(Type type) {
        super(type);
    }

    /**
     * Creates an instance of this class.
     *
     * @param type
     *            the type of execution this Object is doing.
     * @param parameter
     *            the parameter used by this Object, dependent on what
     *            {@link gov.lbl.nest.common.tasks.MockTask.Type}. of execution is
     *            happening.
     */
    public MockActiveTask(Type type,
                          int parameter) {
        super(type,
              parameter);
    }

    @Override
    public Boolean inspect() {
        return isFinished();
    }

    @Override
    public boolean isActive() {
        return active.get();
    }

    /**
     * Sets whether this Object is "active" or not.
     *
     * @param active
     *            <code>true</code> if this Object is "active".
     */
    public void setActive(boolean active) {
        this.active.set(active);
    }

    @Override
    public String toString() {
        return "MockActiveTask [active=" + active
               + ", toString()="
               + super.toString()
               + "]";
    }
}
