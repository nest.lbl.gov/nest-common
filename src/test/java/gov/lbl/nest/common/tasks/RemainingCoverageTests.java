package gov.lbl.nest.common.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This class make sure that code coverage visits code that would not normally
 * be visited.
 *
 * @author patton
 */
@DisplayName("Execution Remaining Coverage")
public class RemainingCoverageTests {

    static class MockConsumer implements
                              Consumer {

        private boolean flushed = false;

        private final Producer<?> producer;

        private MockConsumer(Producer<?> producer) {
            this.producer = producer;
            producer.consumer(this);
        }

        @Override
        public boolean addedTask() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void flush() {
            flushed = true;
        }

        @Override
        public AtomicInteger getExecutingCount() {
            return EXECUTION_COUNT;
        }

        @Override
        public int getExecutionLimit() {
            return EXECUTION_LIMIT;
        }

        @Override
        public String getName() {
            return CONSUMER_NAME;
        }

        @Override
        public Supplier<?> getSupplier() {
            return producer.getSupplier();
        }

        @Override
        public Object getSynchronizer() {
            return producer;
        }

        boolean isFlushed() {
            return flushed;
        }

        @Override
        public boolean pause() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean proceed() {
            throw new UnsupportedOperationException();
        }
    }

    static class MockProducer<T extends Task> implements
                             Producer<T>,
                             Supplier<T> {

        Consumer consumer;

        @Override
        public boolean add(Task task) {
            return false;
        }

        @Override
        public void consumer(Consumer consumer) {
            this.consumer = consumer;
        }

        private Consumer getConsumer() {
            return consumer;
        }

        @Override
        public Supplier<T> getSupplier() {
            return this;
        }

        @Override
        public Task next() {
            throw new UnsupportedOperationException();
        }

        @Override
        public int size() {
            throw new UnsupportedOperationException();
        }
    }

    private static final String CONSUMER_NAME = "name";

    private static final AtomicInteger EXECUTION_COUNT = new AtomicInteger(4);

    private static final int EXECUTION_LIMIT = 4;

    @Test
    @DisplayName(value = "Consumer decorator for OpportunisticProducers")
    void consumerDecoration() {
        final MockProducer<Task> producer = new MockProducer<>();
        final OpportunisticProducers<Task> producers = new OpportunisticProducers<>(producer,
                                                                                    null);
        final MockConsumer consumer = new MockConsumer(producers.getSingleProducer());
        final Consumer decoratedConsumer = producer.getConsumer();
        assertEquals(CONSUMER_NAME,
                     decoratedConsumer.getName());
        assertEquals(EXECUTION_COUNT,
                     decoratedConsumer.getExecutingCount());
        assertEquals(EXECUTION_LIMIT,
                     decoratedConsumer.getExecutionLimit());
        assertSame(producer,
                   decoratedConsumer.getSupplier());
        assertNotNull(decoratedConsumer.getSynchronizer());
        decoratedConsumer.flush();
        assertTrue(consumer.isFlushed());
    }

    @Test
    @DisplayName(value = "OrdinalActiveCollection creation with factory")
    void ordinalActiveCollectionCreationWithFactory() {
        OrdinalActiveCollectionFactory<MockElement, MockInspection> testInstance = new OrdinalActiveCollectionFactory<>(new OrdinalActiveCollectionFactory<>(new ActiveDequeFactory<MockElement, MockInspection>(),
                                                                                                                                                             new MockOrdinal<>()),
                                                                                                                        new MockOrdinal<>());
        final ActiveCollection<MockElement, MockInspection> result = testInstance.createCollection();
        assertNotNull(result);
    }
}
