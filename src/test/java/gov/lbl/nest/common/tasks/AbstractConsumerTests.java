package gov.lbl.nest.common.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.Consumer.Task;
import gov.lbl.nest.common.tasks.MockTask.Type;

/**
 * This class defines the tests that the {@link AbstractConsumerTests} class
 * must pass.
 *
 * @author patton
 */
public abstract class AbstractConsumerTests {

    static class MockProducer extends
                              ContainerProducer<MockTask> {

        private final List<Task> list = new ArrayList<>();

        @Override
        protected boolean isEmpty() {
            return list.isEmpty();
        }

        @Override
        protected Task retrieve() {
            return list.remove(0);
        }

        @Override
        public int size() {
            return list.size();
        }

        @Override
        protected boolean store(Task task) {
            return list.add(task);
        }

    }

    /**
     * The number of {@link Thread} instances to make available during "small"
     * testing.
     */
    private static final int LARGE = 10;

    /**
     * The number of {@link Thread} instances to make available during "small"
     * testing.
     */
    private static final int SMALL = 4;

    /**
     * The number of {@link Task} instances to use during "heavy load" testing.
     */
    private static final int HEAVY = 50;

    /**
     * The number by which to increase the execution limit.
     */
    private static final int LIMIT_DECREASE = 2;

    /**
     * The number by which to increase the execution limit.
     */
    private static final int LIMIT_INCREASE = LIMIT_DECREASE + 1;

    /**
     * The number of tasks to let run before checking that replacements has been
     * pulled.
     */
    private static final int REPLACEMENT_COUNT = 2;

    /**
     * The number of tasks above the limit to initially add to the {@link Producer}
     * instance
     */
    private static final int OVER_COUNT = REPLACEMENT_COUNT * 3;

    /**
     * The number of tasks below the limit to initially add to the {@link Producer}
     * instance
     */
    private static final int BELOW_COUNT = SMALL / 2;

    /**
     * The number of {@link Task} instances to use during "heavy" testing.
     */
    private static final int MULTIPLE = SMALL + LIMIT_INCREASE * 2
                                        + OVER_COUNT;

    /**
     * The number of {@link Task} instances to use during "heavy load" testing.
     */
    private static final int PAUSE = 10;

    /**
     * The {@link Consumer} instance being tested.
     */
    Consumer testInstance;

    /**
     * Test that a {@link Consumer} instance successfully handles a task that throws
     * an exception.
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName(value = "Bad task")
    void badTask() throws InterruptedException {
        final MockProducer producer = new MockProducer();
        testInstance = getTestInstance(SMALL,
                                       producer);
        final MockTask task = new MockTaskImpl(Type.FLAGGED,
                                               new RuntimeException());
        producer.add(task);
        synchronized (task) {
            while (!task.isStarted()) {
                task.wait();
            }
        }
        assertEquals(1,
                     (testInstance.getExecutingCount()).get());
        completeTask(task);
        assertTrue(task.isFailure());
    }

    private void completeTask(final MockTask task) throws InterruptedException {
        task.complete();
        synchronized (task) {
            while (!task.isFinished()) {
                task.wait();
            }
        }
    }

    private void completeTasks(final List<? extends MockTask> tasks,
                               int begin,
                               int end) throws InterruptedException {
        for (MockTask task : tasks.subList(begin,
                                           end)) {
            completeTask(task);
        }
        // Allow the consumer to settle down
        Thread.sleep(10);
    }

    /**
     * Decrease the execution limit of the consumer by the specified number of
     * counts.
     *
     * @param count
     *            the number of count by which to decrease the execution limit.
     */
    protected abstract void decreaseExecutionLimit(int count);

    /**
     * Test that a {@link Consumer} instance successfully handle the case when there
     * is an attempt to reduced the execution limit to less than zero.
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName("Execution limit lower bound")
    void executionLimitLowerBound() throws InterruptedException {
        final MockProducer producer = new MockProducer();
        testInstance = getTestInstance(1,
                                       producer);
        assertEquals(1,
                     testInstance.getExecutionLimit());
        decreaseExecutionLimit(2);
        assertEquals(0,
                     testInstance.getExecutionLimit());
    }

    /**
     * Test that a {@link Consumer} instance successfully flush existing threads and
     * create new ones.
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName("Flush with no pending executions")
    void flushWithNoPendingExecutions() throws InterruptedException {
        final MockProducer producer = new MockProducer();
        testInstance = getTestInstance(2,
                                       producer);

        // Create task that will "stall"
        final MockTask task1 = new MockTaskImpl(Type.FLAGGED);
        producer.add(task1);
        synchronized (task1) {
            while (!task1.isStarted()) {
                task1.wait();
            }
        }
        assertEquals(1,
                     (testInstance.getExecutingCount()).get());

        testInstance.flush();
        assertFalse(task1.isFinished());

        completeTask(task1);
        synchronized (task1) {
            while (!task1.isStarted()) {
                task1.wait();
            }
        }
    }

    /**
     * Test that a {@link Consumer} instance successfully flush existing threads and
     * create new ones.
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName("Flush with pending executions")
    void flushWithPendingExecutions() throws InterruptedException {
        final MockProducer producer = new MockProducer();
        testInstance = getTestInstance(1,
                                       producer);

        // Create task that will "stall"
        final MockTask task1 = new MockTaskImpl(Type.FLAGGED);
        producer.add(task1);
        synchronized (task1) {
            while (!task1.isStarted()) {
                task1.wait();
            }
        }
        assertEquals(1,
                     (testInstance.getExecutingCount()).get());

        // Prepare second task
        final MockTask task2 = new MockTaskImpl(Type.FLAGGED);
        producer.add(task2);
        assertFalse(task2.isStarted());
        assertEquals(1,
                     producer.size());

        testInstance.flush();
        synchronized (task2) {
            while (!task2.isStarted()) {
                task2.wait();
            }
        }
        assertEquals(1,
                     (testInstance.getExecutingCount()).get());
        assertEquals(0,
                     producer.size());

        completeTask(task2);
        // Allow the consumer to settle down
        Thread.sleep(1);
        assertEquals(0,
                     (testInstance.getExecutingCount()).get());
        assertFalse(task1.isFinished());

        completeTask(task1);
        synchronized (task1) {
            while (!task1.isStarted()) {
                task1.wait();
            }
        }
    }

    /**
     * Returns the instance to be tested.
     *
     * @param initialCount
     *            the initial number of {@link Task} instances that can be executing
     *            at the same time.
     * @param producer
     *            the {@link Producer} instance to use when creating the
     *            {@link Consumer} instance.
     *
     * @return the instance to be tested.
     */
    protected abstract Consumer getTestInstance(int initialCount,
                                                Producer<MockTask> producer);

    /**
     * Test that a {@link Consumer} instance successfully executes under a heavy
     * load of {@link Task} instances.
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName("Heavy Load")
    void heavyLoad() throws InterruptedException {
        final MockProducer producer = new MockProducer();
        testInstance = getTestInstance(LARGE,
                                       producer);
        final List<MockTask> tasks = new ArrayList<>();
        for (int count = 0;
             HEAVY != count;
             ++count) {
            tasks.add(new MockTaskImpl(Type.TIMED,
                                       PAUSE));
        }

        /*
         * Load all the tasks into the producer and let them be consumed
         */
        for (Task task : tasks) {
            producer.add(task);
        }

        final MockTask first = tasks.get(0);
        while (!first.isStarted()) {
            Thread.sleep(1);
        }

        final MockTask last = tasks.get(tasks.size() - 1);
        while (!last.isFinished()) {
            Thread.sleep(1);
        }

        Thread.sleep(PAUSE);

        for (MockTask task : tasks) {
            assertTrue(task.isFinished());
        }

        assertTrue(producer.isEmpty());
    }

    /**
     * Increase the execution limit of the consumer by the specified number of
     * counts.
     *
     * @param count
     *            the number of count by which to increase the execution limit.
     */
    protected abstract void increaseExecutionLimit(int count);

    /**
     * Test that a {@link Consumer} instance successfully executes multiple
     * {@link Task} instances where less tasks are added than the current execution
     * limit.
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName("Multiple execution below execution limit")
    void multipleExecutionsBelowExecutionLimit() throws InterruptedException {
        final MockProducer producer = new MockProducer();
        testInstance = getTestInstance(SMALL,
                                       producer);
        assertEquals(SMALL,
                     testInstance.getExecutionLimit());
        assertEquals(0,
                     (testInstance.getExecutingCount()).get());

        // Create Task instances.
        final List<MockTask> tasks = new ArrayList<>();
        for (int count = 0;
             MULTIPLE != count;
             ++count) {
            tasks.add(new MockTaskImpl(Type.FLAGGED));
        }

        // Load more into the producer than the consumer can consume.
        final int loaded = SMALL - BELOW_COUNT;
        for (Task task : tasks.subList(0,
                                       loaded)) {
            producer.add(task);
        }

        // Wait for the initial tasks to start.
        waitForTasksToStart(tasks,
                            0,
                            loaded);
        assertEquals(loaded,
                     (testInstance.getExecutingCount()).get());
        waitForActiveCount(loaded);

        // Allow first task to complete.
        completeTasks(tasks,
                      0,
                      1);
        assertEquals(loaded - 1,
                     (testInstance.getExecutingCount()).get());

        // Wait for excess thread to terminate.
        waitForActiveCount(loaded - 1);

        // Add in new task to "revive" terminated thread.
        final int loaded2 = loaded + 2;
        for (Task task : tasks.subList(loaded,
                                       loaded2)) {
            producer.add(task);
        }

        // Wait for the "new" tasks to start.
        waitForTasksToStart(tasks,
                            1,
                            loaded2);
        assertEquals(loaded2 - 1,
                     (testInstance.getExecutingCount()).get());
        waitForActiveCount(loaded2 - 1);

        // Allow current tasks to complete
        completeTasks(tasks,
                      1,
                      loaded2);
        assertEquals(0,
                     (testInstance.getExecutingCount()).get());
        waitForActiveCount(0);

        runRemainingTasks(producer,
                          tasks,
                          loaded2);
    }

    /**
     * Test that a {@link Consumer} instance successfully executes multiple
     * {@link Task} instances where more tasks are added than the current execution
     * limit.
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName("Multiple execution over execution limit")
    void multipleExecutionsOverExecutionLimit() throws InterruptedException {
        final MockProducer producer = new MockProducer();
        testInstance = getTestInstance(SMALL,
                                       producer);
        assertEquals(SMALL,
                     testInstance.getExecutionLimit());
        assertEquals(0,
                     (testInstance.getExecutingCount()).get());

        // Create Task instances.
        final List<MockTask> tasks = new ArrayList<>();
        for (int count = 0;
             MULTIPLE != count;
             ++count) {
            tasks.add(new MockTaskImpl(Type.FLAGGED));
        }

        // Load more into the producer than the consumer can consume.
        final int loaded = SMALL + OVER_COUNT;
        for (Task task : tasks.subList(0,
                                       loaded)) {
            producer.add(task);
        }

        // Wait for the initial tasks to start
        waitForTasksToStart(tasks,
                            0,
                            SMALL);
        assertEquals(SMALL,
                     (testInstance.getExecutingCount()).get());

        // Allow first tasks to complete
        completeTasks(tasks,
                      0,
                      REPLACEMENT_COUNT);

        // Wait for "new" tasks to start
        waitForTasksToStart(tasks,
                            SMALL,
                            SMALL + REPLACEMENT_COUNT);
        assertEquals(SMALL,
                     (testInstance.getExecutingCount()).get());

        // Test that "new" Tasks has been pulled
        assertEquals(loaded - (SMALL + REPLACEMENT_COUNT),
                     producer.size());

        runRemainingTasks(producer,
                          tasks,
                          loaded);
    }

    /**
     * Test that a {@link Consumer} instance successfully stops, temporarily,
     * consumption of tasks, and then allows consumption to proceed.
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName("Pause and Proceed")
    void pauseAndProceed() throws InterruptedException {
        final MockProducer producer = new MockProducer();
        testInstance = getTestInstance(SMALL,
                                       producer);
        assertEquals(SMALL,
                     testInstance.getExecutionLimit());
        assertEquals(0,
                     (testInstance.getExecutingCount()).get());

        // Create Task instances.
        final List<MockTask> tasks = new ArrayList<>();
        for (int count = 0;
             MULTIPLE != count;
             ++count) {
            tasks.add(new MockTaskImpl(Type.FLAGGED));
        }

        // Load more into the producer than the consumer can consume.
        final int loaded = SMALL + OVER_COUNT;
        for (Task task : tasks.subList(0,
                                       loaded)) {
            producer.add(task);
        }

        // Wait for the initial tasks to start
        waitForTasksToStart(tasks,
                            0,
                            SMALL);
        assertEquals(SMALL,
                     (testInstance.getExecutingCount()).get());

        // Pause consumption
        testInstance.pause();

        // Allow first tasks to complete
        completeTasks(tasks,
                      0,
                      REPLACEMENT_COUNT);

        // Confirm no new tasks have started.
        assertEquals(SMALL - REPLACEMENT_COUNT,
                     (testInstance.getExecutingCount()).get());

        // Allow remaining executing tasks to complete
        completeTasks(tasks,
                      REPLACEMENT_COUNT,
                      SMALL);

        // Confirm no new tasks have started.
        assertEquals(0,
                     (testInstance.getExecutingCount()).get());

        // Proceed to consume new tasks
        testInstance.proceed();

        // Wait for "new" tasks to start
        waitForTasksToStart(tasks,
                            SMALL,
                            SMALL * 2);
        assertEquals(SMALL,
                     (testInstance.getExecutingCount()).get());

        // Test that "new" Tasks has been pulled
        assertEquals(loaded - (SMALL * 2),
                     producer.size());

        runRemainingTasks(producer,
                          tasks,
                          loaded);
    }

    /**
     * Runs the remaining tasks as supplied in the list.
     *
     * @param producer
     *            the {@link Producer} instance into which the {@link Task}
     *            instances should be added.
     * @param tasks
     *            the list of all {@link Task} instances.
     * @param loaded
     *            the index to the first remaining task.
     *
     * @throws InterruptedException
     *             if this method is interrupted.
     */
    protected void runRemainingTasks(final MockProducer producer,
                                     final List<? extends MockTask> tasks,
                                     final int loaded) throws InterruptedException {
        // Run the remaining tasks.
        for (Task task : tasks.subList(loaded,
                                       tasks.size())) {
            producer.add(task);
        }

        completeTasks(tasks,
                      REPLACEMENT_COUNT,
                      tasks.size());
        assertTrue(producer.isEmpty());

        waitForActiveCount(0);
    }

    /**
     * Test that a {@link Consumer} instance successfully executes a {@link Task}
     * instance.
     *
     * @throws InterruptedException
     */
    @Test
    @DisplayName("Single Execution")
    void singleExecution() throws InterruptedException {
        final MockProducer producer = new MockProducer();
        testInstance = getTestInstance(SMALL,
                                       producer);
        assertSame(producer,
                   testInstance.getSupplier());
        assertNotNull(testInstance.getSynchronizer());
        assertEquals(0,
                     producer.size());
        assertEquals(SMALL,
                     testInstance.getExecutionLimit());
        final MockTask task = new MockTaskImpl(Type.FLAGGED);
        producer.add(task);
        synchronized (task) {
            while (!task.isStarted()) {
                task.wait();
            }
        }
        assertEquals(0,
                     producer.size());
        assertEquals(1,
                     (testInstance.getExecutingCount()).get());
        assertEquals(SMALL,
                     testInstance.getExecutionLimit());
        completeTask(task);
        assertEquals(0,
                     (testInstance.getExecutingCount()).get());
        assertEquals(SMALL,
                     testInstance.getExecutionLimit());

        assertTrue(producer.isEmpty());
    }

    @Test
    @DisplayName("Variable Execution Limit")
    void variableExecutionLimit() throws InterruptedException {
        final MockProducer producer = new MockProducer();
        testInstance = getTestInstance(SMALL,
                                       producer);

        // Create Task instances.
        final List<MockTask> tasks = new ArrayList<>();
        for (int count = 0;
             MULTIPLE != count;
             ++count) {
            tasks.add(new MockTaskImpl(Type.FLAGGED));
        }

        // Load more into the producer than the consumer can consume.
        final int loaded = SMALL + OVER_COUNT;
        for (Task task : tasks.subList(0,
                                       loaded)) {
            producer.add(task);
        }

        // Wait for the initial tasks to start
        waitForTasksToStart(tasks,
                            0,
                            SMALL);

        increaseExecutionLimit(LIMIT_INCREASE);
        assertEquals(SMALL + LIMIT_INCREASE,
                     testInstance.getExecutionLimit());

        // Wait for "new" tasks to start
        waitForTasksToStart(tasks,
                            SMALL,
                            SMALL + LIMIT_INCREASE);
        assertEquals(SMALL + LIMIT_INCREASE,
                     (testInstance.getExecutingCount()).get());

        // Test that "new" Tasks has been pulled
        assertEquals(loaded - (SMALL + LIMIT_INCREASE),
                     producer.size());

        decreaseExecutionLimit(LIMIT_DECREASE);
        assertEquals(SMALL + LIMIT_INCREASE
                     - LIMIT_DECREASE,
                     testInstance.getExecutionLimit());

        // Allow enough task to complete to reduce execution count
        completeTasks(tasks,
                      0,
                      LIMIT_DECREASE);
        assertEquals(SMALL + LIMIT_INCREASE
                     - LIMIT_DECREASE,
                     (testInstance.getExecutingCount()).get());

        runRemainingTasks(producer,
                          tasks,
                          loaded);
    }

    /**
     * Waits for any excess threads to be terminated.
     *
     * @param target
     *            the target for the active count.
     *
     * @throws InterruptedException
     *             when this method is interrupted.
     */
    protected abstract void waitForActiveCount(int target) throws InterruptedException;

    private void waitForTasksToStart(final List<? extends MockTask> tasks,
                                     int begin,
                                     int end) throws InterruptedException {
        for (MockTask task : tasks.subList(begin,
                                           end)) {
            synchronized (task) {
                while (!task.isStarted()) {
                    task.wait();
                }
            }
        }
    }

}
