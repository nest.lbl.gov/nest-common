package gov.lbl.nest.common.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * This class defines the common tests that any {@link ActiveCollection}
 * implementation must pass.
 *
 * @author patton
 *
 * @param <E>
 *            the type of Object contained in this container.
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public abstract class AbstractActiveCollectionTests<E extends Element<S>, S> {

    /**
     * Makes the supplied element active.
     *
     * @param element
     *            the {@link Element} instance to be activated.
     */
    protected abstract void activateElement(E element);

    @Test
    @DisplayName("Basic Inspection")
    void basicInspection() {

        final ActiveCollection<E, S> testInstance = getTestInstance();
        final List<? extends E> unorderedElements = getUnorderedElements();
        assertNotNull(unorderedElements,
                      "Elements not provide for test");
        final E element = (unorderedElements.iterator()).next();
        testInstance.add(element);
        assertFalse(testInstance.isEmpty(),
                    "Collection is empty");
        assertEquals(1,
                     testInstance.size(),
                     "Collection is the wrong size");
        final List<S> active = testInstance.inspectActive();
        assertEquals(1,
                     active.size(),
                     "Collection has the wrong number of Inspections");
        final List<S> all = testInstance.inspectAll();
        assertEquals(1,
                     all.size(),
                     "Collection has the wrong number of Inspections");
        final S expected = element.inspect();
        final S note = (all.iterator()).next();
        assertEquals(expected,
                     note);
        final E next = testInstance.next();
        assertSame(element,
                   next,
                   "Wrong execution instance returned");
        assertTrue(testInstance.isEmpty(),
                   "Collection is empty");
        assertEquals(0,
                     testInstance.size(),
                     "Collection is the wrong size");
    }

    /**
     * Returns the instance to be tested.
     *
     * @return the {@link ActiveCollection} instance to be tested.
     */
    protected abstract ActiveCollection<E, S> getTestInstance();

    /**
     * Returns the unordered list of {@link Element} instance to be used.
     *
     * @return the unordered list of {@link Element} instance to be used.
     */
    protected abstract List<? extends E> getUnorderedElements();

    @Test
    @DisplayName("Ignore Inactive")
    void ignoreInactive() {
        final List<? extends E> mockElements = getUnorderedElements();

        final ActiveCollection<E, S> testInstance = getTestInstance();
        final List<E> active = new ArrayList<>();
        final List<E> inactive = new ArrayList<>();
        for (E element : mockElements) {
            if (element.isActive()) {
                active.add(element);
            } else {
                inactive.add(element);
            }
            testInstance.add(element);
        }

        assertFalse(testInstance.isEmpty());
        assertEquals(mockElements.size(),
                     testInstance.size());
        assertEquals(active.size(),
                     testInstance.activeSize());
        assertEquals(inactive.size(),
                     testInstance.inactiveSize());

        final List<E> initiallyActive = new ArrayList<>();
        E actual = testInstance.next();
        while (null != actual) {
            initiallyActive.add(actual);
            actual = testInstance.next();
        }
        assertTrue(testInstance.isEmpty());
        assertEquals(active.size(),
                     initiallyActive.size());

        testInstance.reset();
        for (E element : inactive) {
            activateElement(element);
        }
        if (inactive.isEmpty()) {
            assertTrue(testInstance.isEmpty());
        } else {
            assertFalse(testInstance.isEmpty());
        }

        final List<E> nowActive = new ArrayList<>();
        actual = testInstance.next();
        while (null != actual) {
            nowActive.add(actual);
            actual = testInstance.next();
        }
        assertTrue(testInstance.isEmpty());
        assertEquals(inactive.size(),
                     nowActive.size());

    }

}
