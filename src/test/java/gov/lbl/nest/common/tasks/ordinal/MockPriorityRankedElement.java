package gov.lbl.nest.common.tasks.ordinal;

/**
 * This class is used to test that priority and ranking of elements works
 * correctly.
 *
 * @author patton
 */
public class MockPriorityRankedElement extends
                                       MockUninspectedRankedElement<MockPriorityRankedInspector> implements
                                       PriorityRankedElement<MockPriorityRankedInspector> {

    /**
     * The "priority" of this element, where object with are higher priority a
     * preferred.
     */
    private final Integer priority;

    /**
     * Creates an instance of this class.
     *
     * @param priority
     *            the "priority" of this element, where object with are higher
     *            priority a preferred.
     * @param depth
     *            the "rank" of this element, where object with a higher rank are
     *            preferred.
     */
    public MockPriorityRankedElement(Integer priority,
                                     Integer depth) {
        super(depth,
              new MockPriorityRankedInspector());
        this.priority = priority;
    }

    @Override
    public Integer getPriority() {
        return priority;
    }
}
