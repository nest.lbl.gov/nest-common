package gov.lbl.nest.common.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * This class defines the tests that the {@link OrdinalActiveCollection} class
 * must pass.
 *
 * @author patton
 *
 * @param <E>
 *            the sub-type of the {@link Element} class that is stored in the
 *            collections.
 *
 * @param <S>
 *            the type of class to be returned by the inspect method of the E
 *            type.
 */
public abstract class AbstractOrdinalActiveCollectionTests<E extends Element<S>, S> extends
                                                          AbstractActiveCollectionTests<E, S> {

    /**
     * Returns the sequence containing the expected order of the collection returned
     * by {@link #getUnorderedElements()}
     *
     * @return the sequence containing the expected order of the collection returned
     *         by {@link #getUnorderedElements()}
     */
    protected abstract List<? extends E> getOrderedElements();

    /**
     * Returns the instance to be tested.
     */
    @Override
    protected abstract OrdinalActiveCollection<E, S> getTestInstance();

    /**
     * Tests that the elements are delivered in the correct order.
     */
    @Test
    @DisplayName("Ordered Delivery")
    void rankOrderedDelivery() {

        final OrdinalActiveCollection<E, S> testInstance = getTestInstance();

        final List<? extends E> unorderedTasks = getUnorderedElements();
        for (E task : unorderedTasks) {
            testInstance.add(task);
        }

        assertFalse(testInstance.isEmpty(),
                    "Collection is empty");
        assertEquals(unorderedTasks.size(),
                     testInstance.size(),
                     "Collection is the wrong size");
        final List<S> inspections = testInstance.inspectAll();
        assertEquals(unorderedTasks.size(),
                     inspections.size(),
                     "Collection has the wrong number of inspections");

        final List<? extends E> expectedOrder = getOrderedElements();
        final Iterator<? extends E> tasksIterator = expectedOrder.iterator();
        for (S inspection : inspections) {
            final S expected = (tasksIterator.next()).inspect();
            assertEquals(expected,
                         inspection);
        }

        int count = 1;
        for (E element : expectedOrder) {
            final E next = testInstance.next();
            assertSame(element,
                       next,
                       "Wrong execution instance returned");
            assertEquals(unorderedTasks.size() - count,
                         testInstance.size(),
                         "Collection is the wrong size");
            ++count;
        }
        assertTrue(testInstance.isEmpty(),
                   "Collection is empty");

        testInstance.next();
        testInstance.next();
    }

}
