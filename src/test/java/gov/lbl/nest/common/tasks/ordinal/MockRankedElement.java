package gov.lbl.nest.common.tasks.ordinal;

/**
 * This class is used to test that ranking of elements works correctly.
 *
 * @author patton
 */
public class MockRankedElement extends
                               MockUninspectedRankedElement<MockRankedInspector> {

    /**
     * Creates an instance of this class.
     *
     * @param depth
     *            the "rank" of this element, where object with a higher rank are
     *            preferred.
     */
    public MockRankedElement(Integer depth) {
        super(depth,
              new MockRankedInspector());
    }
}
