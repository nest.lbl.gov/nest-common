package gov.lbl.nest.common.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.queued.QueueableImpl;
import gov.lbl.nest.common.tasks.MockTask.Type;
import gov.lbl.nest.common.tasks.TaskTracker.StateChange;

/**
 * This class defines the common tests the {@link TaskTracker} class must pass.
 *
 * @author patton
 *
 * @param <T>
 *            the type of the {@link MockTask} class being tested.
 */
public abstract class AbstractTaskTrackerTests<T extends MockTask> {

    /**
     * The {@link QueueableImpl} instance being tested.
     */
    TaskTracker<T> testInstance;

    /**
     * Creates a new {@link MockTask} instance to be used in the tests.
     *
     * @param type
     *            the {@link Type} instance of the {@link MockTask} instance to be
     *            created
     *
     * @return the new {@link MockTask} instance to be used in the tests.
     */
    protected abstract T createTask(Type type);

    @Test
    @DisplayName("Drop Single Execution")
    void dropSingleExecution() throws InterruptedException {
        simpleConstruction();
        singleExecution(false);
    }

    /**
     * Returns the instance to be tested.
     *
     * @return the {@link TaskTracker} instance to be tested.
     */
    protected abstract TaskTracker<T> getTestInstance();

    /**
     * Returns the instance to be tested.
     *
     * @param stateChange
     *            the {@link StateChange} instance that the returned
     *            {@link TaskTracker} should use.
     *
     * @return the {@link TaskTracker} instance to be tested.
     */
    protected abstract TaskTracker<T> getTestInstance(StateChange<T> stateChange);

    @Test
    @DisplayName("Keeper Construction")
    void keeperConstruction() {
        testInstance = getTestInstance(new StateChange<T>() {

            @Override
            public boolean finished(T task) {
                return true;
            }

            @Override
            public void queued(T task) {
            }

            @Override
            public void starting(T task) {
            }

        });
        assertNotNull(testInstance);
    }

    @Test
    @DisplayName("Drop Single Execution")
    void keepSingleExecution() throws InterruptedException {
        keeperConstruction();
        singleExecution(true);
    }

    @Test
    @DisplayName("Simple Construction")
    void simpleConstruction() {
        testInstance = getTestInstance();
        assertNotNull(testInstance);
    }

    @SuppressWarnings("unchecked")
    private void singleExecution(boolean keep) throws InterruptedException {
        final T task = createTask(Type.FLAGGED);

        final T wrappedTask = (T) testInstance.trackTask(task,
                                                         MockTask.class);
        final List<T> waiting = testInstance.getWaiting();
        assertEquals(1,
                     waiting.size());
        assertSame(task,
                   waiting.get(0));
        assertTrue((testInstance.getExecuting()).isEmpty());
        assertTrue((testInstance.getTerminated()).isEmpty());

        wrappedTask.beginExecution();
        final Thread thread = new Thread(wrappedTask);
        thread.start();
        while (!task.isStarted()) {
            Thread.sleep(1);
        }
        assertTrue((testInstance.getWaiting()).isEmpty());
        final List<T> executing = testInstance.getExecuting();
        assertEquals(1,
                     executing.size());
        assertSame(task,
                   executing.get(0));
        assertTrue((testInstance.getTerminated()).isEmpty());

        task.complete();
        while (!task.isFinished()) {
            Thread.sleep(1);
        }

        wrappedTask.endExecution();
        assertTrue(task.isFinished());
        assertTrue((testInstance.getWaiting()).isEmpty());
        assertTrue((testInstance.getExecuting()).isEmpty());
        if (keep) {
            final List<T> terminated = testInstance.getTerminated(false);
            assertEquals(1,
                         terminated.size());
            assertSame(task,
                       terminated.get(0));
            assertEquals(1,
                         (testInstance.getTerminated()).size());

        } else {
            final List<T> terminated = testInstance.getTerminated(false);
            assertTrue(terminated.isEmpty());
        }
        assertTrue((testInstance.getTerminated()).isEmpty());
    }

}
