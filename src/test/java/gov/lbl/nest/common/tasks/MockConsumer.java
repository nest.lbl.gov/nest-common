package gov.lbl.nest.common.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class implements the {@link Consumer} interface for use in testing. It
 * requires consumption of {@link gov.lbl.nest.common.tasks.Consumer.Task}
 * instances to be explicitly triggers by calling the {@link #consume()} method.
 *
 * @author patton
 *
 */
public class MockConsumer implements
                          Consumer {

    class RunTask implements
                  Runnable {

        private final Task task;

        private Thread thread;

        RunTask(Task task) {
            this.task = task;
        }

        @Override
        public void run() {
            totalThreadCount.incrementAndGet();
            synchronized (executionCount) {
                executionCount.add(thread);
            }
            task.run();
            synchronized (executionCount) {
                executionCount.remove(thread);
            }
            totalThreadCount.decrementAndGet();
        }

        void setThread(Thread thread) {
            this.thread = thread;
        }

    }

    /**
     * The {@link Producer} instance used by this object.
     */
    private final Producer<?> producer;

    private final List<Thread> executionCount = new ArrayList<>();

    private final AtomicInteger totalThreadCount = new AtomicInteger();

    /**
     * Create an instance of this class.
     *
     * @param producer
     *            the {@link gov.lbl.nest.common.tasks.Consumer.Producer} instance
     *            used by this object.
     */
    public MockConsumer(Producer<?> producer) {
        this.producer = producer;
        producer.consumer(this);
    }

    @Override
    public boolean addedTask() {
        return true;
    }

    /**
     * Returns the number of {@link gov.lbl.nest.common.tasks.Consumer.Task}
     * instances that are consumed.
     *
     * @return the number of {@link gov.lbl.nest.common.tasks.Consumer.Task}
     *         instances that are consumed.
     */
    public Integer consume() {
        return consume(producer.size());
    }

    /**
     * Returns the number of {@link gov.lbl.nest.common.tasks.Consumer.Task}
     * instances that are consumed.
     *
     * @param count
     *            the number of {@link gov.lbl.nest.common.tasks.Consumer.Task}
     *            instances to attempt to consume.
     *
     * @return the number of {@link gov.lbl.nest.common.tasks.Consumer.Task}
     *         instances that are consumed.
     */
    public Integer consume(int count) {
        int consumed = 0;
        Task task = producer.next();
        while (null != task && count != consumed) {
            if (null != task) {
                final RunTask taskToUse = new RunTask(task);
                final Thread thread = new Thread(taskToUse);
                taskToUse.setThread(thread);
                thread.start();
            }
            ++consumed;
            if (count != consumed) {
                task = producer.next();
            }
        }
        return consumed;
    }

    @Override
    public void flush() {
        synchronized (executionCount) {
            executionCount.clear();
        }
    }

    @Override
    public AtomicInteger getExecutingCount() {
        synchronized (executionCount) {
            return new AtomicInteger(executionCount.size());
        }
    }

    @Override
    public int getExecutionLimit() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Supplier<?> getSupplier() {
        return producer.getSupplier();
    }

    @Override
    public Object getSynchronizer() {
        return producer;
    }

    /**
     * Returns the total thread count.
     *
     * @return the total thread count.
     */
    public int getTotalThreadCount() {
        return totalThreadCount.intValue();
    }

    @Override
    public boolean pause() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean proceed() {
        throw new UnsupportedOperationException();
    }

}
