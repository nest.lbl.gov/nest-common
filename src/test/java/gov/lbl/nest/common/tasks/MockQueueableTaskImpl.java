package gov.lbl.nest.common.tasks;

import java.time.Duration;
import java.util.Date;

import gov.lbl.nest.common.queued.QueueableImpl;

/**
 * This class implements the {@link MockQueueableTask} interface.
 *
 * @author patton
 */
public class MockQueueableTaskImpl extends
                                   MockTaskImpl implements
                                   MockQueueableTask {

    final QueueableImpl queueableImpl = new QueueableImpl();

    MockQueueableTaskImpl(Type type) {
        super(type);
    }

    @Override
    public void beginExecution() {
        super.beginExecution();
    }

    @Override
    public void endExecution() {
        super.endExecution();
    }

    @Override
    public Duration getExecutionDuration() {
        return queueableImpl.getExecutionDuration();
    }

    @Override
    public Duration getTotalDuration() {
        return queueableImpl.getTotalDuration();
    }

    @Override
    public Duration getWaitDuration() {
        return queueableImpl.getWaitDuration();
    }

    @Override
    public Date getWhenCreated() {
        return queueableImpl.getWhenCreated();
    }

    @Override
    public Date getWhenFinished() {
        return queueableImpl.getWhenFinished();
    }

    @Override
    public Date getWhenStarted() {
        return queueableImpl.getWhenStarted();
    }

    @Override
    public void setWhenFinished(Date dateTime) {
        queueableImpl.setWhenFinished(dateTime);
    }

    @Override
    public void setWhenStarted(Date dateTime) {
        queueableImpl.setWhenStarted(dateTime);
        super.setWhenStarted(dateTime);
    }
}
