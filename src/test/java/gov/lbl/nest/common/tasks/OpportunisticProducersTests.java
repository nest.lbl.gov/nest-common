package gov.lbl.nest.common.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.MockTask.Type;

/**
 * This class test that a {@link OpportunisticProducers} instance fulfills its
 * requirements.
 *
 * @author patton
 */
@DisplayName("OpportunisticProducers class")
public class OpportunisticProducersTests {

    /**
     * The number of task to create for use in testing.
     */
    private static final int TASK_CREATION_COUNT = 3;

    private void completeTask(final MockTask task) throws InterruptedException {
        task.complete();
        synchronized (task) {
            while (!task.isFinished()) {
                task.wait();
            }
        }
    }

    private void completeTasks(final List<? extends MockTask> tasks,
                               int begin,
                               int end) throws InterruptedException {
        for (MockTask task : tasks.subList(begin,
                                           end)) {
            completeTask(task);
        }
        // Allow the consumer to settle down
        Thread.sleep(1);
    }

    @Test
    @DisplayName(value = "Construction")
    void constuction() {
        Producer<MockTask> doubleInbound = new FifoProducer<>();
        Producer<MockTask> singleInbound = new FifoProducer<>();
        OpportunisticProducers<MockTask> producers = new OpportunisticProducers<>(doubleInbound,
                                                                                  singleInbound);
        assertNotNull(producers.getSingleProducer());
        assertNotNull(producers.getDoubleProducer());
    }

    private void consumeTasks(MockConsumer consumer,
                              Producer<MockTask> producer,
                              int initialSize,
                              List<? extends MockTask> tasks,
                              int begin,
                              int end) throws InterruptedException {
        assertEquals(initialSize,
                     producer.size());
        final int count = end - begin;
        consumer.consume(count);
        waitForTasksToStart(tasks,
                            begin,
                            end);
        assertEquals(initialSize - count,
                     producer.size());

    }

    /**
     * Creates the number of requested {@link MockTask} instances.
     *
     * @param count
     *            the number of {@link MockTask} instances to create.
     *
     * @return the created {@link MockTask} instances.
     */
    protected List<MockTask> createTasks(int count) {
        final List<MockTask> tasks = new ArrayList<>();
        for (int index = 0;
             count != index;
             ++index) {
            tasks.add(new MockTaskImpl(Type.FLAGGED));
        }
        return tasks;
    }

    @Test
    @DisplayName(value = "Opportunistic executions")
    void opportunisticExecutions() throws InterruptedException {
        FifoProducer<MockTask> doubleInbound = new FifoProducer<>();
        FifoProducer<MockTask> singleInbound = new FifoProducer<>();
        OpportunisticProducers<MockTask> producers = new OpportunisticProducers<>(doubleInbound,
                                                                                  singleInbound);
        MockConsumer singleConsumer = new MockConsumer(producers.getSingleProducer());
        MockConsumer doubleConsumer = new MockConsumer(producers.getDoubleProducer());

        final List<MockTask> tasks = createTasks(TASK_CREATION_COUNT);

        // Load up single consumer and consumer first Task.
        assertEquals(0,
                     (singleConsumer.getExecutingCount()).get());
        doubleInbound.add(tasks.get(0));
        doubleInbound.add(tasks.get(1));
        assertEquals(2,
                     doubleInbound.size());

        // Load up double consumer.
        assertEquals(0,
                     (doubleConsumer.getExecutingCount()).get());
        singleInbound.add(tasks.get(2));
        assertEquals(1,
                     singleInbound.size());

        // Check that each Consumer consumes from the correct Producer
        consumeTasks(singleConsumer,
                     doubleInbound,
                     2,
                     tasks,
                     0,
                     1);
        assertEquals(1,
                     (singleConsumer.getExecutingCount()).get());

        consumeTasks(doubleConsumer,
                     singleInbound,
                     1,
                     tasks,
                     2,
                     3);
        assertEquals(1,
                     (doubleConsumer.getExecutingCount()).get());

        // Check opportunistic consumption by doubleInbound by doubleConsumer.
        consumeTasks(doubleConsumer,
                     doubleInbound,
                     1,
                     tasks,
                     1,
                     2);
        assertEquals(2,
                     (doubleConsumer.getExecutingCount()).get());

        // Clean up
        completeTasks(tasks,
                      0,
                      3);
        assertEquals(0,
                     singleConsumer.getTotalThreadCount());
        assertEquals(0,
                     doubleConsumer.getTotalThreadCount());

    }

    @Test
    @DisplayName(value = "Reverse consumer binding")
    void reverseConsumerBinding() {
        Producer<MockTask> doubleInbound = new FifoProducer<>();
        Producer<MockTask> singleInbound = new FifoProducer<>();
        OpportunisticProducers<MockTask> producers = new OpportunisticProducers<>(doubleInbound,
                                                                                  singleInbound);
        new MockConsumer(producers.getSingleProducer());
        new MockConsumer(producers.getDoubleProducer());
    }

    @Test
    @DisplayName(value = "Shared single consumer")
    void sharedSingleConsumer() throws InterruptedException {
        FifoProducer<MockTask> doubleInbound = new FifoProducer<>();
        Producer<MockTask> singleInbound = new FifoProducer<>();
        OpportunisticProducers<MockTask> producers = new OpportunisticProducers<>(doubleInbound,
                                                                                  singleInbound);
        MockConsumer doubleConsumer = new MockConsumer(producers.getDoubleProducer());
        final List<MockTask> tasks = createTasks(1);
        doubleInbound.add(tasks.get(0));
        consumeTasks(doubleConsumer,
                     doubleInbound,
                     1,
                     tasks,
                     0,
                     1);
        assertEquals(1,
                     (doubleConsumer.getExecutingCount()).get());
        completeTasks(tasks,
                      0,
                      1);
        assertEquals(0,
                     doubleConsumer.getTotalThreadCount());
    }

    @Test
    @DisplayName(value = "Standard consumer binding")
    void standardConsumerBinding() {
        Producer<MockTask> doubleInbound = new FifoProducer<>();
        Producer<MockTask> singleInbound = new FifoProducer<>();
        OpportunisticProducers<MockTask> producers = new OpportunisticProducers<>(doubleInbound,
                                                                                  singleInbound);
        final Producer<MockTask> doubleProducer = producers.getDoubleProducer();
        final MockConsumer doubleConsumer = new MockConsumer(doubleProducer);
        final Producer<MockTask> singleProducer = producers.getSingleProducer();
        final MockConsumer singleConsumer = new MockConsumer(singleProducer);
        assertSame(doubleInbound,
                   singleConsumer.getSupplier());
        assertSame(singleInbound,
                   doubleConsumer.getSupplier());
    }

    private void waitForTasksToStart(final List<? extends MockTask> tasks,
                                     int begin,
                                     int end) throws InterruptedException {
        for (MockTask task : tasks.subList(begin,
                                           end)) {
            synchronized (task) {
                while (!task.isStarted()) {
                    task.wait();
                }
            }
        }
    }
}
