package gov.lbl.nest.common.watching;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.testing.UnitTestUtility;

/**
 * This class test that a {@link Command} instance fulfills its requirements.
 *
 * @author patton
 */
@DisplayName("Digest class")
public class DigestTests {

    /**
     * The format for date and time queries.
     */
    private static final String DATE_AND_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    private static final DateFormat DATE_AND_TIME_FORMATTER = new SimpleDateFormat(DATE_AND_TIME_FORMAT);

    /**
     * The subject used in testing.
     */
    private static final String SUBJECT = "Subject of test instance";

    /**
     * The date and time at or after which items are included in the test object.
     */
    private static final Date BEGIN;

    /**
     * The date and time before which items are included in the test object.
     */
    private static final Date END;

    static {
        Date begin;
        Date end;
        try {
            begin = DATE_AND_TIME_FORMATTER.parse("2012-01-01T00:00:00.000+0000");
            end = DATE_AND_TIME_FORMATTER.parse("2012-01-01T01:23:45.678+0000");
        } catch (ParseException e) {
            begin = null;
            end = null;
            e.printStackTrace();
        }
        BEGIN = begin;
        END = end;
    }

    /**
     * The ordered list of {@link ChangedItem} instances which are included in the
     * test object as being issued.
     */
    private static final List<MockItem> ISSUED = new ArrayList<>();

    /**
     * The ordered list of {@link ChangedItem} instances which are included in the
     * test object as being revoked.
     */
    private static final List<MockItem> REVOKED = new ArrayList<>();

    /**
     * The ordered list of {@link ChangedItem} instances which are included in the
     * test object as queued.
     */
    private static final List<String> QUEUED = new ArrayList<>();

    static {
        try {
            ISSUED.add(new MockItem("Issued Item",
                                    DATE_AND_TIME_FORMATTER.parse("2012-01-01T00:01:23.456+0000")));
            REVOKED.add(new MockItem("Revoked Item",
                                     DATE_AND_TIME_FORMATTER.parse("2012-01-01T00:12:34.567+0000")));
            QUEUED.add("Queued Item");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * The name of the resource contains an example digest.
     */
    private static final String EXAMPLE_RESOURCE = "example-digest.xml";

    /**
     * The file from which to read the test object during tests.
     */
    private static final File SIMPLE_FILE = new File(EXAMPLE_RESOURCE);

    /**
     * The name of the resource contains an example digest containing details.
     */
    private static final String EXAMPLE_2_RESOURCE = "example.2-digest.xml";

    /**
     * The file from which to read the test object during tests.
     */
    private static final File DETAILS_FILE = new File(EXAMPLE_2_RESOURCE);

    /**
     * The name of the resource contains an example digest containing details.
     */
    private static final String RESULTS_RESOURCE = "results-digest.xml";

    /**
     * The name of the resource contains an example digest containing details.
     */
    private static final String RESULTS_2_RESOURCE = "results.2-digest.xml";

    /**
     * The file into which to write the test object during tests.
     */
    private static final File OUTPUT_FILE = new File("digest.xml");

    /**
     * The array for files used in tests.
     */
    private static final File[] FILES = new File[] { SIMPLE_FILE,
                                                     DETAILS_FILE,
                                                     OUTPUT_FILE };

    /**
     * Returns true is the content of the specified {@link Digest} instance exactly
     * matches the supplied information.
     *
     * @param digest
     *            the {@link Digest} instance to test,
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param begin
     *            the date and time at or after which items are included in this
     *            object.
     * @param end
     *            the date and time before which items are included in this object.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     * @param revoked
     *            the list of {@link ChangedItem} instance that were revoked during
     *            the period of this object.
     * @param queued
     *            the list of item that were issued during the period of this
     *            object.
     *
     * @return true is the content of the specified {@link Digest} instance exactly
     *         matches the supplied information.
     */
    private static boolean confirmContent(Digest digest,
                                          String subject,
                                          Date begin,
                                          Date end,
                                          List<MockItem> issued,
                                          List<MockItem> revoked,
                                          List<String> queued) {
        if (null == subject) {
            if (null != digest.getSubject()) {
                return false;
            }
        } else {
            if (!subject.equals(digest.getSubject())) {
                return false;
            }
        }
        if (null == begin) {
            if (null != digest.getBegin()) {
                return false;
            }
        } else {
            if (!begin.equals(digest.getBegin())) {
                return false;
            }
        }
        if (null == end) {
            if (null != digest.getEnd()) {
                return false;
            }
        } else {
            if (!end.equals(digest.getEnd())) {
                return false;
            }
        }
        if (null == issued) {
            if (null != digest.getIssued()) {
                return false;
            }
        } else {
            if (!confirmContents(issued,
                                 digest.getIssued())) {
                return false;
            }
        }
        if (null == revoked) {
            if (null != digest.getRevoked()) {
                return false;
            }
        } else {
            if (!confirmContents(revoked,
                                 digest.getRevoked())) {
                return false;
            }
        }
        if (null == queued) {
            if (null != digest.getQueued()) {
                return false;
            }
        } else {
            if (!queued.equals(digest.getQueued())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns true if the content of the actual list conforms to the specified
     * {@link MockItem} list.
     *
     * @param expected
     *            the list of expected {@link MockItem} instances.
     * @param actual
     *            the list of actual {@link ChangedItem} instances.
     *
     * @return true if the content of the actual list conforms to the specified
     *         {@link MockItem} list.
     */
    private static boolean confirmContents(List<MockItem> expected,
                                           List<? extends ChangedItem> actual) {
        if (expected.size() != actual.size()) {
            return false;
        }
        int index = 0;
        for (MockItem expectedElement : expected) {
            final String expectedItem = expectedElement.getItem();
            final Date expectedTime = expectedElement.getWhenItemChanged();
            final ChangedItem actualElement = actual.get(index++);
            if (!(expectedItem.equals(actualElement.getItem()) && expectedTime.equals(actualElement.getWhenItemChanged()))) {
                return false;
            }
        }
        return true;
    }

    /**
     * The instance being tested.
     */
    private Digest testObject;

    /**
     * Test that a {@link Digest} can be successfully created using its most
     * universal constructor.
     */
    @Test
    @DisplayName("Creation")
    public void creation() {
        testObject = new Digest(SUBJECT,
                                BEGIN,
                                END,
                                ISSUED,
                                REVOKED,
                                null,
                                QUEUED);
        assertTrue(confirmContent(testObject,
                                  SUBJECT,
                                  BEGIN,
                                  END,
                                  ISSUED,
                                  REVOKED,
                                  QUEUED),
                   "Digest did not contain the correct information");
    }

    /**
     * Test that a {@link Digest} can be read from a {@link File}.
     *
     * @throws IOException
     *             when file can not be read.
     * @throws DigestException
     *             when the {@link File} does not contain a {@link Digest} instance.
     */
    @Test
    @DisplayName("Details From File")
    public void detailsFromFile() throws IOException,
                                  DigestException {
        UnitTestUtility.createFileFromResource(getClass(),
                                               DETAILS_FILE);
        testObject = Digest.fromFile(DETAILS_FILE);
        assertTrue(confirmContent(testObject,
                                  SUBJECT,
                                  BEGIN,
                                  END,
                                  ISSUED,
                                  REVOKED,
                                  QUEUED),
                   "Digest did not contain the correct information");
        final List<? extends DigestChange> issued = testObject.getIssued();
        (issued.get(0)).setDetails(null);
        final List<? extends DigestChange> revoked = testObject.getRevoked();
        final List<Detail> details = new ArrayList<>();
        details.add(new Detail("status",
                               "expired"));
        (revoked.get(0)).setDetails(details);
        testObject.toFile(OUTPUT_FILE);
        // TODO : Work out how to test this in a TimeZone independent manner.
        testObject = Digest.fromFile(OUTPUT_FILE);
        assertTrue(UnitTestUtility.identical(DigestTests.class,
                                             RESULTS_2_RESOURCE,
                                             OUTPUT_FILE),
                   "Digest did not contain the correct information");
    }

    /**
     * Test that a {@link Digest} can be read from a {@link File}.
     *
     * @throws IOException
     *             when file can not be read.
     * @throws DigestException
     *             when the {@link File} does not contain a {@link Digest} instance.
     */
    @Test
    @DisplayName("Read From File")
    public void readFromFile() throws IOException,
                               DigestException {
        UnitTestUtility.createFileFromResource(getClass(),
                                               SIMPLE_FILE);
        testObject = Digest.fromFile(SIMPLE_FILE);
        assertTrue(confirmContent(testObject,
                                  SUBJECT,
                                  BEGIN,
                                  END,
                                  ISSUED,
                                  REVOKED,
                                  QUEUED),
                   "Digest did not contain the correct information");
    }

    /**
     * Test that a {@link Digest} can be read from an {@link InputStream}.
     *
     * @throws DigestException
     *             when the {@link File} does not contain a {@link Digest} instance.
     * @throws FileNotFoundException
     *             when the specified file does not exist.
     */
    @Test
    @DisplayName("Read From Input Stream")
    public void readFromInputStream() throws FileNotFoundException,
                                      DigestException {
        testObject = Digest.fromInputStream(UnitTestUtility.getResourceAsStream(getClass(),
                                                                                EXAMPLE_RESOURCE));
        assertTrue(confirmContent(testObject,
                                  SUBJECT,
                                  BEGIN,
                                  END,
                                  ISSUED,
                                  REVOKED,
                                  QUEUED),
                   "Digest did not contain the correct information");
    }

    @BeforeEach
    void setUp() throws Exception {
        UnitTestUtility.deleteFiles(FILES);
        // Reference files are all in PST, so use that timezone for the tests
        TimeZone.setDefault(TimeZone.getTimeZone("PST"));
    }

    /**
     * Test that {@link Digest} can be successfully created using subsidiary
     * constructors.
     */
    @Test
    @DisplayName("Subsidiary Constructors")
    public void subsidiaryConstructors() {
        testObject = new Digest(SUBJECT,
                                ISSUED);
        assertTrue(confirmContent(testObject,
                                  SUBJECT,
                                  null,
                                  null,
                                  ISSUED,
                                  null,
                                  null),
                   "Digest did not contain the correct information");
        testObject = new Digest(SUBJECT,
                                ISSUED,
                                REVOKED);
        assertTrue(confirmContent(testObject,
                                  SUBJECT,
                                  null,
                                  null,
                                  ISSUED,
                                  REVOKED,
                                  null),
                   "Digest did not contain the correct information");
        testObject = new Digest(SUBJECT,
                                ISSUED,
                                REVOKED,
                                null,
                                QUEUED);
        assertTrue(confirmContent(testObject,
                                  SUBJECT,
                                  null,
                                  null,
                                  ISSUED,
                                  REVOKED,
                                  QUEUED),
                   "Digest did not contain the correct information");
        testObject = new Digest(SUBJECT,
                                BEGIN,
                                END,
                                ISSUED);
        assertTrue(confirmContent(testObject,
                                  SUBJECT,
                                  BEGIN,
                                  END,
                                  ISSUED,
                                  null,
                                  null),
                   "Digest did not contain the correct information");
        testObject = new Digest(SUBJECT,
                                BEGIN,
                                END,
                                ISSUED,
                                REVOKED);
        assertTrue(confirmContent(testObject,
                                  SUBJECT,
                                  BEGIN,
                                  END,
                                  ISSUED,
                                  REVOKED,
                                  null),
                   "Digest did not contain the correct information");
    }

    // static member methods (alphabetic)

    @AfterEach
    void tearDown() throws Exception {
        UnitTestUtility.deleteFiles(FILES);
    }

    /**
     * Test that a {@link Digest} can be written to a file.
     *
     * @throws IOException
     *             when the file can not be written.
     * @throws DigestException
     *             when the {@link File} does not contain a {@link Digest} instance.
     */
    @Test
    @DisplayName("Write To File")
    public void writeToFile() throws IOException,
                              DigestException {
        creation();
        testObject.toFile(OUTPUT_FILE);
        // TODO : Work out how to test this in a TimeZone independent manner.
        testObject = Digest.fromFile(OUTPUT_FILE);
        assertTrue(UnitTestUtility.identical(DigestTests.class,
                                             RESULTS_RESOURCE,
                                             OUTPUT_FILE),
                   "Digest did not contain the correct information");
    }

}
