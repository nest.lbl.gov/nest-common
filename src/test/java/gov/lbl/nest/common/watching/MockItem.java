package gov.lbl.nest.common.watching;

import java.util.Date;

/**
 * This class is used as a {@link ChangedItem} implementation for testing.
 *
 * @author patton
 */
public class MockItem implements
                      ChangedItem {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The identity of the item that changed.
     */
    private final String item;

    /**
     * The date and time that the change in the item occurred.
     */
    private final Date whenItemChanged;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param item
     *            the identity of the item that changed.
     * @param dateTime
     *            the date and time that the change in the item occurred.
     */
    private MockItem(final MockItem rhs) {
        this.item = rhs.item;
        whenItemChanged = rhs.whenItemChanged;
    }

    /**
     * Creates an instance of this class.
     *
     * @param item
     *            the identity of the item that changed.
     * @param dateTime
     *            the date and time that the change in the item occurred.
     */
    public MockItem(final String item,
                    final Date dateTime) {
        this.item = item;
        whenItemChanged = dateTime;
    }

    // instance member method (alphabetic)

    @Override
    public ChangedItem clone() {
        return new MockItem(this);
    }

    @Override
    public int compareTo(final ChangedItem o) {
        return whenItemChanged.compareTo(o.getWhenItemChanged());
    }

    @Override
    public String getItem() {
        return item;
    }

    @Override
    public Date getWhenItemChanged() {
        return whenItemChanged;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
