package gov.lbl.nest.common.xml.item;

import java.lang.reflect.Constructor;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;

/**
 * This class is used by {@link ValueAdapter} instance to marshall non-JAXB
 * types as a {@link Value} instance.
 *
 * This should not be confused with the {@link NonJAXBObject} which is a
 * container of a {@link Value} instance, where as this class is used instead of
 * {@link Value} instance.
 *
 * @author patton
 */
@XmlRootElement(name = "value")
public class ValueAsString {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The Java type to which this object is providing access.
     */
    private String type;

    /**
     * The String representation of the Java object.
     */
    private String value;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ValueAsString() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param object
     *            the {@link Object} instance to be put into XML.
     */
    public ValueAsString(Object object) {
        type = (object.getClass()).getName();
        value = object.toString();
    }

    // instance member method (alphabetic)

    /**
     * Returns the Object that is represented by the String.
     *
     * @return the Object that is represented by the String.
     */
    @XmlTransient
    public Object getObject() {
        try {
            final Class<?> clazz = (getClass().getClassLoader()).loadClass(type);
            final Constructor<?> constructor = clazz.getConstructor(String.class);
            return constructor.newInstance(value);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns the Java type to which this object is providing access.
     *
     * @return the Java type to which this object is providing access.
     */
    @XmlAttribute
    protected String getType() {
        return type;
    }

    /**
     * Returns the String representation of the Java object.
     *
     * @return the String representation of the Java object.
     */
    @XmlValue
    public String getValue() {
        return value;
    }

    /**
     * Sets the Java type to which this object is providing access.
     *
     * @param type
     *            the Java type to which this object is providing access.
     */
    protected void setType(String type) {
        this.type = type;
    }

    /**
     * Sets the String representation of the Java object.
     *
     * @param value
     *            the String representation of the Java object.
     */
    protected void setValue(String value) {
        this.value = value;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
