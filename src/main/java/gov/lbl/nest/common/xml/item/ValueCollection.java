package gov.lbl.nest.common.xml.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class is used as a container for a collection of data objects.
 *
 * @author patton
 */
@XmlRootElement(name = "collection")
public class ValueCollection {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The contents of the collection to be saved.
     */
    private Collection<?> content;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the contents of this collection.
     *
     * @return the contents of this collection.
     */
    @XmlAnyElement(lax = true)
    public Collection<?> getContent() {
        return content;
    }

    /**
     * Sets the contents of this collection.
     *
     * @param collection
     *            the collection that contains the content of this object.
     */
    public void setContent(Collection<?> collection) {
        content = collection;
    }

    /**
     * Builds a {@link List} instance from the contents of this object.
     *
     * @return t{@link List} instance built from the contents of this object.
     */
    public List<Object> unwrap() {
        final List<Object> values = new ArrayList<>();
        for (Object value : getContent()) {
            if (value instanceof NullValue) {
                values.add(null);
            } else if (value instanceof ValueAsString) {
                values.add(((ValueAsString) value).getObject());
            } else if (value instanceof ValueCollection) {
                values.add(((ValueCollection) value).unwrap());
            } else {
                values.add(value);
            }
        }
        return values;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
