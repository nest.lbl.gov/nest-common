package gov.lbl.nest.common.xml.item;

/**
 * This exception is thrown when there is an issue with loading or saving an
 * {@link Item} instance.
 *
 * @author patton
 */
public class ItemException extends
                           Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private instance member data

    // constructors

    /**
     * Constructs an instance of this class.
     */
    public ItemException() {
    }

    /**
     * Constructs an instance of this class.
     *
     * @param message
     *            the detail message. The detail message is saved for later
     *            retrieval by the {@link Throwable#getMessage()} method.
     */
    public ItemException(String message) {
        super(message);
    }

    /**
     * Constructs an instance of this class.
     *
     * @param message
     *            the detail message. The detail message is saved for later
     *            retrieval by the {@link Throwable#getMessage()} method.
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            {@link Throwable#getCause()} method). (A null value is permitted,
     *            and indicates that the cause is nonexistent or unknown.)
     */
    public ItemException(String message,
                         Throwable cause) {
        super(message,
              cause);
    }

    /**
     * Constructs an instance of this class.
     *
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            {@link Throwable#getCause()} method). (A null value is permitted,
     *            and indicates that the cause is nonexistent or unknown.)
     */
    public ItemException(Throwable cause) {
        super(cause);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
