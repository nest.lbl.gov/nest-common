/**
 * This package provides a generic mechanism of managing the Value of POJO items
 * in an XML document.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.io}</li>
 * <li>{@link java.lang.reflect}</li>
 * <li>{@link java.util}</li>
 * <li>{@link javax.xml.bind}</li>
 * <li>{@link org.w3c.dom}</li>
 * </ul>
 *
 * @author patton
 */
package gov.lbl.nest.common.xml.item;