package gov.lbl.nest.common.xml.item;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * This class is used by JAXB to convert input and output values for a workflow
 * instance between XML and instance of java classes.
 *
 * Based on http://blog.bdoughan.com/2012/02/xmlanyelement-and-xmladapter.html
 *
 * @author patton
 */
@XmlRootElement(name = "value")
public class ValueAdapter extends
                          XmlAdapter<Object, Value> {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The name of the resource holding the list of additional packages that this
     * class should support.
     */
    private static final String ITEM_PACKAGES_RESOURCE = "item.packages";

    /**
     * The name of the resource holding the collection of additional classes that do
     * not have JAXB annotations and that can be written and read as a
     * {@link String}.
     */
    private static final String NON_JAXB_CLASSES_RESOURCE = "non.jaxb.classes";

    /**
     * The list of packages, separated by ":", that this class should support.
     */
    private static String itemPackages;

    /**
     * The collection of classes that do not have JAXB annotations and that can be
     * written and read as a {@link String}.
     */
    private static Collection<Class<?>> nonJAXBClasses;

    // private static member data

    // private instance member data

    private static <T> JAXBElement<?> createJAXBElement(QName name,
                                                        T value) {
        @SuppressWarnings("unchecked")
        final Class<T> clazz = (Class<T>) (value.getClass());
        return new JAXBElement<>(name,
                                 clazz,
                                 value);
    }

    /**
     * Returns the list of packages, separated by ":", that this class should
     * support.
     *
     * @return the list of packages, separated by ":", that this class should
     *         support.
     */
    public static String getItemPackages() {
        if (null == itemPackages) {
            final StringBuffer sb = new StringBuffer("gov.lbl.nest.common.xml.item");
            try {
                final InputStream is = (ValueAdapter.class).getResourceAsStream(ITEM_PACKAGES_RESOURCE);
                try (final BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
                    String line = null;
                    do {
                        line = br.readLine();
                        if (null != line && !"".equals(line)) {
                            final String lineToUse = line.trim();
                            if (!lineToUse.startsWith("#")) {
                                sb.append(":" + lineToUse);
                            }
                        }
                    } while (null != line);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (NullPointerException e) {
                // No resource so do nothing here.
            }
            itemPackages = sb.toString();
        }
        return itemPackages;
    }

    /**
     * Returns the list of packages, separated by ":", that this class should
     * support.
     *
     * @return the list of packages, separated by ":", that this class should
     *         support.
     */
    public static Collection<Class<?>> getNonJAXBClasses() {
        if (null == nonJAXBClasses) {
            final List<Class<?>> classList = new ArrayList<>(Arrays.asList(new Class<?>[] { String.class,
                                                                                            Integer.class }));
            try {
                final InputStream is = (ValueAdapter.class).getResourceAsStream(NON_JAXB_CLASSES_RESOURCE);
                try (final BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
                    String line = null;
                    do {
                        line = br.readLine();
                        if (null != line && !"".equals(line)) {
                            final String lineToUse = line.trim();
                            if (!lineToUse.startsWith("#")) {
                                classList.add(((ValueAdapter.class).getClassLoader()).loadClass(lineToUse));
                            }
                        }
                    } while (null != line);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            } catch (NullPointerException e) {
                // No resource so do nothing here.
            }
            nonJAXBClasses = classList;
        }
        return nonJAXBClasses;
    }

    // constructors

    /**
     * The {@link ClassLoader} instance used by this object.
     */
    private final ClassLoader classLoader;

    /**
     * The {@link DocumentBuilder} instance used by this object.
     */
    private DocumentBuilder documentBuilder;

    // instance member method (alphabetic)

    /**
     * The {@link JAXBContext} instance used by this object.
     */
    private JAXBContext jaxbContext;

    /**
     * Create an instance of this class.
     */
    public ValueAdapter() {
        classLoader = (Thread.currentThread()).getContextClassLoader();
    }

    /**
     * Create an instance of this class.
     *
     * @param jaxbContext
     *            the existing {@link JAXBContext} instance this class will use.
     */
    public ValueAdapter(JAXBContext jaxbContext) {
        this();
        this.jaxbContext = jaxbContext;
    }

    private DocumentBuilder getDocumentBuilder() throws Exception {
        // Lazy load the DocumentBuilder as it is not used for unmarshalling.
        if (null == documentBuilder) {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            documentBuilder = dbf.newDocumentBuilder();
        }
        return documentBuilder;
    }

    // static member methods (alphabetic)

    private JAXBContext getJAXBContext(Class<?> type) throws Exception {
        if (null == jaxbContext) {
            // A JAXBContext was not set, so create a new one based on the type.
            return JAXBContext.newInstance(getItemPackages());
        }
        return jaxbContext;
    }

    @Override
    public Object marshal(Value value) throws Exception {
        if (null == value) {
            return null;
        }

        // 1. Build the JAXBElement to wrap the instance of Parameter.
        final Object object = value.getObject();
        if (null == object) {
            return null;
        }
        final Element element;
        if (getNonJAXBClasses().contains(object.getClass())) {
            final ValueAsString stringBasedValue = new ValueAsString(object);
            Document document = getDocumentBuilder().newDocument();
            Class<?> type = object.getClass();
            Marshaller marshaller = getJAXBContext(type).createMarshaller();
            marshaller.marshal(stringBasedValue,
                               document);
            element = document.getDocumentElement();
        } else {
            final QName name = new QName("value");
            JAXBElement<?> jaxbElement = createJAXBElement(name,
                                                           object);

            // 2. Marshal the JAXBElement to a DOM element.
            Document document = getDocumentBuilder().newDocument();
            Class<?> type = object.getClass();
            Marshaller marshaller = getJAXBContext(type).createMarshaller();
            marshaller.marshal(jaxbElement,
                               document);
            element = document.getDocumentElement();

            // 3. Set the type attribute based on the value's type.
            element.setAttribute("type",
                                 type.getName());
        }
        return element;
    }

    @Override
    public Value unmarshal(Object object) throws Exception {
        if (null == object) {
            return null;
        }

        final Element element = (Element) object;
        // 1. Determine the values type from the type attribute.
        try {
            Class<?> type = classLoader.loadClass(element.getAttribute("type"));

            // 2. Unmarshal the element based on the value's type.
            DOMSource source = new DOMSource(element);
            Unmarshaller unmarshaller = getJAXBContext(type).createUnmarshaller();
            JAXBElement<?> jaxbElement = unmarshaller.unmarshal(source,
                                                                type);

            // 3. Build the instance of ItemValue
            return new Value(jaxbElement.getValue());
        } catch (Throwable t) {
            System.out.println(t.getMessage());
            throw t;
        }
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
