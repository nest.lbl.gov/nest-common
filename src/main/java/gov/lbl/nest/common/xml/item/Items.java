package gov.lbl.nest.common.xml.item;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is used to group a collection of {@link Item} instances into a
 * single object. (This helps to support JSON parsing, which does not appear to
 * handle XmlElementWrapper annotations correctly!)
 *
 * @author patton
 *
 */
@XmlRootElement(name = "items")
@XmlType(propOrder = { "content" })
public class Items {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The Collection of {@link Item} instances in this object.
     */
    private List<Item> content;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Items() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param content
     *            the Collection of {@link Item} instances in this object.
     */
    public Items(List<Item> content) {
        setContent(content);
    }

    // instance member method (alphabetic)

    /**
     * Returns the Collection of {@link Item} instances in this object.
     *
     * @return the Collection of {@link Item} instances in this object.
     */
    @XmlElement(name = "item")
    public List<Item> getContent() {
        return content;
    }

    /**
     * Sets the Collection of {@link Item} instances in this object.
     *
     * @param content
     *            the Collection of {@link Item} instances in this object.
     */
    public void setContent(List<Item> content) {
        this.content = content;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
