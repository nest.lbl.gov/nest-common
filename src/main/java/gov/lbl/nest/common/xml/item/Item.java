package gov.lbl.nest.common.xml.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * This class provides a XML access to the value of an Item that makes up the
 * input and/or output of a workflow instance.
 *
 * @author patton
 */
@XmlRootElement(name = "item")
@XmlType(propOrder = { "name",
                       "itemSingle",
                       "itemArray",
                       "itemDictionary" })
public class Item {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The XML representation of the dictionary of values that is the values of this
     * Object.
     */
    private List<Item> items;

    /**
     * The name of this Object in the Workflow Definition.
     */
    private String name;

    /**
     * The XML representation of the value of this Object.
     */
    private Value value;

    /**
     * The XML representation of the array of values that is the values of this
     * Object.
     */
    private List<Value> values;

    // constructors

    /**
     * Creates an instance of this object.
     */
    protected Item() {
    }

    /**
     * Creates an instance of this object.
     *
     * @param name
     *            the name of this Object in the Workflow Definition.
     * @param value
     *            the value of this object.
     */
    @SuppressWarnings("unchecked")
    public Item(String name,
                Object value) {
        setName(name);
        if (value instanceof Map) {
            setDictionary((Map<String, ?>) value);
        } else if (value instanceof Collection) {
            setArray((Collection<?>) value);
        } else {
            setValue(value);
        }
    }

    // instance member method (alphabetic)

    /**
     * Returns the XML representation of the array of values that is the value of
     * this Object.
     *
     * @return the XML representation of the array of values that is the value of
     *         this Object.
     */
    @XmlElement(name = "value")
    @XmlElementWrapper(name = "values")
    public List<Value> getItemArray() {
        return values;
    }

    /**
     * Returns the XML representation of the dictionary of values that is the value
     * of this Object.
     *
     * @return the XML representation of the dictionary of values that is the value
     *         of this Object.
     */
    @XmlElement(name = "item")
    public List<Item> getItemDictionary() {
        return items;
    }

    /**
     * Returns the XML representation of the value of this Object.
     *
     * @return the XML representation of the value of this Object.
     */
    @XmlElement(name = "value")
    public Value getItemSingle() {
        return value;
    }

    /**
     * Returns the name of this Object in the Workflow Definition.
     *
     * @return the name of this Object in the Workflow Definition.
     */
    @XmlElement
    public String getName() {
        return name;
    }

    /**
     * Returns the value of this Object within a Workflow instance.
     *
     * @return the value of this Object within a Workflow instance.
     */
    @XmlTransient
    public Object getValue() {
        if (null == value) {
            if (null == values) {
                if (null == items) {
                    return null;
                }
                final Map<String, Object> unwrappedObjects = new HashMap<>();
                for (Item item : items) {
                    unwrappedObjects.put(item.getName(),
                                         item.getValue());
                }
                return unwrappedObjects;
            }
            final List<Object> unwrappedObjects = new ArrayList<>(values.size());
            for (Value value : values) {
                unwrappedObjects.add(value.getObject());
            }
            return unwrappedObjects;
        }
        return value.getObject();
    }

    /**
     * Set the array of values that is the value of this Object within a Workflow
     * instance.
     *
     * @param values
     *            the array of values that is the value of this Object within a
     *            Workflow instance.
     */
    protected void setArray(Collection<?> values) {
        if (null == values) {
            setItemArray(null);
        } else {
            final List<Value> wrappedObjects = new ArrayList<>(values.size());
            for (Object value : values) {
                wrappedObjects.add(new Value(value));
            }
            setItemArray(wrappedObjects);
        }
    }

    /**
     * Set the dictionary of values that is the value of this Object within a
     * Workflow instance.
     *
     * @param values
     *            the dictionary of values that is the value of this Object within a
     *            Workflow instance.
     */
    protected void setDictionary(Map<String, ?> values) {
        if (null == values) {
            setItemDictionary(null);
        } else {
            final List<Item> wrappedObjects = new ArrayList<>(values.size());
            for (String itemName : values.keySet()) {
                wrappedObjects.add(new Item(itemName,
                                            values.get(itemName)));
            }
            setItemDictionary(wrappedObjects);
        }
    }

    /**
     * Sets the XML representation of the array of values that is the value of this
     * Object.
     *
     * @param values
     *            the XML representation of the array of values that are the value
     *            of this Object.
     */
    public void setItemArray(List<Value> values) {
        this.values = values;
    }

    /**
     * Sets the XML representation of the dictionary of values that is the value of
     * this Object.
     *
     * @param items
     *            the XML representation of the dictionary of values that are the
     *            value of this Object.
     */
    public void setItemDictionary(List<Item> items) {
        this.items = items;
    }

    /**
     * Sets the XML representation of the value of this Object.
     *
     * @param value
     *            the XML representation of the value of this Object.
     */
    protected void setItemSingle(Value value) {
        this.value = value;
    }

    /**
     * Sets the name of this Object in the Workflow Definition.
     *
     * @param name
     *            the name of this Object in the Workflow Definition.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Set the value of this Object within a Workflow instance.
     *
     * @param value
     *            the value of this Object within a Workflow instance.
     */
    protected void setValue(Object value) {
        final Value wrappedObject = new Value(value);
        setItemSingle(wrappedObject);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
