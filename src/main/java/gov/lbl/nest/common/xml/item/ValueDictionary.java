package gov.lbl.nest.common.xml.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * This class is used as a container for a collection of data objects.
 *
 * @author patton
 */
@XmlRootElement(name = "dictionary")
public class ValueDictionary {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The contents of the collection to be saved.
     */
    private Collection<Item> content;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ValueDictionary() {
        // Default constructor used by JAXB
    }

    /**
     * Creates an instance of this class.
     *
     * @param map
     *            the {@link Map} instance from which this object will be built.
     */
    public ValueDictionary(final Map<String, ? extends Object> map) {
        content = new ArrayList<>();
        for (Map.Entry<String, ? extends Object> entry : map.entrySet()) {
            content.add(new Item(entry.getKey(),
                                 entry.getValue()));
        }
    }

    // instance member method (alphabetic)

    /**
     * Returns the contents of this collection.
     *
     * @return the contents of this collection.
     */
    @XmlElement
    public Collection<Item> getContent() {
        return content;
    }

    @XmlTransient
    Map<String, ? extends Object> getDictionary() {
        final Map<String, Object> result = new HashMap<>(content.size());
        for (Item item : content) {
            result.put(item.getName(),
                       item.getValue());
        }
        return result;
    }

    /**
     * Sets the contents of this collection.
     *
     * @param collection
     *            the collection that contains the content of this object.
     */
    public void setContent(Collection<Item> collection) {
        content = collection;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
