package gov.lbl.nest.common.xml.item;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * This class provides a RESTful access to the value of an Item that makes up
 * the input and/or output of a workflow instance.
 *
 * @author patton
 */
@XmlJavaTypeAdapter(value = ValueAdapter.class)
public class Value {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Object} that is the value of the containing {@link Item} instance.
     */
    private Object object;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Value() {
    }

    /**
     * Creates an instance of this object.
     *
     * @param object
     *            the {@link Object} that is the value of the containing
     *            {@link Item} instance.
     */
    public Value(Object object) {
        this.object = object;
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link Object} that is the value of the containing {@link Item}
     * instance.
     *
     * @return the {@link Object} that is the value of the containing {@link Item}
     *         instance.
     */
    public Object getObject() {
        return object;
    }

    /**
     * Sets the {@link Object} that is the value of the containing {@link Item}
     * instance.
     *
     * @param object
     *            the {@link Object} that is the value of the containing
     *            {@link Item} instance.
     */
    protected void setObject(Object object) {
        this.object = object;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
