package gov.lbl.nest.common.xml.item;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * This class provides a XML access to a POJO that does not have JAXB
 * annotations and that can be written and read as a {@link String}.
 *
 * @author patton
 */
@XmlRootElement(name = "non_jaxb_object")
public class NonJAXBObject {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The XML representation of the value of this item.
     */
    private Value value;

    // constructors

    /**
     * Creates an instance of this object.
     */
    protected NonJAXBObject() {
    }

    /**
     * Creates an instance of this object.
     *
     * @param value
     *            the value of this object.
     */
    public NonJAXBObject(Object value) {
        setValue(value);
    }

    // instance member method (alphabetic)

    /**
     * Returns the XML representation of the value of this item.
     *
     * @return the XML representation of the value of this item.
     */
    @XmlElement(name = "value")
    public Value getItemValue() {
        return value;
    }

    /**
     * Returns the value of this item within a Workflow instance.
     *
     * @return the value of this item within a Workflow instance.
     */
    @XmlTransient
    public Object getValue() {
        return value.getObject();
    }

    /**
     * Sets the XML representation of the value of this item.
     *
     * @param value
     *            the XML representation of the value of this item.
     */
    public void setItemValue(Value value) {
        this.value = value;
    }

    /**
     * Set the value of this item within a Workflow instance.
     *
     * @param value
     *            the value of this item within a Workflow instance.
     */
    public void setValue(Object value) {
        final Value itemValue = new Value(value);
        setItemValue(itemValue);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
