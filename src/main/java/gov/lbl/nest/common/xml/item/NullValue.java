package gov.lbl.nest.common.xml.item;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class is used to indicate a <code>null</code> value oject.
 *
 * @author patton
 */
@XmlRootElement(name = "null")
public class NullValue {
}
