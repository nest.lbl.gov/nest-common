package gov.lbl.nest.common.monitor;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is used to create instrumented {@link Collection} instances.
 *
 * @author patton
 *
 */
public class InstrumentedCollection {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The collection and all of the {@link InstrumentedList} instances know to this
     * Class.
     */
    private static final Map<Class<?>, Map<String, InstrumentedList<?>>> instrumentedLists = new HashMap<>();

    /**
     * True is the monitoring features should be enabled.
     */
    private static final boolean monitor = true;

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Returns the collection and all of the {@link InstrumentedList} instances know
     * to this Class.
     *
     * @return the collection and all of the {@link InstrumentedList} instances know
     *         to this Class.
     */
    public static final Map<Class<?>, Map<String, InstrumentedList<?>>> getIinstrumentedLists() {
        if (!monitor) {
            return null;
        }
        return instrumentedLists;
    }

    /**
     * Returns an instrumented view of the specified {@link List} instance.
     *
     * @param <T>
     *            the class held in the returned list.
     *
     * @param list
     *            the {@link List} instance for which an unmodifiable view is to be
     *            returned.
     * @param clazz
     *            the {@link Class} in which the list is used.
     * @param label
     *            a label to differentiate the List instance within the class.
     *            Normally the field name.
     *
     * @return the instrumented view of the specified {@link List} instance.
     */
    public static <T> List<T> instrumentedList(List<T> list,
                                               Class<?> clazz,
                                               String label) {
        if (!monitor) {
            return list;
        }

        if (null == clazz) {
            throw new NullPointerException("Class must be supplied");
        }
        final InstrumentedList<T> result = new InstrumentedList<>(list);
        synchronized (instrumentedLists) {
            final Map<String, InstrumentedList<?>> lists = instrumentedLists.get(clazz);
            final Map<String, InstrumentedList<?>> listsToUse;
            if (null == lists) {
                listsToUse = new HashMap<>();
                instrumentedLists.put(clazz,
                                      listsToUse);
            } else {
                listsToUse = lists;
            }

            final String labelToUsed;
            if (null == label) {
                labelToUsed = "List_" + Integer.toString(listsToUse.size());
            } else {
                labelToUsed = label;
            }
            listsToUse.put(labelToUsed,
                           result);
        }
        return result;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
