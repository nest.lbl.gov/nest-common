package gov.lbl.nest.common.monitor;

/**
 * The standard level of details for a point of interest (
 * {@link PointOfInterest}).
 *
 * @author patton
 */
public enum Level {

                   /**
                    * Component cannot continue, or system is unusable.
                    */
                   FATAL,

                   /**
                    * Action must be taken immediately.
                    */
                   ALERT,

                   /**
                    * Critical conditions (on the system).
                    */
                   CRITICAL,

                   /**
                    * Errors in the component; not errors from elsewhere.
                    */
                   ERROR,

                   /**
                    * Problems that are recovered from, usually.
                    */
                   WARNING,

                   /**
                    * Normal but significant condition.
                    */
                   NOTICE,

                   /**
                    * Informational messages that would be useful to a deployer or administrator.
                    */
                   INFO,

                   /**
                    * Lower level information concerning program logic decisions, internal state,
                    * etc.
                    */
                   DEBUG,

                   /**
                    * Finest granularity, similar to “stepping through” the component or system.
                    */
                   TRACE;

}
