package gov.lbl.nest.common.monitor;

import javax.xml.bind.annotation.XmlElement;

/**
 * This class is used to provide initialization parameters for object declared
 * in a configuration file.
 *
 * @author patton
 */
public class Attribute {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The name of the parameter.
     */
    private String name;

    /**
     * The value of the parameter.
     */
    private String value;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Attribute() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of the parameter.
     * @param value
     *            the value of the parameter.
     */
    public Attribute(final String name,
                     final String value) {
        setName(name);
        setValue(value);
    }

    // instance member method (alphabetic)

    /**
     * Returns the name of the parameter.
     *
     * @return the name of the parameter.
     */
    @XmlElement(name = "param-name",
                required = true)
    public String getName() {
        return name;
    }

    /**
     * Returns the value of the parameter.
     *
     * @return the value of the parameter.
     */
    @XmlElement(name = "param-value",
                required = true)
    public String getValue() {
        return value;
    }

    /**
     * Sets the name of the parameter.
     *
     * @param name
     *            the name of the parameter.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the value of the parameter.
     *
     * @param value
     *            the value of the parameter.
     */
    protected void setValue(String value) {
        this.value = value;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
