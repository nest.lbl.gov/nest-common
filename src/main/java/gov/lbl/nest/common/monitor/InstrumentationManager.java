package gov.lbl.nest.common.monitor;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * This interface is used to record Points of Interest.
 *
 * @author patton
 */
public interface InstrumentationManager {

    /**
     * Returns the collection of {@link PointOfInterest} instance that match the
     * specified attributes.
     *
     * @param since
     *            the time on or after which {@link PointOfInterest} instances
     *            should be include in the returned list.
     * @param before
     *            the time before which {@link PointOfInterest} instances should be
     *            include in the returned list. If <code>null</code> then no time
     *            limit is used.
     * @param maxCount
     *            the maximum number of {@link PointOfInterest} instances to return,
     *            0 is unlimited.
     * @param type
     *            the type of event that must match a {@link PointOfInterest}
     *            instance for it to be included in the resultant list.
     * @param level
     *            the level of detail that must match a {@link PointOfInterest}
     *            instance for it to be included in the resultant list.
     * @param status
     *            The status, if any, that must match a {@link PointOfInterest}
     *            instance for it to be included in the resultant list.
     * @param guid
     *            the guid that must match a {@link PointOfInterest} instance for it
     *            to be included in the resultant list.
     * @param attributes
     *            a Map of attributes that must match a {@link PointOfInterest}
     *            instance for it to be included in the resultant list.
     *
     * @return the collection of {@link PointOfInterest} instance that match the
     *         specified attributes.
     */
    Collection<PointOfInterest> getPointsOfInterest(Date since,
                                                    Date before,
                                                    int maxCount,
                                                    String type,
                                                    Level level,
                                                    Integer status,
                                                    String guid,
                                                    Map<String, String> attributes);

    /**
     * Records the current point of interest.
     *
     * @param dateTime
     *            the date and time when the point of interest occurred.
     * @param type
     *            the type of event that occurred at this point of interest.
     * @param level
     *            the level of detail of this point of interest.
     * @param status
     *            The status, if any, of an operation executed this point of
     *            interest.
     * @param guid
     *            the guid associated of event that occurred at this point of
     *            interest.
     * @param attributes
     *            the other attributes to associated with this point of interest.
     */
    void pointOfInterest(Date dateTime,
                         String type,
                         Level level,
                         Integer status,
                         String guid,
                         Map<String, String> attributes);
}
