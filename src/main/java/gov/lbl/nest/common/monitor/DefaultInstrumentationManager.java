package gov.lbl.nest.common.monitor;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * The default implementation of the {@link InstrumentationManager} interface.
 *
 * @author patton
 */
public class DefaultInstrumentationManager implements
                                           InstrumentationManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    public Collection<PointOfInterest> getPointsOfInterest(Date since,
                                                           Date before,
                                                           int maxCount,
                                                           String type,
                                                           Level level,
                                                           Integer status,
                                                           String guid,
                                                           Map<String, String> attributes) {
        return null;
    }

    @Override
    public void pointOfInterest(Date dateTime,
                                String type,
                                Level level,
                                Integer status,
                                String guid,
                                Map<String, String> attributes) {
        // Does nothing
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
