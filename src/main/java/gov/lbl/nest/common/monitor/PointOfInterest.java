package gov.lbl.nest.common.monitor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class captures a point of interest within a given system occurring at a
 * specific time.
 *
 * @author patton
 */
@XmlRootElement
@XmlType(propOrder = { "timeStamp",
                       "type",
                       "level",
                       "status",
                       "guid" })
public class PointOfInterest {

    // public static final member data

    /**
     * The name used for the guid attibute.
     */
    public static final String GUID_NAME = "guid";

    /**
     * The name used for the level attibute.
     */
    public static final String LEVEL_NAME = "level";

    /**
     * The name used for the status attibute.
     */
    public static final String STATUS_NAME = "status";

    /**
     * The name used for the timestamp attibute.
     */
    public static final String TIMESTAMP_NAME = "ts";

    /**
     * The name used for the type attibute.
     */
    public static final String TYPE_NAME = "event";

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The other attributes of this point of interest.
     */
    private Collection<Attribute> attributes;

    /**
     * The guid associated with this point of interest.
     */
    private String guid;

    /**
     * The level of detail of this point of interest.
     */
    private Level level;

    /**
     * The status of an operation executed this point of interest.
     */
    private Integer status;

    /**
     * The date and time when this point of interest occurred.
     */
    private Date timeStamp;

    /**
     * The type of event that occurred at this point of interest.
     */
    private String type;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected PointOfInterest() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param dateTime
     *            the date and time when the point of interest occurred.
     * @param type
     *            the type of event that occurred at this point of interest.
     * @param level
     *            the level of detail of this point of interest.
     * @param status
     *            The status, if any, of an operation executed this point of
     *            interest.
     * @param guid
     *            the guid associated of event that occurred at this point of
     *            interest.
     * @param attributes
     *            the other attributes of this point of interest.
     */
    public PointOfInterest(Date dateTime,
                           String type,
                           Level level,
                           Integer status,
                           String guid,
                           Map<String, String> attributes) {
        setGuid(guid);
        setLevel(level);
        setStatus(status);
        setTimeStamp(dateTime);
        setType(type);
        if (null != attributes) {
            final Collection<Attribute> attributesToUse = new ArrayList<>();
            for (String key : attributes.keySet()) {
                final String value = attributes.get(key);
                if (key.equals(GUID_NAME) && null == guid) {
                    setGuid(value);
                } else if (key.equals(LEVEL_NAME) && null == level) {
                    setLevel(Level.valueOf(value));
                } else if (key.equals(STATUS_NAME) && null == status) {
                    setStatus(Integer.parseInt(value));
                } else if (key.equals(TIMESTAMP_NAME) && null == dateTime) {
                    setTimeStamp((DatatypeConverter.parseDateTime(value)).getTime());
                } else if (key.equals(TYPE_NAME) && null == type) {
                    setType(type);
                } else {
                    attributesToUse.add(new Attribute(key,
                                                      attributes.get(key)));
                }
            }
            setAttributes(attributesToUse);
        }
    }

    /**
     * Creates an instance of this class.
     *
     * @param attributes
     *            all of the attributes of this point of interest.
     */
    public PointOfInterest(Map<String, String> attributes) {
        this(null,
             null,
             null,
             null,
             null,
             attributes);
    }

    // instance member method (alphabetic)

    /**
     * Returns the other attributes of this point of interest.
     *
     * @return the other attributes of this point of interest.
     */
    @XmlElement(name = "attribute")
    protected Collection<Attribute> getAttributes() {
        return attributes;
    }

    /**
     * Returns the guid associated with this point of interest.
     *
     * @return the guid associated with this point of interest.
     */
    @XmlElement
    protected String getGuid() {
        return guid;
    }

    /**
     * Returns the level of detail of this point of interest.
     *
     * @return the level of detail of this point of interest.
     */
    @XmlElement
    protected Level getLevel() {
        return level;
    }

    /**
     * Returns the status, if any, of an operation executed this point of interest.
     * Zero indicating success, negative values indicating failures, and positive
     * values indicating something in between.
     *
     * @return the status, if any, of an operation executed this point of interest.
     */
    @XmlElement
    protected Integer getStatus() {
        return status;
    }

    /**
     * Returns the date and time when this point of interest occurred.
     *
     * @return the date and time when this point of interest occurred.
     */
    @XmlElement(name = "ts")
    protected Date getTimeStamp() {
        return timeStamp;
    }

    /**
     * Returns the type of event that occurred at this point of interest.
     *
     * @return the type of event that occurred at this point of interest.
     */
    @XmlElement(name = "guid")
    protected String getType() {
        return type;
    }

    /**
     * Sets the other attributes of this point of interest.
     *
     * @param attributes
     *            the other attributes of this point of interest.
     */
    protected void setAttributes(Collection<Attribute> attributes) {
        this.attributes = attributes;
    }

    /**
     * Sets the guid associated with this point of interest.
     *
     * @param guid
     *            the guid associated with this point of interest.
     */
    protected void setGuid(String guid) {
        this.guid = guid;
    }

    /**
     * Sets the level of detail of this point of interest.
     *
     * @param level
     *            the level of detail of this point of interest.
     */
    protected void setLevel(Level level) {
        this.level = level;
    }

    /**
     * Sets the status of an operation executed this point of interest. Zero
     * indicating success, negative values indicating failures, and positive values
     * indicating something in between.
     *
     * @param status
     *            the status of an operation executed this point of interest.
     */
    protected void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * Sets the date and time when this point of interest occurred.
     *
     * @param dateTime
     *            the date and time when this point of interest occurred.
     */
    protected void setTimeStamp(Date dateTime) {
        timeStamp = dateTime;
    }

    /**
     * Sets the type of event that occurred at this point of interest.
     *
     * @param type
     *            the type of event that occurred at this point of interest.
     */
    protected void setType(String type) {
        this.type = type;
    }

    /**
     * Returns the JSON representation of this object.
     *
     * @param externalIndent
     *            the indent, if any, to use on the whole object.
     * @param internalIndent
     *            the indent, if any, to use within this object. This is be added to
     *            the external indent to create a total indent for elements within
     *            the object.
     *
     * @return the JSON representation of this object.
     */
    public String toJSON(String externalIndent,
                         String internalIndent) {
        final String externalIndentToUse;
        final String endOfItem;
        final String separator;
        if (null == externalIndent || "\n" == externalIndent) {
            externalIndentToUse = "";
            endOfItem = "";
            separator = "\":\"";
        } else {
            externalIndentToUse = externalIndent;
            endOfItem = "\n";
            separator = "\" : \"";
        }
        final String totalIndenet;
        if (null == internalIndent) {
            totalIndenet = externalIndentToUse + "\"";
        } else {
            totalIndenet = externalIndentToUse + internalIndent
                           + "\"";
        }
        final StringBuilder sb = new StringBuilder(externalIndentToUse + "{");
        String conjunction = endOfItem;
        if (null != timeStamp) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(timeStamp);
            sb.append(conjunction + totalIndenet
                      + TIMESTAMP_NAME
                      + separator
                      + DatatypeConverter.printDateTime(calendar)
                      + "\"");
            conjunction = "," + endOfItem;
        }
        if (null != type) {
            sb.append(conjunction + totalIndenet
                      + TYPE_NAME
                      + separator
                      + type
                      + "\"");
            conjunction = "," + endOfItem;
        }
        if (null != level) {
            sb.append(conjunction + totalIndenet
                      + LEVEL_NAME
                      + separator
                      + level.toString()
                      + "\"");
            conjunction = "," + endOfItem;
        }
        if (null != status) {
            sb.append(conjunction + totalIndenet
                      + STATUS_NAME
                      + separator
                      + Integer.toString(status)
                      + "\"");
            conjunction = "," + endOfItem;
        }
        if (null != guid) {
            sb.append(conjunction + totalIndenet
                      + GUID_NAME
                      + separator
                      + guid
                      + "\"");
            conjunction = "," + endOfItem;
        }
        if (null != attributes) {
            for (Attribute attribute : attributes) {
                sb.append(conjunction + totalIndenet
                          + attribute.getName()
                          + separator
                          + attribute.getValue()
                          + "\"");
                conjunction = "," + endOfItem;
            }
        }
        sb.append(endOfItem + externalIndentToUse
                  + "}");
        return sb.toString();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
