package gov.lbl.nest.common.monitor;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * This class is used to instrument {@link List} instance so their behavior can
 * be inspected later.
 *
 * @author patton
 *
 * @param <E>
 *            the class held in the list.
 */
public class InstrumentedList<E> implements
                             List<E> {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link List} instance being wrapped.
     */
    private List<E> delegatee;

    // constructors

    InstrumentedList(List<E> delegatee) {
        this.delegatee = delegatee;
    }

    // instance member method (alphabetic)

    @Override
    public boolean add(E e) {
        return delegatee.add(e);
    }

    @Override
    public void add(int index,
                    E element) {
        delegatee.add(index,
                      element);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return delegatee.addAll(c);
    }

    @Override
    public boolean addAll(int index,
                          Collection<? extends E> c) {
        return delegatee.addAll(index,
                                c);
    }

    @Override
    public void clear() {
        delegatee.clear();
    }

    @Override
    public boolean contains(Object o) {
        return delegatee.contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return delegatee.containsAll(c);
    }

    @Override
    public E get(int index) {
        return delegatee.get(index);
    }

    @Override
    public int indexOf(Object o) {
        return delegatee.indexOf(o);
    }

    @Override
    public boolean isEmpty() {
        return delegatee.isEmpty();
    }

    @Override
    public Iterator<E> iterator() {
        return delegatee.iterator();
    }

    @Override
    public int lastIndexOf(Object o) {
        return delegatee.lastIndexOf(o);
    }

    @Override
    public ListIterator<E> listIterator() {
        return delegatee.listIterator();
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return delegatee.listIterator(index);
    }

    @Override
    public E remove(int index) {
        return delegatee.remove(index);
    }

    @Override
    public boolean remove(Object o) {
        return delegatee.remove(o);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return delegatee.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return delegatee.retainAll(c);
    }

    @Override
    public E set(int index,
                 E element) {
        return delegatee.set(index,
                             element);
    }

    @Override
    public int size() {
        return delegatee.size();
    }

    @Override
    public List<E> subList(int fromIndex,
                           int toIndex) {
        return delegatee.subList(fromIndex,
                                 toIndex);
    }

    @Override
    public Object[] toArray() {
        return delegatee.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return delegatee.toArray(a);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
