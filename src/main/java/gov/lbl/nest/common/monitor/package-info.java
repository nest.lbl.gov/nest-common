/**
 * This package provides classes used to capture performance information.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.util}</li>
 * <li>{@link javax.xml.bind}</li>
 * <li>{@link org.slf4j}</li>
 * </ul>
 */
package gov.lbl.nest.common.monitor;
