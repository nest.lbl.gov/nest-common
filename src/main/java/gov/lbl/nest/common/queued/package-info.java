/**
 * This package provides the core interfaces and classes used to capture the
 * timing information for any object that may be held in a queue.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.time}</li>
 * <li>{@link java.util}</li>
 * </ul>
 */
package gov.lbl.nest.common.queued;
