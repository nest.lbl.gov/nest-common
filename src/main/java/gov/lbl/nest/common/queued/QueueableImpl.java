package gov.lbl.nest.common.queued;

import java.time.Duration;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

/**
 * This class implements the {@link Queueable} and {@link Queued} interfaces.
 *
 * @author patton
 *
 */
public class QueueableImpl implements
                           Queueable,
                           Queued {

    /**
     * The date and time when this Object was created.
     */
    private final AtomicReference<Date> whenCreated = new AtomicReference<>();

    /**
     * The date and time when this Object finished being executed.
     */
    private final AtomicReference<Date> whenFinished = new AtomicReference<>();

    /**
     * The date and time when this Object started to be executed.
     */
    private final AtomicReference<Date> whenStarted = new AtomicReference<>();

    /**
     * Creates a new instance of this class.
     */
    public QueueableImpl() {
        whenCreated.set(new Date());
    }

    @Override
    public final Duration getExecutionDuration() {
        final Date started = whenStarted.get();
        if (null == started) {
            return null;
        }
        final Date finishedToUse;
        final Date finished = whenFinished.get();
        if (null == finished) {
            finishedToUse = new Date();
        } else {
            finishedToUse = finished;
        }
        return Duration.ofMillis(finishedToUse.getTime() - started.getTime());
    }

    @Override
    public final Duration getTotalDuration() {
        final Date finishedToUse;
        final Date finished = whenFinished.get();
        if (null == finished) {
            final Date started = whenStarted.get();
            if (null == started) {
                finishedToUse = new Date();
            } else {
                finishedToUse = started;
            }
        } else {
            finishedToUse = finished;
        }
        return Duration.ofMillis(finishedToUse.getTime() - (whenCreated.get()).getTime());
    }

    @Override
    public final Duration getWaitDuration() {
        final Date startedToUse;
        final Date started = whenFinished.get();
        if (null == started) {
            startedToUse = new Date();
        } else {
            startedToUse = started;
        }

        return Duration.ofMillis(startedToUse.getTime() - (whenCreated.get()).getTime());
    }

    @Override
    public final Date getWhenCreated() {
        return whenCreated.get();
    }

    @Override
    public final Date getWhenFinished() {
        return whenFinished.get();
    }

    @Override
    public final Date getWhenStarted() {
        return whenStarted.get();
    }

    @Override
    public final void setWhenFinished(Date dateTime) {
        if (null == whenStarted.get()) {
            throw new IllegalStateException("Start timing is not yet set");
        }
        if (null != whenFinished.get()) {
            throw new IllegalStateException("Finish timing already set");
        }
        whenFinished.set(dateTime);
    }

    @Override
    public final void setWhenStarted(Date dateTime) {
        if (null != whenStarted.get()) {
            throw new IllegalStateException("Start timing is not yet set");
        }
        whenStarted.set(dateTime);
    }
}
