package gov.lbl.nest.common.queued;

import java.time.Duration;
import java.util.Date;

/**
 * This interface defined the set of methods use to retrieving the timing
 * information for any object that is held in a queue.
 *
 * @author patton
 *
 */
public interface Queued {

    /**
     * Returns the {@link Duration} instance for the time spent executing, or until
     * now if this Object has not yet finished.
     *
     * @return the {@link Duration} instance for the time spent executing, or until
     *         now if this Object has not yet finished.
     */
    Duration getExecutionDuration();

    /**
     * Returns the {@link Duration} instance for the time between the created of
     * this Object and the completion of its execution.
     *
     * @return the {@link Duration} instance for the time between the created of
     *         this Object and the completion of its execution.
     */
    Duration getTotalDuration();

    /**
     * Returns the {@link Duration} instance for the time waiting before execution
     * started, or until now if this Object has not yet started.
     *
     * @return the {@link Duration} instance for the time waiting before execution
     *         started, or until now if this Object has not yet started.
     */
    Duration getWaitDuration();

    /**
     * Returns the data and time when this Object was created.
     *
     * @return the data and time when this Object was created.
     */
    Date getWhenCreated();

    /**
     * Returns the data and time when this Object finished being executed.
     *
     * @return the data and time when this Object finished being executed.
     */
    Date getWhenFinished();

    /**
     * Returns the data and time when this Object started to be executed.
     *
     * @return the data and time when this Object started to be executed.
     */
    Date getWhenStarted();

}