package gov.lbl.nest.common.queued;

import java.util.Date;

/**
 * This interface is used to capture the timing information for any object that
 * may be held in a queue. The captured information can be retrieved by using
 * the {@link Queued} interface. Normally the constructor of an implementation
 * of this interface is used to set the <code>whenCreated</code> field return by
 * the {@link Queued} interface.
 *
 * @author patton
 *
 */
public interface Queueable {

    /**
     * Sets the date and time when this Object finished being executed.
     *
     * @param dateTime
     *            the date and time when this Object finished being executed.
     */
    void setWhenFinished(Date dateTime);

    /**
     * Sets the date and time when this Object started to be executed.
     *
     * @param dateTime
     *            the date and time when this Object started to be executed.
     */
    void setWhenStarted(Date dateTime);
}
