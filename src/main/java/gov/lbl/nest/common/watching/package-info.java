/**
 * <p>
 * This package provides classes that watch for changes in items. These changes
 * may be issuance (or creation), revocation (or deletion), modification, or
 * simply waiting (or waiting) for a change.
 * </p>
 *
 * <p>
 * The core of this package is the {@link gov.lbl.nest.common.watching.Digest}
 * class that captures information about items. This class provides lists of
 * items, one list for each type of change. The
 * <code>issued</code>,<code>revoked</code>, and <code>modified</code> lists
 * bind an item to an associated time, but enclosing the item in a
 * {@link gov.lbl.nest.common.watching.DigestChange} instance. The
 * <code>queued</code> list is simply a list of the items that are explicitly
 * waiting for a change to occur.
 * </p>
 *
 * <p>
 * A {@link gov.lbl.nest.common.watching.DigestChange} instance may also include
 * a list of {@link gov.lbl.nest.common.watching.Detail} instances that provide
 * additional information about the item.
 * </p>
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.io}</li>
 * <li>{@link java.net}</li>
 * <li>{@link java.text}</li>
 * <li>{@link java.util}</li>
 * <li>{@link javax.xml.bind}</li>
 * <li>{@link org.xml.sax}</li>
 * <li>{@link gov.lbl.nest.common.rs}</li>
 * <li>{@link gov.lbl.nest.common.xml.item}</li>
 * </ul>
 */
package gov.lbl.nest.common.watching;
