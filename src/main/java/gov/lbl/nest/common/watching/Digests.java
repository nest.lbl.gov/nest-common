package gov.lbl.nest.common.watching;

import java.net.URI;
import java.util.List;

import gov.lbl.nest.common.rs.Resource;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a collection of {@link Digest} instances.
 *
 * @author patton
 */
@XmlRootElement(name = "digests")
@XmlType(propOrder = { "subject",
                       "collection" })
public class Digests extends
                     Resource {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The ordered collection making up this object.
     */
    private List<Digest> collection;

    /**
     * The overall subject to which the contained {@link Digest} instances refer.
     */
    private String subject;

    // constructors

    /**
     * Constructs and instance of this class.
     */
    protected Digests() {
    }

    /**
     * Constructs and instance of this class.
     *
     * @param collection
     *            the ordered collection making up this object.
     */
    public Digests(List<Digest> collection) {
        this(null,
             null,
             collection);
    }

    /**
     * Constructs and instance of this class.
     *
     * @param subject
     *            the overall subject to which the contained {@link Digest}
     *            instances refer.
     * @param collection
     *            the ordered collection making up this object.
     */
    public Digests(String subject,
                   List<Digest> collection) {
        this(subject,
             null,
             collection);
    }

    /**
     * Constructs and instance of this class.
     *
     * @param subject
     *            the overall subject to which the contained {@link Digest}
     *            instances refer.
     * @param uri
     *            the URI, if any, that is related to the content of this object.
     * @param collection
     *            the ordered collection making up this object.
     */
    public Digests(String subject,
                   URI uri,
                   List<Digest> collection) {
        super(uri);
        setCollection(collection);
        setSubject(subject);
    }

    // instance member method (alphabetic)

    /**
     * Returns the ordered collection making up this object.
     *
     * @return the ordered collection making up this object.
     */
    @XmlElement(name = "digest")
    public List<Digest> getCollection() {
        return collection;
    }

    /**
     * Returns the overall subject to which the contained {@link Digest} instances
     * refer.
     *
     * @return the overall subject to which the contained {@link Digest} instances
     *         refer.
     */
    @XmlElement
    public String getSubject() {
        return subject;
    }

    /**
     * Sets the ordered collection making up this object.
     *
     * @param collection
     *            the ordered collection making up this object.
     */
    public void setCollection(List<Digest> collection) {
        this.collection = collection;
    }

    /**
     * Sets the overall subject to which the contained {@link Digest} instances
     * refer.
     *
     * @param subject
     *            the overall subject to which the contained {@link Digest}
     *            instances refer.
     */
    protected void setSubject(String subject) {
        this.subject = subject;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
