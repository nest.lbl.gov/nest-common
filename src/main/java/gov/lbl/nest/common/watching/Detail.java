package gov.lbl.nest.common.watching;

import gov.lbl.nest.common.xml.item.Item;
import javax.xml.bind.annotation.XmlType;

/**
 * This simply renames the class for using in the {@link DigestChange} class.o
 *
 * @author patton
 *
 */
@XmlType
public class Detail extends
                    Item {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this object.
     */
    protected Detail() {
    }

    /**
     * Creates an instance of this object.
     *
     * @param name
     *            the name of this item in the Workflow Definition.
     * @param value
     *            the value of this object.
     */
    public Detail(String name,
                  Object value) {
        super(name,
              value);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
