package gov.lbl.nest.common.watching;

import gov.lbl.nest.common.watching.WatchedList.Callback;

/**
 * This interface is used to create new {@link WatchedList} isntances.
 *
 * @author patton
 *
 */
public interface WatchedListFactory {

    /**
     * Creates, if necessary, a new {@link WatchedList} instance.
     *
     * @param role
     *            A unique {@link String}, within this Object, that specifies the
     *            {@link WatchedList} to be returned.
     * @param callback
     *            The {@link Callback} instance to be used by the returned
     *            {@link WatchedList} to read the contents of the list being
     *            watched.
     *
     * @return the {@link WatchedList} instance for the specified role.
     *
     */
    WatchedList createWatchedList(String role,
                                  Callback callback);
}
