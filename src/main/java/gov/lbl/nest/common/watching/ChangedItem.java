package gov.lbl.nest.common.watching;

import java.util.Date;

/**
 * This interface is used capture a change to an {@link NamedItem} instance
 * along with the date and time that that change occurred.
 *
 * @author patton
 */
public interface ChangedItem extends
                             Cloneable,
                             Comparable<ChangedItem>,
                             NamedItem {

    /**
     * Creates and returns a copy of this object.
     *
     * @return the copy of this object.
     *
     * @throws CloneNotSupportedException
     *             if the object's class does not support the Cloneable interface.
     *             Subclasses that override the clone method can also throw this
     *             exception to indicate that an instance cannot be cloned.
     */
    Object clone() throws CloneNotSupportedException;

    /**
     * Returns the date and time that the change in the {@link NamedItem} instance
     * occurred,
     *
     * @return the date and time that the change in the {@link NamedItem} instance
     *         occurred,
     */
    Date getWhenItemChanged();
}
