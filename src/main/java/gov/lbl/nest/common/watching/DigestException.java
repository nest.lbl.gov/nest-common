package gov.lbl.nest.common.watching;

/**
 * This exception is thrown when there is an issue with loading or saving a
 * {@link Digest} instance.
 *
 * @author patton
 */
public class DigestException extends
                             Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private instance member data

    // constructors

    /**
     * Constructs an instance of this class.
     */
    public DigestException() {
    }

    /**
     * Constructs an instance of this class.
     *
     * @param message
     *            the detail message. The detail message is saved for later
     *            retrieval by the {@link Throwable#getMessage()} method.
     */
    public DigestException(String message) {
        super(message);
    }

    /**
     * Constructs an instance of this class.
     *
     * @param message
     *            the detail message. The detail message is saved for later
     *            retrieval by the {@link Throwable#getMessage()} method.
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            {@link Throwable#getCause()} method). (A null value is permitted,
     *            and indicates that the cause is nonexistent or unknown.)
     */
    public DigestException(String message,
                           Throwable cause) {
        super(message,
              cause);
    }

    /**
     * Constructs an instance of this class.
     *
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            {@link Throwable#getCause()} method). (A null value is permitted,
     *            and indicates that the cause is nonexistent or unknown.)
     */
    public DigestException(Throwable cause) {
        super(cause);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
