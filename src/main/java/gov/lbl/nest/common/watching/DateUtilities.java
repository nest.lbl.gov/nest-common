package gov.lbl.nest.common.watching;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

/**
 * This class provides utilities for manipluation date and time structures.
 *
 * @author patton
 */
public class DateUtilities {

    /**
     * The format for date queries.
     */
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * The format for date and time queries.
     */
    private static final String DATE_AND_TIME_FORMAT = DATE_FORMAT + "'T'HH:mm:ss";

    /**
     * The format for date and time queries.
     */
    private static final String LONG_DATE_AND_TIME_FORMAT = DATE_AND_TIME_FORMAT + ".SSS";

    /**
     * The format for date and time queries.
     */
    private static final String ZONED_FORMAT = DATE_AND_TIME_FORMAT + "Z";

    /**
     * The format for date and time queries.
     */
    private static final String LONG_ZONED_FORMAT = LONG_DATE_AND_TIME_FORMAT + "Z";

    /**
     * The formatter for date supplied to this class as Strings.
     */
    public static final DateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    public static final DateFormat DATE_AND_TIME_FORMATTER = new SimpleDateFormat(DATE_AND_TIME_FORMAT);

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    public static final DateFormat LONG_DATE_AND_TIME_FORMATTER = new SimpleDateFormat(LONG_DATE_AND_TIME_FORMAT);

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    public static final DateFormat ZONED_FORMATTER = new SimpleDateFormat(ZONED_FORMAT);

    /**
     * The formatter to use to create a for date and time query element.
     */
    public static final DateFormat LONG_ZONED_FORMATTER = new SimpleDateFormat(LONG_ZONED_FORMAT);

    /**
     * The minimum allowed year for {@link DatatypeConverter} result (to avoid DB
     * lookup issues).
     */
    private static final Date MINIMUM_YEAR;

    /**
     * The maximum allowed year for {@link DatatypeConverter} result (to avoid DB
     * lookup issues).
     */
    private static final Date MAXIMUM_YEAR;

    static {
        Date min = null;
        Date max = null;
        try {
            min = DATE_FORMATTER.parse("1969-12-31T23:59:59.999");
            max = DATE_FORMATTER.parse("10000-01-01");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        MINIMUM_YEAR = min;
        MAXIMUM_YEAR = max;
    }

    /**
     * Parses a query date and/or time into a {@link Date} instance.
     *
     * @param dateTime
     *            the date and/or time to parse
     *
     * @return the {@link Date} instance.
     *
     * @throws ParseException
     *             when the date and/or time can not be parsed.
     */
    public static Date parseQueryDate(final String dateTime) throws ParseException {
        if (null == dateTime || "".equals(dateTime)) {
            return null;
        }
        try {
            final Calendar calendar = DatatypeConverter.parseDateTime(dateTime);
            final Date result = calendar.getTime();
            if (MINIMUM_YEAR.after(result) || MAXIMUM_YEAR.before(result)) {
                throw new ParseException("Year outside allow range",
                                         0);
            }
            return result;
        } catch (IllegalArgumentException e) {

            if (-1 == dateTime.indexOf('.')) {
                if (DATE_FORMAT.length() == dateTime.length()) {
                    return DATE_FORMATTER.parse(dateTime);
                }
                if ((DATE_AND_TIME_FORMAT.length() - 2) == dateTime.length()) {
                    return DATE_AND_TIME_FORMATTER.parse(dateTime);
                }
                return ZONED_FORMATTER.parse(dateTime);
            }
            if ((LONG_DATE_AND_TIME_FORMAT.length() - 2) == dateTime.length()) {
                return LONG_DATE_AND_TIME_FORMATTER.parse(dateTime);
            }
            return LONG_ZONED_FORMATTER.parse(dateTime);
        }
    }

}
