package gov.lbl.nest.common.watching;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class an implementation of the {@link NamedItem} interface that can be
 * included in a {@link Digest} instance.
 *
 * @author patton
 */
@XmlType(propOrder = { "item" })
public class DigestItem implements
                        NamedItem {

    /**
     * The identity of the item within the {@link Digest} instance.
     */
    private String item;

    /**
     * Creates an instance of this class.
     */
    public DigestItem() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param item
     *            the identity of the item within the {@link Digest} instance.
     */
    public DigestItem(String item) {
        setItem(item);
    }

    @Override
    @XmlElement
    public String getItem() {
        return item;
    }

    /**
     * Sets the identity of the item within the {@link Digest} instance.
     *
     * @param item
     *            the identity of the item within the {@link Digest} instance.
     */
    public void setItem(String item) {
        this.item = item;
    }
}
