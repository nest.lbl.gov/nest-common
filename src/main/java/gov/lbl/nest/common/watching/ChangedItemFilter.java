package gov.lbl.nest.common.watching;

import java.util.List;

/**
 * This interface is used to declared classes the can filter a list of
 * {@link ChangedItem} instances, returning only those instance that pass the
 * filter.
 *
 * @author patton
 *
 */
public interface ChangedItemFilter {

    /**
     * Filters the list of {@link ChangedItem} instances returning only those that
     * pass the filter.
     *
     * @param items
     *            the list of {@link ChangedItem} instances to filter.
     * @param <T>
     *            the sub-class of {@link ChangedItem} actually in the list.
     *
     * @return the filtered list of {@link ChangedItem} instances.
     */
    <T extends ChangedItem> List<T> filter(List<T> items);
}
