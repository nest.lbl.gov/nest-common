package gov.lbl.nest.common.watching;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * This class an implementation of the {@link ChangedItem} interface that can be
 * included in a {@link Digest}.
 *
 * @author patton
 */
@XmlType(propOrder = { "time",
                       "details" })
public class DigestChange extends
                          DigestItem implements
                          ChangedItem {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The collection, if any, of details of the change.
     */
    private List<Detail> details;

    /**
     * The date and time associated with this item.
     */
    private Date time;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected DigestChange() {
    }

    /**
     * Create an instance of this class.
     *
     * @param rhs
     *            the {@link DigestChange} instance to copy.
     */
    private DigestChange(DigestChange rhs) {
        setDetails(rhs.getDetails());
        setItem(rhs.getItem());
        setTime(new Date((rhs.getTime()).getTime()));
    }

    /**
     * Create an instance of this class.
     *
     * @param item
     *            the identity of the item in this line.
     * @param dateTime
     *            the date and time associated with this item.
     */
    public DigestChange(String item,
                        Date dateTime) {
        this(item,
             dateTime,
             null);
    }

    /**
     * Create an instance of this class.
     *
     * @param item
     *            the identity of the item in this line.
     * @param dateTime
     *            the date and time associated with this item.
     * @param details
     *            the collection, if any, of details of the change.
     */
    public DigestChange(final String item,
                        final Date dateTime,
                        final List<Detail> details) {
        super(item);
        setDetails(details);
        setTime(dateTime);
    }

    // instance member method (alphabetic)

    @Override
    public Object clone() {
        return new DigestChange(this);
    }

    @Override
    public int compareTo(final ChangedItem o) {
        return getWhenItemChanged().compareTo(o.getWhenItemChanged());
    }

    /**
     * Returns the collection, if any, of details of the change.
     *
     * @return the collection, if any, of details of the change.
     */
    @XmlElement(name = "detail")
    public List<Detail> getDetails() {
        return details;
    }

    /**
     * Returns the date and time associated with this item.
     *
     * @return the date and time associated with this item.
     */
    @XmlElement
    public Date getTime() {
        return time;
    }

    @Override
    @XmlTransient
    public Date getWhenItemChanged() {
        return getTime();
    }

    /**
     * Sets the collection, if any, of details of the change.
     *
     * @param details
     *            the collection, if any, of details of the change.
     */
    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    /**
     * Sets the date and time associated with this item.
     *
     * @param dateTime
     *            the date and time associated with this item.
     */
    public void setTime(Date dateTime) {
        time = dateTime;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
