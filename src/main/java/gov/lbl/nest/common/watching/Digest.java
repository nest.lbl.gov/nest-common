package gov.lbl.nest.common.watching;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.xml.sax.SAXParseException;

import gov.lbl.nest.common.rs.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class contains a sequence changes to items. These changes may be
 * issuance (or creation), revocation (or deletion), modification, or simply
 * queuing (or waiting) for a change.
 *
 * @author patton
 */
@XmlRootElement(name = "digest")
@XmlType(propOrder = { "subject",
                       "begin",
                       "end",
                       "items",
                       "issued",
                       "modified",
                       "revoked",
                       "queued" })
public class Digest extends
                    Resource {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The http response code for OK.
     */
    private static final int HTTP_STATUS_OK = 200;

    /**
     * The http response code for no content.
     */
    private static final int HTTP_STATUS_NO_CONTENT = 204;

    private static final Comparator<DigestChange> MOST_RECENT_FIRST = new Comparator<>() {

        @Override
        public int compare(DigestChange arg0,
                           DigestChange arg1) {
            return arg0.compareTo(arg1);
        }
    };

    // private static member data

    // private instance member data

    /**
     * Creates a {@link Digest} from the specified {@link File}.
     *
     * @param file
     *            the {@link File} from which the {@link Digest} should be created.
     *
     * @return the {@link Digest} created from the specified {@link String}.
     *
     * @throws DigestException
     *             when the {@link File} does not contain a {@link Digest} instance.
     * @throws FileNotFoundException
     *             when the specified file does not exist.
     */
    public static Digest fromFile(File file) throws DigestException,
                                             FileNotFoundException {
        return fromInputStream(new FileInputStream(file));
    }

    /**
     * Creates a {@link Digest} from the specified {@link InputStream}.
     *
     * @param inputStream
     *            the {@link InputStream} from which the {@link Digest} should be
     *            created.
     *
     * @return the {@link Digest} created from the specified {@link InputStream}.
     *
     * @throws DigestException
     *             when the {@link InputStream} does not contain a {@link Digest}
     *             instance.
     */
    public static Digest fromInputStream(InputStream inputStream) throws DigestException {
        try {
            JAXBContext content = JAXBContext.newInstance(Digest.class);
            Unmarshaller unmarshaller = content.createUnmarshaller();
            return (Digest) unmarshaller.unmarshal(inputStream);
        } catch (UnmarshalException e) {
            final Throwable cause = e.getCause();
            if (null != cause && cause instanceof SAXParseException) {
                // Means that a Digest was not returned.
                return null;
            }
            throw new DigestException(e);
        } catch (JAXBException e) {
            throw new DigestException(e);
        }
    }

    /**
     * Creates a {@link Digest} from the specified {@link String}.
     *
     * @param string
     *            the {@link String} from which the {@link Digest} should be
     *            created.
     *
     * @return the {@link Digest} created from the specified {@link String}.
     *
     * @throws DigestException
     *             when the {@link String} does not contain a {@link Digest}
     *             instance.
     */
    public static Digest fromString(String string) throws DigestException {
        return fromInputStream(new ByteArrayInputStream(string.getBytes()));
    }

    /**
     * Creates a {@link Digest} from the specified {@link URL}.
     *
     * @param url
     *            the {@link URL} from which the {@link Digest} should be created.
     *
     * @return the {@link Digest} created from the specified {@link String}.
     *
     * @throws DigestException
     *             when the {@link URL} does not contain a {@link Digest} instance.
     */
    public static Digest fromURL(URL url) throws DigestException {
        try {
            JAXBContext content = JAXBContext.newInstance(Digest.class);
            Unmarshaller unmarshaller = content.createUnmarshaller();
            return (Digest) unmarshaller.unmarshal(url);
        } catch (JAXBException e) {
            throw new DigestException(e);
        }
    }

    /**
     * Creates a {@link Digest} from POSTing the supplied attachment (which can be
     * marshaled into XML) to the specified {@link URL}.
     *
     * @param url
     *            the {@link URL} from which the {@link Digest} should be created.
     * @param attachment
     *            the Object, which can be marshaled into XML, that will be posted
     *            to the supplied {@link URL}.
     *
     * @return the {@link Digest} created from the specified {@link String}.
     *
     * @throws DigestException
     *             when the {@link URL} does not contain a {@link Digest} instance.
     */
    public static Digest fromURL(URL url,
                                 Object attachment) throws DigestException {
        try {
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Accept",
                                          "application/xml");
            connection.setRequestProperty("Content-Type",
                                          "application/xml");
            connection.connect();
            JAXBContext context = JAXBContext.newInstance(attachment.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                                   true);
            try (final OutputStream os = connection.getOutputStream()) {
                marshaller.marshal(attachment,
                                   os);
            }
            if (HTTP_STATUS_NO_CONTENT == connection.getResponseCode()) {
                return null;
            }
            if (HTTP_STATUS_OK != connection.getResponseCode()) {
                throw new IOException("HTTP response was " + connection.getResponseCode()
                                      + ", not "
                                      + HTTP_STATUS_OK
                                      + " that is the expected value");
            }
            try (final InputStream inputStream = connection.getInputStream()) {
                return fromInputStream(inputStream);
            }
        } catch (Exception e) {
            throw new DigestException(e);
        }
    }

    /**
     * The date and time at or after which items are included in this object.
     */
    private Date begin;

    /**
     * The date and time before which items are included in this object.
     */
    private Date end;

    /**
     * The list of {@link DigestChange} instance that were issued during the period
     * of this object.
     */
    private List<? extends DigestChange> issued;

    // constructors

    /**
     * The list of {@link DigestItem} instances of note that are unchanged during
     * the period of this object.
     */
    private List<? extends DigestItem> namedItems;

    /**
     * The list of items that were modified during the period of this object.
     */
    private List<? extends DigestChange> modified;

    /**
     * The list of items that were still in the queue for action at the end of the
     * period of this object.
     */
    private List<? extends String> queued;

    /**
     * The list of {@link DigestChange} instance that were revoked during the period
     * of this object.
     */
    private List<? extends DigestChange> revoked;

    /**
     * The subject to which the {@link DigestChange} instances refer.
     */
    private String subject;

    /**
     * Create an instance of this class.
     */
    public Digest() {
    }

    /**
     * Create an instance of this class.
     *
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param begin
     *            the date and time at or after which items are included in this
     *            object.
     * @param end
     *            the date and time before which items are included in this object.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     */
    public Digest(String subject,
                  Date begin,
                  Date end,
                  List<? extends ChangedItem> issued) {
        this(null,
             subject,
             begin,
             end,
             null,
             issued,
             null,
             null,
             null);
    }

    /**
     * Create an instance of this class.
     *
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param begin
     *            the date and time at or after which items are included in this
     *            object.
     * @param end
     *            the date and time before which items are included in this object.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     * @param revoked
     *            the list of {@link ChangedItem} instance that were revoked during
     *            the period of this object.
     */
    public Digest(String subject,
                  Date begin,
                  Date end,
                  List<? extends ChangedItem> issued,
                  List<? extends ChangedItem> revoked) {
        this(null,
             subject,
             begin,
             end,
             null,
             issued,
             revoked,
             null,
             null);
    }

    /**
     * Create an instance of this class.
     *
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param begin
     *            the date and time at or after which items are included in this
     *            object.
     * @param end
     *            the date and time before which items are included in this object.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     * @param revoked
     *            the list of {@link ChangedItem} instance that were revoked during
     *            the period of this object.
     * @param modified
     *            the list of items that were modified during the period of this
     *            object.
     */
    public Digest(String subject,
                  Date begin,
                  Date end,
                  List<? extends ChangedItem> issued,
                  List<? extends ChangedItem> revoked,
                  List<? extends ChangedItem> modified) {
        this(null,
             subject,
             begin,
             end,
             null,
             issued,
             revoked,
             modified,
             null);
    }

    /**
     * Create an instance of this class.
     *
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param begin
     *            the date and time at or after which items are included in this
     *            object.
     * @param end
     *            the date and time before which items are included in this object.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     * @param revoked
     *            the list of {@link ChangedItem} instance that were revoked during
     *            the period of this object.
     * @param modified
     *            the list of items that were modified during the period of this
     *            object.
     * @param queued
     *            the list of items that were still in the queue for action at the
     *            end of the period of this object.
     */
    public Digest(String subject,
                  Date begin,
                  Date end,
                  List<? extends ChangedItem> issued,
                  List<? extends ChangedItem> revoked,
                  List<? extends ChangedItem> modified,
                  List<? extends String> queued) {
        this(null,
             subject,
             begin,
             end,
             null,
             issued,
             revoked,
             modified,
             queued);
    }

    /**
     * Create an instance of this class.
     *
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param begin
     *            the date and time at or after which items are included in this
     *            object.
     * @param end
     *            the date and time before which items are included in this object.
     * @param namedItems
     *            the list of {@link DigestItem} instances of note that are
     *            unchanged during the period of this object.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     * @param revoked
     *            the list of {@link ChangedItem} instance that were revoked during
     *            the period of this object.
     * @param modified
     *            the list of items that were modified during the period of this
     *            object.
     * @param queued
     *            the list of items that were still in the queue for action at the
     *            end of the period of this object.
     */
    public Digest(String subject,
                  Date begin,
                  Date end,
                  List<? extends DigestItem> namedItems,
                  List<? extends ChangedItem> issued,
                  List<? extends ChangedItem> revoked,
                  List<? extends ChangedItem> modified,
                  List<? extends String> queued) {
        this(null,
             subject,
             begin,
             end,
             namedItems,
             issued,
             revoked,
             modified,
             queued);
    }

    /**
     * Create an instance of this class.
     *
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     */
    public Digest(String subject,
                  List<? extends ChangedItem> issued) {
        this(null,
             subject,
             null,
             null,
             null,
             issued,
             null,
             null,
             null);
    }

    /**
     * Create an instance of this class.
     *
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     * @param revoked
     *            the list of {@link ChangedItem} instance that were revoked during
     *            the period of this object.
     */
    public Digest(String subject,
                  List<? extends ChangedItem> issued,
                  List<? extends ChangedItem> revoked) {
        this(null,
             subject,
             null,
             null,
             null,
             issued,
             revoked,
             null,
             null);
    }

    /**
     * Create an instance of this class.
     *
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     * @param revoked
     *            the list of {@link ChangedItem} instance that were revoked during
     *            the period of this object.
     * @param modified
     *            the list of items that existed during the period of this object.
     */
    public Digest(String subject,
                  List<? extends ChangedItem> issued,
                  List<? extends ChangedItem> revoked,
                  List<? extends ChangedItem> modified) {
        this(null,
             subject,
             null,
             null,
             null,
             issued,
             revoked,
             modified,
             null);
    }

    /**
     * Create an instance of this class.
     *
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     * @param revoked
     *            the list of {@link ChangedItem} instance that were revoked during
     *            the period of this object.
     * @param modified
     *            the list of items that were modified during the period of this
     *            object.
     * @param queued
     *            the list of items that were still in the queue for action at the
     *            end of the period of this object.
     */
    public Digest(String subject,
                  List<? extends ChangedItem> issued,
                  List<? extends ChangedItem> revoked,
                  List<? extends ChangedItem> modified,
                  List<? extends String> queued) {
        this(null,
             subject,
             null,
             null,
             null,
             issued,
             revoked,
             modified,
             queued);
    }

    /**
     * Create an instance of this class.
     *
     * @param uri
     *            the URI, if any, that is related to the content of this object.
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param begin
     *            the date and time at or after which items are included in this
     *            object.
     * @param end
     *            the date and time before which items are included in this object.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     */
    public Digest(URI uri,
                  String subject,
                  Date begin,
                  Date end,
                  List<? extends ChangedItem> issued) {
        this(uri,
             subject,
             begin,
             end,
             null,
             issued,
             null,
             null,
             null);
    }

    // instance member method (alphabetic)

    /**
     * Create an instance of this class.
     *
     * @param uri
     *            the URI, if any, that is related to the content of this object.
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param begin
     *            the date and time at or after which items are included in this
     *            object.
     * @param end
     *            the date and time before which items are included in this object.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     * @param revoked
     *            the list of {@link ChangedItem} instance that were revoked during
     *            the period of this object.
     */
    public Digest(URI uri,
                  String subject,
                  Date begin,
                  Date end,
                  List<? extends ChangedItem> issued,
                  List<? extends ChangedItem> revoked) {
        this(uri,
             subject,
             begin,
             end,
             null,
             issued,
             revoked,
             null,
             null);
    }

    /**
     * Create an instance of this class.
     *
     * @param uri
     *            the URI, if any, that is related to the content of this object.
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param begin
     *            the date and time at or after which items are included in this
     *            object.
     * @param end
     *            the date and time before which items are included in this object.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     * @param revoked
     *            the list of {@link ChangedItem} instance that were revoked during
     *            the period of this object.
     * @param modified
     *            the list of items that were modified during the period of this
     *            object.
     */
    public Digest(URI uri,
                  String subject,
                  Date begin,
                  Date end,
                  List<? extends ChangedItem> issued,
                  List<? extends ChangedItem> revoked,
                  List<? extends ChangedItem> modified) {
        this(uri,
             subject,
             begin,
             end,
             null,
             issued,
             revoked,
             modified,
             null);
    }

    /**
     * Create an instance of this class.
     *
     * @param uri
     *            the URI, if any, that is related to the content of this object.
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param begin
     *            the date and time at or after which items are included in this
     *            object.
     * @param end
     *            the date and time before which items are included in this object.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     * @param revoked
     *            the list of {@link ChangedItem} instance that were revoked during
     *            the period of this object.
     * @param modified
     *            the list of items that were modified during the period of this
     *            object.
     * @param queued
     *            the list of items that were still in the queue for action at the
     *            end of the period of this object.
     */
    public Digest(URI uri,
                  String subject,
                  Date begin,
                  Date end,
                  List<? extends ChangedItem> issued,
                  List<? extends ChangedItem> revoked,
                  List<? extends ChangedItem> modified,
                  List<? extends String> queued) {
        this(uri,
             subject,
             begin,
             end,
             null,
             issued,
             revoked,
             modified,
             queued);
    }

    /**
     * Create an instance of this class.
     *
     * @param uri
     *            the URI, if any, that is related to the content of this object.
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param begin
     *            the date and time at or after which items are included in this
     *            object.
     * @param end
     *            the date and time before which items are included in this object.
     * @param notedItems
     *            the list of {@link DigestItem} instances of note that are
     *            unchanged during the period of this object.
     * @param issued
     *            the list of {@link ChangedItem} instance that were issued during
     *            the period of this object.
     * @param revoked
     *            the list of {@link ChangedItem} instance that were revoked during
     *            the period of this object.
     * @param modified
     *            the list of items that were modified during the period of this
     *            object.
     * @param queued
     *            the list of items that were still in the queue for action at the
     *            end of the period of this object.
     */
    public Digest(URI uri,
                  String subject,
                  Date begin,
                  Date end,
                  List<? extends DigestItem> notedItems,
                  List<? extends ChangedItem> issued,
                  List<? extends ChangedItem> revoked,
                  List<? extends ChangedItem> modified,
                  List<? extends String> queued) {
        super(uri);
        setBegin(begin);
        setEnd(end);
        setSubject(subject);

        try {
            if (null != notedItems) {
                setItems(new ArrayList<>(notedItems));
            }
            if (null != issued) {
                final List<DigestChange> changes = new ArrayList<>();
                for (ChangedItem change : issued) {
                    if (change instanceof DigestChange) {
                        changes.add((DigestChange) change.clone());
                    } else {
                        changes.add(new DigestChange(change.getItem(),
                                                     change.getWhenItemChanged()));
                    }
                }
                Collections.sort(changes,
                                 MOST_RECENT_FIRST);
                setIssued(changes);
            }

            if (null != revoked) {
                final List<DigestChange> changes = new ArrayList<>();
                for (ChangedItem change : revoked) {
                    if (change instanceof DigestChange) {
                        changes.add((DigestChange) change.clone());
                    } else {
                        changes.add(new DigestChange(change.getItem(),
                                                     change.getWhenItemChanged()));
                    }
                }
                Collections.sort(changes,
                                 MOST_RECENT_FIRST);
                setRevoked(changes);
            }

            if (null != modified) {
                final List<DigestChange> changes = new ArrayList<>();
                for (ChangedItem change : revoked) {
                    if (change instanceof DigestChange) {
                        changes.add((DigestChange) change.clone());
                    } else {
                        changes.add(new DigestChange(change.getItem(),
                                                     change.getWhenItemChanged()));
                    }
                }
                Collections.sort(changes,
                                 MOST_RECENT_FIRST);
                setModified(changes);
            }
        } catch (CloneNotSupportedException e) {
            throw new IllegalArgumentException(e);
        }

        if (null != queued) {
            setQueued(new ArrayList<>(queued));
        }
    }

    /**
     * Create an instance of this class.
     *
     * @param uri
     *            the URI, if any, that is related to the content of this object.
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param namedItems
     *            the list of {@link DigestItem} instances of note that are
     *            unchanged during the period of this object.
     */
    public Digest(URI uri,
                  String subject,
                  List<? extends DigestItem> namedItems) {
        this(uri,
             subject,
             null,
             null,
             namedItems,
             null,
             null,
             null,
             null);
    }

    /**
     * Returns the date and time at or after which items are included in this
     * object.
     *
     * @return the date and time at or after which items are included in this
     *         object.
     */
    @XmlElement
    public Date getBegin() {
        return begin;
    }

    /**
     * Returns the date and time before which items are included in this object.
     *
     * @return the date and time before which items are included in this object.
     */
    @XmlElement
    public Date getEnd() {
        return end;
    }

    /**
     * Returns the list of {@link DigestChange} instance that were issued during the
     * period of this object.
     *
     * @return the list of {@link DigestChange} instance that were issued during the
     *         period of this object.
     */
    @XmlElement(name = "issued")
    @XmlElementWrapper(name = "issue")
    public List<? extends DigestChange> getIssued() {
        return issued;
    }

    /**
     * Returns the list of {@link DigestItem} instances of note that are unchanged
     * during the period of this object.
     *
     * @return the list of {@link DigestItem} instances of note that are unchanged
     *         during the period of this object.
     */
    @XmlElement(name = "item")
    @XmlElementWrapper(name = "items")
    public List<? extends DigestItem> getItems() {
        return namedItems;
    }

    /**
     * Returns the list of items that were modified during the period of this
     * object.
     *
     * @return the list of items that were modified during the period of this
     *         object.
     */
    @XmlElement(name = "modified")
    @XmlElementWrapper(name = "modify")
    public List<? extends DigestChange> getModified() {
        return modified;
    }

    /**
     * Returns the list of items that were still in the queue for action at the end
     * of the period of this object.
     *
     * @return the list of items that were still in the queue for action at the end
     *         of the period of this object.
     */
    @XmlElement(name = "queued")
    @XmlElementWrapper(name = "queue")
    public List<? extends String> getQueued() {
        return queued;
    }

    /**
     * Returns the list of {@link DigestChange} instance that were revoked during
     * the period of this object.
     *
     * @return the list of {@link DigestChange} instance that were revoked during
     *         the period of this object.
     */
    @XmlElement(name = "revoked")
    @XmlElementWrapper(name = "revoke")
    public List<? extends DigestChange> getRevoked() {
        return revoked;
    }

    /**
     * Returns the subject to which the {@link DigestChange} instances refer.
     *
     * @return the subject to which the {@link DigestChange} instances refer.
     */
    @XmlElement
    public String getSubject() {
        return subject;
    }

    /**
     * Sets the date and time at or after which items are included in this object.
     *
     * @param dateTime
     *            the date and time at or after which items are included in this
     *            object.
     */
    protected void setBegin(Date dateTime) {
        this.begin = dateTime;
    }

    /**
     * Sets the date and time before which items are included in this object.
     *
     * @param dateTime
     *            the date and time before which items are included in this object.
     */
    protected void setEnd(Date dateTime) {
        this.end = dateTime;
    }

    /**
     * Sets the list of {@link DigestChange} instance that were issued during the
     * period of this object.
     *
     * @param changes
     *            the list of {@link DigestChange} instance that were issued during
     *            the period of this object.
     */
    protected void setIssued(List<? extends DigestChange> changes) {
        this.issued = changes;
    }

    /**
     * Sets the list of {@link DigestItem} instances of note that are unchanged
     * during the period of this object.
     *
     * @param namedItems
     *            the list of {@link DigestItem} instances of note that are
     *            unchanged during the period of this object.
     */
    public void setItems(List<? extends DigestItem> namedItems) {
        this.namedItems = namedItems;
    }

    /**
     * Sets the list of items that existed during the period of this object.
     *
     * @param modifications
     *            the list of items that existed during the period of this object.
     */
    protected void setModified(List<? extends DigestChange> modifications) {
        modified = modifications;
    }

    /**
     * Sets the list of items that where awaiting action during the period of this
     * object.
     *
     * @param items
     *            the list of items that where awaiting action during the period of
     *            this object.
     */
    protected void setQueued(List<? extends String> items) {
        this.queued = items;
    }

    /**
     * Sets the list of {@link DigestChange} instance that were revoked during the
     * period of this object.
     *
     * @param changes
     *            the list of {@link DigestChange} instance that were revoked during
     *            the period of this object.
     */
    protected void setRevoked(List<? extends DigestChange> changes) {
        this.revoked = changes;
    }

    // static member methods (alphabetic)

    /**
     * Sets the subject to which the {@link DigestChange} instances refer.
     *
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Sets the URI of this resource.
     *
     * @param uri
     *            the URI of this resource.
     */
    @Override
    public void setUri(URI uri) {
        super.setUri(uri);
    }

    /**
     * Write this object to the specified {@link File}.
     *
     * @param file
     *            the file into which to write this object.
     *
     * @throws IOException
     *             when the file can not be accessed
     * @throws DigestException
     *             when this object can not be written.
     */
    public void toFile(File file) throws IOException,
                                  DigestException {
        final Writer writer = new FileWriter(file);
        writeDigest(writer);
    }

    // Description of this object.
    @Override
    public String toString() {
        try {
            final StringWriter writer = new StringWriter();
            writeDigest(writer);
            return writer.toString();
        } catch (DigestException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Writes this object out to the specified {@link Writer}.
     *
     * @param writer
     *            the {@link Writer}into which to write this object.
     *
     * @throws DigestException
     *             when the {@link Digest} can not be written.
     */
    private void writeDigest(Writer writer) throws DigestException {
        try {
            JAXBContext context = JAXBContext.newInstance(Digest.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                                   true);
            marshaller.marshal(this,
                               writer);
        } catch (JAXBException e) {
            throw new DigestException(e);
        }
    }

    // public static void main(String args[]) {}
}
