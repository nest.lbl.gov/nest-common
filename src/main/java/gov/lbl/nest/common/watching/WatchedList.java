package gov.lbl.nest.common.watching;

import java.util.Date;
import java.util.List;

/**
 * This interface is used to return a sequence of blocks from a time-ordered
 * list.
 *
 * @author patton
 */
public interface WatchedList {

    /**
     * This interface defins the callback method that must be implemented to return
     * new items.
     *
     * @author patton
     */
    interface Callback {
        /**
         * Returns the list of {@link ChangedItem} instance that are at or after the
         * current high water mark for the list being read.
         *
         * @param highWater
         *            the date and time of the last item previously read from the list.
         * @param blockSize
         *            the maximum number of items to be read from the list.
         *
         * @return the list of {@link ChangedItem} instance that are at or after the
         *         current high water mark for the list being read.
         */
        List<? extends ChangedItem> getItemsOnOrAfter(Date highWater,
                                                      Integer blockSize);
    }

    /**
     * Returns the next sequence of {@link ChangedItem} instance being watched.
     *
     * @return the next sequence of {@link ChangedItem} instance being watched.
     */
    List<? extends ChangedItem> getNextBlock();

    /**
     * Returns the next sequence of {@link ChangedItem} instance being watched, up
     * to the specified limit.
     *
     * @param blockLimit
     *            the maximinm number of {@link ChangedItem} instance in the
     *            returned sequence.
     *
     * @return the next sequence of {@link ChangedItem} instance being watched, up
     *         to the specified limit.
     */
    List<? extends ChangedItem> getNextBlock(Integer blockLimit);
}
