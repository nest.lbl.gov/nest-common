package gov.lbl.nest.common.watching;

/**
 * This interface is used to identify an item within in this package.
 *
 * @author patton
 *
 */
public interface NamedItem {

    /**
     * Returns the identity of the item in this package.
     *
     * @return the identity of the item in this package.
     */
    String getItem();

}
