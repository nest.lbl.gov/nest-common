package gov.lbl.nest.common.configure;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements a generic mechanism to find a mapping between disk
 * mount points.
 *
 * @author patton
 */
public class MountPointsMapping {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * A String with no content.
     */
    private static final String EMPTY_STRING = "";

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(MountPointsMapping.class);

    // private static member data

    // private instance member data

    /**
     * The {@link CustomConfigPath} used to locate the resource specifying the
     * {@link Properties} file.
     */
    private final CustomConfigPath customConfigPath;

    // constructors

    /**
     * Crates an instance of this class.
     *
     * @param clazz
     *            the class for which the mapping file will be used.
     * @param resource
     *            the name of the resource, if any, that holds the path to the
     *            {@link Properties} file.
     * @param defaultPropertiesPath
     *            the path the to properties file if no resource exist or it is
     *            empty.
     */
    public MountPointsMapping(final Class<?> clazz,
                              final String resource,
                              final File defaultPropertiesPath) {
        customConfigPath = new CustomConfigPath(clazz,
                                                resource,
                                                defaultPropertiesPath);
    }

    /**
     * Returns the {@link File} that results from mapping the supplied file path.
     *
     * @param original
     *            the file path to be mapped.
     *
     * @return the directory within the warehouse as indicated by original remote
     *         directory of the data file.
     *
     * @throws IllegalArgumentException
     *             when the supplied file path is not found in the mapping.
     * @throws IOException
     *             when the mount points information can not be loaded.
     */
    public File getMappedFile(final String original) throws IOException {
        final String originalMountPoint = getOriginalMountPoint(original);
        if (null == originalMountPoint) {
            final String message = "There was no mount point mapping for the path \"" + original
                                   + "\"";
            LOG.error(message);
            throw new IllegalArgumentException();
        }
        final String localMountPoint = getProperties().getProperty(originalMountPoint);
        final File result = new File(original.replaceFirst(originalMountPoint,
                                                           localMountPoint));
        return result;
    }

    /**
     * Returns the effective mount point of the supplied path based on the available
     * mappings.
     *
     * @param path
     *            the file path the be mapped.
     *
     * @return the effective mount point of the supplied path based on the available
     *         mappings.
     *
     * @throws IOException
     *             when the mount points information can not be loaded.
     */
    private String getOriginalMountPoint(final String path) throws IOException {
        final Set<String> originalMounts = getProperties().stringPropertyNames();
        String result = EMPTY_STRING;
        for (String originalMount : originalMounts) {
            if (path.startsWith(originalMount) && originalMount.length() > result.length()) {
                result = originalMount;
            }
        }
        if (EMPTY_STRING.equals(result)) {
            return null;
        }
        return result;
    }

    /**
     * Returns the {@link Properties} defining the mapping between disk mount
     * points.
     *
     * @return the {@link Properties} defining the mapping between disk mount
     *         points.
     *
     * @throws IOException
     *             when the mount points information can not be loaded.
     */
    private Properties getProperties() throws IOException {
        return customConfigPath.getProperties();
    }

    /**
     * Returns the {@link File} that results from mapping the supplied file path.
     *
     * @param original
     *            the file path to be mapped.
     *
     * @return the directory within the warehouse as indicated by original remote
     *         directory of the data file. <code>null</code> is returned if there is
     *         no mapping.
     *
     * @throws IOException
     *             when the mount points information can not be loaded.
     */
    public File peekMappedFile(final String original) throws IOException {
        final String originalMountPoint = getOriginalMountPoint(original);
        if (null == originalMountPoint) {
            return null;
        }
        final String localMountPoint = getProperties().getProperty(originalMountPoint);
        final File result = new File(original.replaceFirst(originalMountPoint,
                                                           localMountPoint));
        return result;
    }
}
