package gov.lbl.nest.common.configure;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements a generic mechanism to find a custom configuration
 * file.
 *
 * @author patton
 */
public class CustomConfigPath {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CustomConfigPath.class);

    // private static member data

    // private instance member data

    /**
     * The class for which the custom configuration file will be used.
     */
    private final Class<?> clazz;

    /**
     * True if the {@link #configPath} value has been loaded.
     */
    private boolean pathLoaded = false;

    /**
     * The {@link File}, if required, containing the default {@link Properties}
     * instance if no resource exist or it is empty.
     */
    private final File defaultPropertiesPath;

    /**
     * The {@link Path} to the custom configuration file, if any.
     */
    private Path configPath;

    /**
     * The {@link Properties} instance, if any, read in by this Object.
     */
    private Properties properties;

    /**
     * The name of the resource, is any, that holds the path to the custom
     * configuration file.
     */
    private final String resource;

    /**
     * The list of property names that should <em>not</em> be reported in the log
     * files.
     */
    private final List<String> sensitiveProperties;

    // constructors

    /**
     * Crates an instance of this class.
     *
     * @param clazz
     *            the class for which the custom configuration file will be used.
     * @param resource
     *            the name of the resource, is any, that holds the path to the
     *            custom configuration file.
     */
    public CustomConfigPath(final Class<?> clazz,
                            final String resource) {
        this(clazz,
             resource,
             null,
             null);
    }

    /**
     * Crates an instance of this class.
     *
     * @param clazz
     *            the class for which the custom configuration file will be used.
     * @param resource
     *            the name of the resource, is any, that holds the path to the
     *            custom configuration file.
     * @param defaultProperties
     *            the {@link File}, if required, containing the default
     *            {@link Properties} instance if no resource exist or it is empty.
     */
    public CustomConfigPath(final Class<?> clazz,
                            final String resource,
                            final File defaultProperties) {
        this(clazz,
             resource,
             defaultProperties,
             null);
    }

    /**
     * Crates an instance of this class.
     *
     * @param clazz
     *            the class for which the custom configuration file will be used.
     * @param resource
     *            the name of the resource, is any, that holds the path to the
     *            custom configuration file.
     * @param defaultProperties
     *            the {@link File}, if required, containing the default
     *            {@link Properties} instance if no resource exist or it is empty.
     * @param sensitiveProperties
     *            the list of property names that should <em>not</em> be reported in
     *            the log files.
     */
    public CustomConfigPath(final Class<?> clazz,
                            final String resource,
                            final File defaultProperties,
                            List<String> sensitiveProperties) {
        this.clazz = clazz;
        defaultPropertiesPath = defaultProperties;
        this.resource = resource;
        if (null == sensitiveProperties) {
            this.sensitiveProperties = new ArrayList<>();
        } else {
            this.sensitiveProperties = sensitiveProperties;
        }
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link Path} to the custom configuration file, if any.
     *
     * @return the path to the custom configuration file, if any.
     */
    public synchronized Path getConfigPath() {
        if (pathLoaded) {
            return configPath;
        }
        try {
            final InputStream is = clazz.getResourceAsStream(resource);
            try (final BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
                String line = null;
                do {
                    line = br.readLine();
                    if (null != line && !"".equals(line)) {
                        final String lineToUse = line.trim();
                        if (!lineToUse.startsWith("#")) {
                            configPath = (new File(lineToUse).getAbsoluteFile()).toPath();
                            pathLoaded = true;
                            return configPath;
                        }
                    }
                } while (null != line);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (NullPointerException e) {
            pathLoaded = true;
        }
        return configPath;
    }

    /**
     * Returns the {@link Properties} instance define in the custom configuration
     * file.
     *
     * @return the {@link Properties} define in the custom configuration file.
     *
     * @throws IOException
     *             when the {@link Properties} instance can not be loaded.
     */
    public Properties getProperties() throws IOException {
        if (null != properties) {
            return properties;
        }
        if (null == defaultPropertiesPath) {
            properties = new Properties();
        }

        final Properties result = new Properties();
        try {
            final Path configPath = getConfigPath();
            final File properties;
            if (null == configPath) {
                properties = defaultPropertiesPath;
            } else {
                properties = configPath.toFile();
            }
            result.load(new FileInputStream(properties));
            final Set<String> names = result.stringPropertyNames();
            if (names.isEmpty()) {
                LOG.info("No Properties were specified for the \"" + getResource()
                         + "\" resource");
            } else {
                LOG.info("The following Properties were specified for the \"" + getResource()
                         + "\" resource");
                for (String name : names) {
                    if (sensitiveProperties.contains(name)) {
                        LOG.info("  " + name
                                 + " = "
                                 + "<provided>");
                    } else {
                        LOG.info("  " + name
                                 + " = "
                                 + result.getProperty(name));
                    }
                }
            }
        } catch (FileNotFoundException e) {
            LOG.warn("No Properties file was specified for the \"" + getResource()
                     + "\" class");
        }
        properties = result;
        return result;
    }

    /**
     * Returns the path to the resource that contains the {@link Path} to the custom
     * configuration file.
     *
     * @return the path to the resource that contains the {@link Path} to the custom
     *         configuration file.
     */
    public String getResource() {
        return clazz.getName() + "/"
               + resource;
    }

    /**
     * Sets the {@link File} to the custom configuration file if it has not already
     * been set.
     *
     * @param file
     *            the {@link File} to the custom configuration file.
     */
    public synchronized void setConfigPath(final File file) {
        if (pathLoaded) {
            throw new IllegalStateException("Custom configuration path has already been set.");
        }
        configPath = (file.getAbsoluteFile()).toPath();
        pathLoaded = true;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
