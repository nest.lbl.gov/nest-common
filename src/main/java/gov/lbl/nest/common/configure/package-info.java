/**
 * This package provides classes that help with the configuring an application.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.io}</li>
 * <li>{@link javax.xml.bind}</li>
 * </ul>
 *
 * @author patton
 */
package gov.lbl.nest.common.configure;