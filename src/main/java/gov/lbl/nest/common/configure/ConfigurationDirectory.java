package gov.lbl.nest.common.configure;

import java.io.File;

/**
 * This interface provides access to the directory that contains the
 * configuration files.
 *
 * @author patton
 */
public interface ConfigurationDirectory {

    /**
     * Returns the {@link File} that is the directory that contains the
     * configuration files.
     *
     * @return the {@link File} that is the directory that contains the
     *         configuration files.
     */
    File getDirectory();
}
