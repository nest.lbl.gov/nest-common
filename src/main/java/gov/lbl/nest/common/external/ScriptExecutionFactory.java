package gov.lbl.nest.common.external;

import java.io.File;
import java.io.IOException;
import java.net.URI;

/**
 * This interface is used to create instances of the {@link ScriptExecution}
 * class.
 *
 * @author patton
 */
public interface ScriptExecutionFactory {

    /**
     * Creates a instance of the {@link ScriptExecution} class.
     *
     * @param script
     *            the file containing the script to be executed in background.
     * @param reference
     *            the {@link URI} that identifies both originating part of a the
     *            application that is requesting the creation of a
     *            {@link ScriptExecution} instance.
     *
     * @return the created {@link ScriptExecution} instance.
     *
     * @throws IOException
     *             when the script can not be read.
     */
    ScriptExecution createScriptExecution(File script,
                                          URI reference) throws IOException;
}
