/**
 * This package provides classes that interact outside the executing JVM.
 *
 * Two basic usage patterns are deal with here.
 * <ul>
 * <li>Execution of a single command.</li>
 * <li>Execution of a set of command contained in a "script"</li>
 * </ul>
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.io}</li>
 * <li>{@link java.nio}</li>
 * <li>{@link java.net}</li>
 * <li>{@link java.util}</li>
 * <li>{@link javax.security.auth.login}</li>
 * <li>{@link javax.xml.bind}</li>
 * </ul>
 */
package gov.lbl.nest.common.external;
