/**
 * This package provides bash inplementations of the Script tools.
 *
 * @author patton
 *
 */
package gov.lbl.nest.common.external.bash;