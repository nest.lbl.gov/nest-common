package gov.lbl.nest.common.external;

import java.io.File;
import java.io.InputStream;
import java.net.URI;

import javax.security.auth.login.Configuration;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The class captures the result of an external Script execution.
 *
 * @author patton
 */
@XmlRootElement(name = "script_response")
@XmlType(propOrder = { "reference",
                       "result" })
public class ScriptResponse {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * Attempts to read in a {@link Configuration} instance from an XML file.
     *
     * @param stream
     *            the {@link File} containing the XML.
     *
     * @return the {@link Configuration} instance read from the file.
     *
     * @throws JAXBException
     *             when the setting file can not be read.
     */
    public static ScriptResponse load(final InputStream stream) throws JAXBException {
        JAXBContext content = JAXBContext.newInstance(ScriptResponse.class);
        Unmarshaller unmarshaller = content.createUnmarshaller();
        final ScriptResponse result = (ScriptResponse) unmarshaller.unmarshal(stream);
        return result;
    }

    /**
     * The URI used as the callback reference.
     */
    private URI reference;

    // constructors

    /**
     * The result of the script's external execution.
     */
    private ScriptResult result;

    /**
     * Create an instance of this class.
     */
    protected ScriptResponse() {
    }

    // instance member method (alphabetic)

    /**
     * Create an instance of this class.
     *
     * @param reference
     *            the URI used as the callback reference.
     * @param result
     *            the result of the script's external execution.
     */
    public ScriptResponse(URI reference,
                          ScriptResult result) {
        this.reference = reference;
        this.result = result;
    }

    /**
     * Returns the URI used as the callback reference.
     *
     * @return the URI used as the callback reference.
     */
    @XmlElement
    public URI getReference() {
        return reference;
    }

    /**
     * Returns the result of the script's external execution.
     *
     * @return the result of the script's external execution.
     */
    @XmlElement
    public ScriptResult getResult() {
        return result;
    }

    /**
     * Sets the URI used as the callback reference.
     *
     * @param reference
     *            the URI used as the callback reference.
     */
    protected void setReference(URI reference) {
        this.reference = reference;
    }

    // static member methods (alphabetic)

    /**
     * Sets the result of the script's external execution.
     *
     * @param result
     *            the result of the script's external execution.
     */
    protected void setResult(ScriptResult result) {
        this.result = result;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
