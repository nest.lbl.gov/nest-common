package gov.lbl.nest.common.external;

import java.io.IOException;

/**
 * This exception is thrown when the complete output of an external command has
 * not been successfully consumed for reasons other than an {@link IOException}.
 *
 * @author patton
 */
public class CompletionException extends
                                 IOException {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    public CompletionException() {
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
