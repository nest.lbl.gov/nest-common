package gov.lbl.nest.common.external;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * This class is used to manage synchronous execution of an external command,
 * that is to say executing is a separate process outside the JVM.
 *
 * @author patton
 */
public class Command {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The file use as a an empty input of discarded output.
     */
    private final static File NULL_DEVICE = new File("/dev/null");

    /**
     * The prefix to used in a temporary file is needed for standard output.
     */
    private final static String STDOUT_PREFIX = "nest.cmd.";

    /**
     * The suffix to used in a temporary file is needed for standard output.
     */
    private final static String STDOUT_SUFFIX = ".stdout";

    /**
     * The prefix to used in a temporary file is needed for standard error.
     */
    private final static String STDERR_PREFIX = STDOUT_PREFIX;

    /**
     * The suffix to used in a temporary file is needed for standard error.
     */
    private final static String STDERR_SUFFIX = ".stderr";

    // private static member data

    // private instance member data

    /**
     * Creates an input file from an {@link InputStream}. Copies all bytes from an
     * input stream to a file. On return, the input stream will be at end of stream.
     *
     * @param inputStream
     *            the {@link InputStream} providing the contents of the new file.
     *
     * @return the newly created temporary file.
     *
     * @throws IOException
     */
    private static File createInputFile(final InputStream inputStream) throws IOException {
        if (null == inputStream) {
            return null;
        }
        final File inputFile = File.createTempFile("nest.cmd",
                                                   ".stdin");
        Files.copy(inputStream,
                   inputFile.toPath(),
                   new CopyOption[] {});
        return inputFile;
    }

    /**
     * Executes an instance of this object using the command line input are the
     * command to execute.
     *
     * @param args
     *            the arguments from the command line.
     *
     * @throws IOException
     *             when the command can not be completed because of an IO problem.
     * @throws InterruptedException
     *             when it is interrupted while waiting for the command to complete.
     *
     */
    public static void main(String args[]) throws IOException,
                                           InterruptedException {
        final InputStream inputStream;
        int begin = 0;
        if (args[begin].startsWith("-i")) {
            begin += 1;
            inputStream = System.in;
        } else {
            inputStream = null;
        }

        final File outputFile;
        if (args[begin].startsWith("-o")) {
            if (2 == args[begin].length()) {
                outputFile = new File(args[begin + 1]);
                begin += 2;
            } else {
                outputFile = new File(args[begin].substring(2));
                begin += 1;
            }
        } else {
            outputFile = null;
        }

        final File errorFile;
        if (args[begin].startsWith("-e")) {
            if (2 == args[begin].length()) {
                errorFile = new File(args[begin + 1]);
                begin += 2;
            } else {
                errorFile = new File(args[begin].substring(2));
                begin += 1;
            }
        } else {
            errorFile = null;
        }

        String[] arguments = new String[args.length - begin];
        System.arraycopy(args,
                         begin,
                         arguments,
                         0,
                         arguments.length);
        final Command command = new Command(arguments);
        final int result;
        if (null == inputStream) {
            result = command.execute(outputFile,
                                     errorFile,
                                     (File) null);
        } else {
            result = command.execute(outputFile,
                                     errorFile,
                                     inputStream);
        }
        if (null == outputFile) {
            final List<String> output = command.getStdOut();
            if (!output.isEmpty()) {
                System.out.println("Standard Out:");
                for (String line : output) {
                    System.out.println(line);
                }
            }
        }
        if (null == errorFile) {
            final List<String> error = command.getStdErr();
            if (!error.isEmpty()) {
                System.out.println("Standard Error:");
                for (String line : error) {
                    System.err.println(line);
                }
            }
        }

        final Runtime runtime = Runtime.getRuntime();
        runtime.exit(result);
    }

    /**
     * Transforms a command array into a single String.
     *
     * @param cmdArray
     *            the command array to transform.
     *
     * @return the command array as a sting String.
     */
    private static String toString(List<String> command) {
        final StringBuffer result = new StringBuffer();
        String conjunction = "";
        for (String element : command) {
            final String delimiter;
            if (-1 == element.indexOf(' ')) {
                delimiter = "";
            } else {
                delimiter = "\"";
            }
            result.append(conjunction + delimiter
                          + element
                          + delimiter);
            conjunction = " ";
        }
        return result.toString();
    }

    /**
     * The ProcessBuilder that is managing the execution of the external command.
     */
    private final ProcessBuilder processBuilder;

    /**
     * The {@link ExecutionFailedException}, if any, relevant to the execution of
     * this object.
     */
    private ExecutionFailedException cmdException;

    /**
     * The {@link IOException}, if any, that cause this object to fail.
     */
    private IOException exception;

    /**
     * The {@link Process} in which the command is executing.
     */
    private Process process;

    /**
     * The temporary {@link File}, if any, into which the standard error will be
     * written.
     */
    private File stderr;

    // constructors

    /**
     * The temporary {@link File}, if any, into which the standard output will be
     * written.
     */
    private File stdout;

    /**
     * The final contents, if any, of the standard error.
     */
    private List<String> finalStderr;

    /**
     * The final contents, if any, of the standard output.
     */
    private List<String> finalStdout;

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class. There is no simple {@link String}
     * constructor as that can create ambiguities when parsing.
     *
     * @param cmdarray
     *            array containing the command to call and its arguments.
     */
    public Command(final String[] cmdarray) {
        this(cmdarray,
             null,
             true);
    }

    /**
     * Creates an instance of this class. There is no simple {@link String}
     * constructor as that can create ambiguities when parsing.
     *
     * @param cmdarray
     *            array containing the command to call and its arguments.
     * @param env
     *            the {@link Map} of environmental variables to add or modify to the
     *            inherited environment of the command.
     */
    public Command(final String[] cmdarray,
                   final Map<String, String> env) {
        this(cmdarray,
             env,
             true);
    }

    /**
     * Creates an instance of this class. There is no simple {@link String}
     * constructor as that can create ambiguities when parsing.
     *
     * @param cmdarray
     *            array containing the command to call and its arguments.
     * @param env
     *            the {@link Map} of environmental variables to add or modify the
     *            environment of the command.
     * @param inherit
     *            true if the command should inherit its environment from the
     *            current one.
     */
    public Command(final String[] cmdarray,
                   final Map<String, String> env,
                   final boolean inherit) {
        processBuilder = new ProcessBuilder(cmdarray);
        final Map<String, String> map = processBuilder.environment();
        if (!inherit) {
            map.clear();
        }
        if (null != env) {
            map.putAll(env);
        }
    }

    /**
     * Executes the command. If this method has already been invoked then the
     * executing will block until the command is complete or the thread is
     * interrupted.
     *
     * @return the exit value of the command.
     *
     * @throws IOException
     *             when the command can not be completed because of an IO problem.
     * @throws InterruptedException
     *             when it is interrupted while waiting for the command to complete.
     */
    public int execute() throws IOException,
                         InterruptedException {
        try {
            return execute(null,
                           null,
                           null,
                           null,
                           null);
        } catch (TimeoutException e) {
            throw new Error("Should never have reached this section of code",
                            e);
        }
    }

    /**
     * Executes the command. If this method has already been invoked then the
     * executing will block until the command is complete or the thread is
     * interrupted.
     *
     * @param output
     *            the {@link File}, if any, into which the standard output will be
     *            written.
     *
     * @return the exit value of the command.
     *
     * @throws IOException
     *             when the command can not be completed because of an IO problem.
     * @throws InterruptedException
     *             when it is interrupted while waiting for the command to complete.
     */
    public int execute(File output) throws IOException,
                                    InterruptedException {
        try {
            return execute(output,
                           null,
                           null,
                           null,
                           null);
        } catch (TimeoutException e) {
            throw new Error("Should never have reached this section of code",
                            e);
        }
    }

    /**
     * Executes the command. If this method has already been invoked then the
     * executing will block until the command is complete or the thread is
     * interrupted.
     *
     * @param output
     *            the {@link File}, if any, into which the standard output will be
     *            written.
     * @param error
     *            the {@link File}, if any, into which the standard error will be
     *            written.
     *
     * @return the exit value of the command.
     *
     * @throws IOException
     *             when the command can not be completed because of an IO problem.
     * @throws InterruptedException
     *             when it is interrupted while waiting for the command to complete.
     */
    public int execute(File output,
                       File error) throws IOException,
                                   InterruptedException {
        try {
            return execute(output,
                           error,
                           null,
                           null,
                           null);
        } catch (TimeoutException e) {
            throw new Error("Should never have reached this section of code",
                            e);
        }
    }

    /**
     * Executes the command. If this method has already been invoked then the
     * executing will block until the command is complete or the thread is
     * interrupted.
     *
     * @param output
     *            the {@link File}, if any, into which the standard output will be
     *            written.
     * @param error
     *            the {@link File}, if any, into which the standard error will be
     *            written.
     * @param input
     *            the {@link File}, if any, from which the standard input will be
     *            read.
     *
     * @return the exit value of the command.
     *
     * @throws IOException
     *             when the command can not be completed because of an IO problem.
     * @throws InterruptedException
     *             when it is interrupted while waiting for the command to complete.
     */
    public int execute(File output,
                       File error,
                       File input) throws IOException,
                                   InterruptedException {
        try {
            return execute(output,
                           error,
                           input,
                           null,
                           null);
        } catch (TimeoutException e) {
            throw new Error("Should never have reached this section of code",
                            e);
        }
    }

    /**
     * Executes the command. If this method has already been invoked then the
     * execution will block until the command is complete or the thread is
     * interrupted.
     *
     * @param output
     *            the {@link File}, if any, into which the standard output will be
     *            written.
     * @param error
     *            the {@link File}, if any, into which the standard error will be
     *            written.
     * @param input
     *            the {@link File}, if any, from which the standard input will be
     *            read.
     * @param timeout
     *            the maximum time to wait
     * @param unit
     *            the time unit of the timeout argument
     *
     * @return the exit value of the command.
     *
     * @throws IOException
     *             when the command can not be completed because of an IO problem.
     * @throws InterruptedException
     *             when it is interrupted while waiting for the command to complete.
     * @throws TimeoutException
     *             when waiting for the process to complete times out.
     */
    public int execute(File output,
                       File error,
                       File input,
                       Long timeout,
                       TimeUnit unit) throws IOException,
                                      InterruptedException,
                                      TimeoutException {
        /**
         * 'launch' tests if the process is already running and throws an
         * IllegalStateException if it is.
         */
        try {
            launch(output,
                   error,
                   input);
        } catch (IllegalStateException e) {
            // Do nothing, carry on
        }
        if (null == unit) {
            process.waitFor();
        } else {
            if (!process.waitFor(timeout,
                                 unit)) {
                throw new TimeoutException();
            }
        }

        final int retCode = process.exitValue();
        if (0 != retCode) {
            final String[] errors;
            if (null != stderr) {
                final List<String> lines = getStdErr();
                errors = lines.toArray(new String[lines.size()]);
            } else {
                errors = null;
            }
            cmdException = new ExecutionFailedException(toString(processBuilder.command()),
                                                        retCode,
                                                        errors);
        }

        if (null == finalStdout) {
            finalStdout = getStdOut();
            if (null != stdout) {
                stdout.delete();
                stdout = null;
            }
        }
        if (null == finalStderr) {
            finalStderr = getStdErr();
            if (null != stderr) {
                stderr.delete();
                stderr = null;
            }
        }

        return retCode;
    }

    /**
     * Executes the command. If this method has already been invoked then the
     * executing will block until the command is complete or the thread is
     * interrupted.
     *
     * @param output
     *            the {@link File}, if any, into which the standard output will be
     *            written.
     * @param error
     *            the {@link File}, if any, into which the standard error will be
     *            written.
     * @param input
     *            the {@link InputStream}, if any, from which the standard input
     *            will be read.
     *
     * @return the exit value of the command.
     *
     * @throws IOException
     *             when the command can not be completed because of an IO problem.
     * @throws InterruptedException
     *             when it is interrupted while waiting for the command to complete.
     */
    public int execute(File output,
                       File error,
                       InputStream input) throws IOException,
                                          InterruptedException {
        final File stdin = createInputFile(input);
        try {
            return execute(output,
                           error,
                           stdin,
                           null,
                           null);
        } catch (TimeoutException e) {
            throw new Error("Should never have reached this section of code",
                            e);
        } finally {
            stdin.delete();
        }
    }

    /**
     * Returns a {@link ExecutionFailedException} instance if the command has
     * failed, <code>null</code> otherwise.
     *
     * @return the appropriate {@link ExecutionFailedException} instance.
     */
    public ExecutionFailedException getCmdException() {
        return cmdException;
    }

    /**
     * Returns the List of lines error so far into the command's standard error if
     * it is not being written to a file.
     *
     * @return the List of lines error so far into the command's standard error, or
     *         <code>null</code> of the command has not yet been executed or
     *         standard error have been re-directed to a file.
     *
     * @throws IOException
     *             when there is a problem reading the standard error.
     */
    public List<String> getStdErr() throws IOException {
        if (null != finalStderr) {
            return finalStderr;
        }
        if (null != stderr) {
            return Files.readAllLines(stderr.toPath());
        }
        return null;
    }

    // static member methods (alphabetic)

    /**
     * Returns the List of lines output so far into the command's standard output if
     * it is not being written to a file.
     *
     * @return the List of lines output so far into the command's standard output,
     *         or <code>null</code> of the command has not yet been executed or
     *         standard output have been re-directed to a file.
     *
     * @throws IOException
     *             when there is a problem reading the standard output.
     */
    public List<String> getStdOut() throws IOException {
        if (null != finalStdout) {
            return finalStdout;
        }
        if (null != stdout) {
            return Files.readAllLines(stdout.toPath());
        }
        return null;
    }

    /**
     * Launches the command. If this method has already been invoked then it throws
     * an {@link IllegalStateException}.
     *
     * @param output
     *            the {@link File}, if any, into which the standard output will be
     *            written.
     * @param error
     *            the {@link File}, if any, into which the standard error will be
     *            written.
     * @param input
     *            the {@link File}, if any, from which the standard input will be
     *            read.
     *
     * @throws IllegalStateException
     *             when the command has already been launched.
     * @throws IOException
     *             when the command can not be completed because of an IO problem.
     * @throws InterruptedException
     *             when it is interrupted while waiting for the command to complete.
     */
    private synchronized void launch(File output,
                                     File error,
                                     File input) throws IllegalStateException,
                                                 IOException,
                                                 InterruptedException {
        /*
         * An IOException can be generated without a process being launched, so check
         * this first.
         */
        if (null != exception) {
            throw new IllegalStateException("The process has already failed",
                                            exception);
        }

        // If the Process has already been launched do nothing more.
        if (null != process) {
            throw new IllegalStateException("The process has already been launched");
        }
        try {
            if (null == input) {
                processBuilder.redirectInput(NULL_DEVICE);
            } else {
                processBuilder.redirectInput(input);
            }
            if (null == output) {
                stdout = File.createTempFile(STDOUT_PREFIX,
                                             STDOUT_SUFFIX);
                stdout.deleteOnExit();
                processBuilder.redirectOutput(stdout);
            } else {
                processBuilder.redirectOutput(output);
            }
            if (null == error) {
                stderr = File.createTempFile(STDERR_PREFIX,
                                             STDERR_SUFFIX);
                stderr.deleteOnExit();
                processBuilder.redirectError(stderr);
            } else {
                processBuilder.redirectError(error);
            }
            process = processBuilder.start();
        } catch (IOException e) {
            exception = e;
            throw exception;
        }
    }

    // Description of this object.
    @Override
    public String toString() {
        final StringBuffer result = new StringBuffer("external command '");
        result.append(toString(processBuilder.command()));
        result.append("'");
        return result.toString();
    }

}
