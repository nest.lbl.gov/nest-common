package gov.lbl.nest.common.external.bash;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.List;

import gov.lbl.nest.common.external.ScriptExecution;
import gov.lbl.nest.common.external.ScriptExecutionFactory;

/**
 * This class implements the {@link ScriptExecutionFactory} for execution of
 * scripts in the background using a bash shell.
 *
 * @author patton
 *
 */
public class BashBackgroundFactory implements
                                   ScriptExecutionFactory {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The suffix used on the name of the resource to locate the callback script.
     */
    private static final String CALLBACK_RESOURCE_SUFFIX = ".callback";

    /**
     * The suffix used on the name of the resource to locate the runner script.
     */
    private static final String RUNNER_RESOURCE_SUFFIX = ".runner";

    // private static member data

    // private instance member data

    /**
     * The sequence of lines that make up the callback script.
     */
    private final List<String> callbackScript;

    /**
     * The sequence of lines that make up the template for the runner script.
     */
    private final List<String> runnerTemplate;

    // constructors

    /**
     * Creates an instance of this class
     *
     * @param resource
     *            the name of the resource containing the "runner" script to used
     *            with this factory.
     *
     * @throws IOException
     *             if runner or callback resources can not be read.
     */
    public BashBackgroundFactory(String resource) throws IOException {
        callbackScript = ScriptExecution.readResource(resource + CALLBACK_RESOURCE_SUFFIX,
                                                      getClass());
        runnerTemplate = ScriptExecution.readResource(resource + RUNNER_RESOURCE_SUFFIX,
                                                      getClass());
    }

    // instance member method (alphabetic)

    @Override
    public ScriptExecution createScriptExecution(File script,
                                                 URI reference) throws IOException {
        return new BashBackgroundExecution(script,
                                           reference,
                                           Collections.unmodifiableList(runnerTemplate),
                                           Collections.unmodifiableList(callbackScript));
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
