package gov.lbl.nest.common.external;

import java.io.File;
import java.net.URI;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The class captures the request for execution of an external Script
 * execution..
 *
 * @author patton
 */
@XmlRootElement(name = "script_response")
@XmlType(propOrder = { "reference",
                       "script",
                       "input" })
public class ScriptRequest {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The input data for the Script.
     */
    private ScriptData input = new ScriptData();

    /**
     * The URI used as the callback reference.
     */
    private URI reference;

    /**
     * The script to execute in order to do the operation.
     */
    private File script;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ScriptRequest() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param reference
     *            the URI used as the callback reference.
     * @param script
     *            the script to execute in order to do the operation.
     * @param input
     *            the input data for the Script.
     */
    public ScriptRequest(URI reference,
                         File script,
                         ScriptData input) {
        this.input = input;
        this.reference = reference;
        this.script = script;
    }

    // instance member method (alphabetic)

    /**
     * Returns the input data from the Script.
     *
     * @return the input data from the Script.
     */
    @XmlElement
    public ScriptData getInput() {
        return input;
    }

    /**
     * Returns the URI used as the callback reference.
     *
     * @return the URI used as the callback reference.
     */
    @XmlElement
    public URI getReference() {
        return reference;
    }

    /**
     * Returns the script to execute in order to do the operation.
     *
     * @return the script to execute in order to do the operation.
     */
    @XmlElement
    public File getScript() {
        return script;
    }

    /**
     * Sets the input data from the Script.
     *
     * @param input
     *            the input data from the Script.
     */
    protected void setInput(ScriptData input) {
        this.input = input;
    }

    /**
     * Sets the URI used as the callback reference.
     *
     * @param reference
     *            the URI used as the callback reference.
     */
    protected void setReference(URI reference) {
        this.reference = reference;
    }

    /**
     * Sets the script to execute in order to do the operation.
     *
     * @param script
     *            the script to execute in order to do the operation.
     */
    public void setScript(File script) {
        this.script = script;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
