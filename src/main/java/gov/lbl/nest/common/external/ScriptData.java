package gov.lbl.nest.common.external;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The class captures the environment in which a script should execute.
 *
 * @author patton
 */
@XmlRootElement(name = "script_data")
@XmlType(propOrder = { "env",
                       "lines" })
public class ScriptData {

    /**
     * This class captures the setting of an environmental variable.
     *
     * @author patton
     */
    @XmlType(propOrder = { "name",
                           "value" })
    public static class EnvVar {

        /**
         * The name of the datum.
         */
        private String name;

        /**
         * The value of the datum.
         */
        private String value;

        /**
         * Creates an instance of this class.
         */
        protected EnvVar() {
        }

        /**
         * Creates an instance of this class.
         *
         * @param name
         *            the name of the datum.
         * @param value
         *            the value of the datum.
         */
        public EnvVar(String name,
                      String value) {
            this.name = name;
            this.value = value;
        }

        /**
         * Returns the name of the datum.
         *
         * @return the name of the datum.
         */
        @XmlElement
        public String getName() {
            return name;
        }

        /**
         * Returns the value of the datum.
         *
         * @return the value of the datum.
         */
        @XmlElement
        public String getValue() {
            return value;
        }

        /**
         * sets the name of the datum.
         *
         * @param name
         *            the name of the datum.
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * Sets the value of the datum.
         *
         * @param value
         *            the value of the datum.
         */
        public void setValue(String value) {
            this.value = value;
        }
    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The collection of data that make up the environment.
     */
    private Collection<EnvVar> environment = new ArrayList<>();

    /**
     * The collection of lines that are to be passed into or have been written out
     * from the script.
     */
    private List<String> lines = new ArrayList<>();

    // constructors

    /**
     * Creates an instance of this class.
     */
    public ScriptData() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param environment
     *            the array of variables that make up the environment.
     */
    public ScriptData(EnvVar[] environment) {
        for (EnvVar envVar : environment) {
            this.environment.add(envVar);
        }
    }

    /**
     * Creates an instance of this class.
     *
     * @param environment
     *            the collection of variables that make up the environment.
     */
    public ScriptData(List<EnvVar> environment) {
        setEnv(environment);
    }

    /**
     * Creates an instance of this class.
     *
     * @param rhs
     *            the {@link ScriptData} instance to copy.
     */
    public ScriptData(ScriptData rhs) {
        environment.addAll(rhs.environment);
        lines.addAll(rhs.lines);
    }

    // instance member method (alphabetic)

    /**
     * Returns the collection of variables that make up the environment.
     *
     * @return the collection of variables that make up the environment.
     */
    @XmlElement
    public Collection<EnvVar> getEnv() {
        return environment;
    }

    /**
     * Returns the value of the specified environmental variable, or null if it does
     * not exist.
     *
     * @param name
     *            the name of the environmental variable whose value should be
     *            returned.
     *
     * @return the value of the specified environmental variable, or null if it does
     *         not exist.
     */
    public String getEnvVar(String name) {
        for (EnvVar datum : environment) {
            if (name.equals(datum.getName())) {
                return datum.getValue();
            }
        }
        return null;
    }

    /**
     * Returns the collection of lines that are to be passed into or have been
     * written out from the script.
     *
     * @return the collection of lines that are to be passed into or have been
     *         written out from the script.
     */
    @XmlElement(name = "line")
    public List<String> getLines() {
        return lines;
    }

    /**
     * Removes the specified the specified environmental variable if it exists,
     * otherwise does nothing.
     *
     * @param name
     *            the name of the environmental variable to remove.
     */
    public void removeEnvVar(String name) {
        final Iterator<EnvVar> iterator = environment.iterator();
        while (iterator.hasNext()) {
            if (name.equals((iterator.next()).getName())) {
                iterator.remove();
                return;
            }
        }
    }

    /**
     * Sets the collection of variables that make up the environment.
     *
     * @param environment
     *            the collection of variables that make up the environment.
     */
    public void setEnv(Collection<EnvVar> environment) {
        this.environment = environment;
    }

    /**
     * Sets the collection of lines that are to be passed into or have been written
     * out from the script.
     *
     * @param lines
     *            the collection of lines that are to be passed into or have been
     *            written out from the script.
     */
    public void setLines(List<String> lines) {
        this.lines = lines;
    }

    /**
     * Updates the specified environmental variable, adding it if it does not
     * already exist.
     *
     * @param name
     *            the name of the environmental variable to update.
     * @param value
     *            the new value of the environmental variable.
     */
    public void updateEnvVar(String name,
                             String value) {
        for (EnvVar datum : environment) {
            if (name.equals(datum.getName())) {
                datum.setValue(value);
                return;
            }
        }
        environment.add(new EnvVar(name,
                                   value));
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
