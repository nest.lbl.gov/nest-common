package gov.lbl.nest.common.external;

import java.util.Arrays;

/**
 * This exception is thrown when an external command fails to successfully
 * execute.
 *
 * @author patton
 */
public class ExecutionFailedException extends
                                      Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private instance member data

    /**
     * The String containing the command to call and its arguments.
     */
    private final String cmdString;

    /**
     * The returns code of the command.
     */
    private final int retCode;

    /**
     * The contents of the command's stderr.
     */
    private final String[] stderr;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param cmdString
     *            the String containing the command to call and its arguments.
     * @param retCode
     *            the returns code of the command.
     * @param stdErr
     *            the contents of the command's stderr.
     */
    public ExecutionFailedException(final String cmdString,
                                    final int retCode,
                                    final String[] stdErr) {
        this.cmdString = cmdString;
        if (null == stdErr) {
            this.stderr = null;
        } else {
            this.stderr = Arrays.copyOf(stdErr,
                                        stdErr.length);
        }
        this.retCode = retCode;
    }

    // instance member method (alphabetic)

    /**
     * Returns the String containing the command to call and its arguments.
     *
     * @return the String containing the command to call and its arguments.
     */
    public String getCmdString() {
        return cmdString;
    }

    /**
     * Returns the returns code of the command.
     *
     * @return the returns code of the command.
     */
    public int getRetCode() {
        return retCode;
    }

    /**
     * Returns the contents of the command's stderr.
     *
     * @return the contents of the command's stderr.
     */
    public String[] getStdErr() {
        return stderr;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
