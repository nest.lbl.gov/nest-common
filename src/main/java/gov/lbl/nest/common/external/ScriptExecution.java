package gov.lbl.nest.common.external;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.external.ScriptData.EnvVar;

/**
 * This class handled the synchronous execution of a script within a Java
 * application.
 *
 * @author patton
 */
public class ScriptExecution {

    /**
     * This class caputures the traits of the different type of script
     * envinronments.
     *
     * @author patton
     *
     */
    protected static class Trait {

        /**
         * The MIME format used for this type of script.
         */
        public String format;

        /**
         * The conventional suffix for this type of script.
         */
        public String preamble;

        /**
         * The first line of a script of this type.
         */
        public String suffix;

        /**
         * Create an instance of this class
         *
         * @param format
         *            the MIME format used for this type of script.
         * @param suffix
         *            the conventional suffix for this type of script.
         * @param preamble
         *            the first line of a script of this type.
         */
        public Trait(String format,
                     String suffix,
                     String preamble) {
            this.format = format;
            this.preamble = preamble;
            this.suffix = suffix;
        }
    }

    // public static final member data

    /**
     * The MIME format used for bash scripts.
     */
    public static final String APPLICATION_X_BSH = "application/x-bsh";

    /**
     * The MIME format used for csh scripts.
     */
    public static final String APPLICATION_X_CSH = "application/x-csh";

    /**
     * The MIME format used for python scripts.
     */
    public static final String APPLICATION_X_PYTHON = "application/x-python";

    /**
     * The MIME format used for shell scripts.
     */
    public static final String APPLICATION_X_SH = "application/x-sh";

    // protected static final member data

    /**
     * The array to use when no file attributes are being specified.
     */
    protected static final FileAttribute<?>[] NO_FILE_ATTRIBUTES = new FileAttribute<?>[] {};

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ScriptExecution.class);

    /**
     * The output key to see if the work area should be preserved.
     */
    private static final String NEST_PRESERVE_WORK = "NEST_PRESERVE_WORK";

    /**
     * The directory in the jar files where configuration files exist.
     */
    private static final String CONFIG_DIR = "/META-INF/config";

    /**
     * The directory name that contains the files used to run the script.
     */
    private static final String FILES_DIR = "files";

    /**
     * The name of the file, without suffix, of the file contains the runner that
     * will execute the script.
     */
    private static final String RUNNER_NAME = "runner";

    /**
     * The directory name used as the working directory for execution of the script.
     */
    private static final String WORK_DIR = "work";

    /**
     * The name of the file, without suffix, of the file contains the script to be
     * executed.
     */
    private static final String SCRIPT_NAME = "script";

    /**
     * The name of the file containing the environment in which to execute the
     * script.
     */
    private static final String INITIAL_ENV_FILENAME = "initial.env";

    /**
     * The name of the file containing the stdin for the script.
     */
    private static final String INPUTS_FILENAME = "inputs.txt";

    /**
     * The name of the file into which to write the stdout for the script.
     */
    private static final String OUTPUTS_FILENAME = "outputs.txt";

    /**
     * The name of the file into which to write the stderr for the script
     */
    private static final String ERROR_FILENAME = "error.txt";

    /**
     * The name of the file into which to write the environment resulting from
     * running the script.
     */
    private static final String RESULTANT_ENV_FILENAME = "resultant.env";

    /**
     * The default name of the resource containing the runner script to use to
     * execute the script.
     */
    private static final String RUNNER_RESOURCE = "bash.blocking.runner";

    /**
     * The prefixed used to generate the temporary area used by the script.
     */
    private static final String DEFAULT_TMP_PREFIX = "nst_";

    /**
     * The Object used to indicate that there is no specified base directory for an
     * instance of this class.
     */
    private static final Path NO_BASE_DIR = Paths.get("/dev/null");

    /**
     * The name used to look up the configuration of this class.
     */
    private static final String SPI_NAME = ScriptExecution.class.getCanonicalName();

    /**
     * The file name used to capture the standard error of the script.
     */
    private static final String STDERR_FILENAME = "stderr";

    /**
     * The file name used to capture the standard output of the script.
     */
    private static final String STDOUT_FILENAME = "stdout";

    /**
     * The collection of known {@link Trait} instances.
     */
    private static final List<Trait> TRAITS = Arrays.asList(new Trait[] { new Trait(APPLICATION_X_SH,
                                                                                    ".sh",
                                                                                    "#!/bin/sh"),
                                                                          new Trait(APPLICATION_X_BSH,
                                                                                    ".sh",
                                                                                    "#!/bin/bash"),
                                                                          new Trait(APPLICATION_X_CSH,
                                                                                    ".csh",
                                                                                    "#!/bin/csh"),
                                                                          new Trait(APPLICATION_X_PYTHON,
                                                                                    ".py",
                                                                                    "#!/usr/bin/env python") });

    // private static member data

    // private instance member data

    /**
     * Creates the String[] that will be used to execute the command.
     *
     * @param filesDir
     *            the directory that contains the files used to run the script.
     * @param runnerName
     *            the file, in the current directory, containing the runner that
     *            will invoke the script.
     * @param scriptName
     *            the file, in the current directory, containing the script to
     *            invoke.
     * @param workDir
     *            the directory to use as the working directory for the script.
     * @param initialEnvName
     *            the file, in the current directory, containing the environment in
     *            which to execute the script.
     * @param inputsName
     *            the file, in the current directory, containing the stdin for the
     *            script.
     * @param outputsName
     *            the file, in the current directory, into which to write the stdout
     *            for the script.
     * @param errorName
     *            the file, in the current directory, into which to write the stderr
     *            for the script.
     * @param resultantEnvName
     *            the file, in the current directory, into which to write the
     *            environment resulting from running the script.
     * @param reference
     *            the URI, if any, used as the callback reference. Otherwise returns
     *            <code>null</code>.
     *
     * @return the String[] that will be used to execute the command.
     */
    protected static String[] createCmdArray(final Path filesDir,
                                             final String runnerName,
                                             final String scriptName,
                                             final Path workDir,
                                             final String initialEnvName,
                                             final String inputsName,
                                             final String outputsName,
                                             final String errorName,
                                             final String resultantEnvName,
                                             final URI reference) {
        final String optionalReference;
        if (null == reference) {
            optionalReference = "";
        } else {
            optionalReference = " " + reference.toString();
        }
        final String[] cmdArray = new String[] { "bash",
                                                 "-c",
                                                 "cd " + filesDir.toAbsolutePath()
                                                       + "; ./"
                                                       + runnerName
                                                       + " "
                                                       + scriptName
                                                       + " "
                                                       + workDir.toAbsolutePath()
                                                       + " "
                                                       + initialEnvName
                                                       + " "
                                                       + inputsName
                                                       + " "
                                                       + outputsName
                                                       + " "
                                                       + errorName
                                                       + " "
                                                       + resultantEnvName
                                                       + optionalReference };
        return cmdArray;
    }

    /**
     * Executes an instance of this object using the command line input are the
     * command to execute.
     *
     * @param args
     *            the arguments from the command line.
     *
     * @throws IOException
     *             when the command can not be completed because of an IO problem.
     * @throws InterruptedException
     *             when it is interrupted while waiting for the command to complete.
     *
     */
    public static void main(String args[]) throws IOException,
                                           InterruptedException {
        int begin = 0;
        final boolean preserve;
        if (args[begin].startsWith("-p")) {
            begin += 1;
            preserve = true;
        } else {
            preserve = false;
        }

        String[] arguments = new String[args.length - begin];
        System.arraycopy(args,
                         begin,
                         arguments,
                         0,
                         arguments.length);
        if (1 != arguments.length) {
            throw new IllegalArgumentException("Must specify one, and only one script file");
        }
        final List<String> lines = new ArrayList<>();
        String format = null;
        try (FileInputStream is = new FileInputStream(arguments[0])) {
            try (InputStreamReader reader = new InputStreamReader(is)) {
                final BufferedReader br = new BufferedReader(reader);
                String line = br.readLine();
                format = selectFormat(line);
                while (null != line) {
                    lines.add(line);
                    line = br.readLine();
                }
            }
        }

        final ScriptExecution scriptExecution = new ScriptExecution(format,
                                                                    lines);
        try {
            scriptExecution.execute(null,
                                    null,
                                    preserve);
        } catch (ExecutionFailedException e) {
            LOG.error("Execution failed");
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Converts the contents of the resource file into a List of Strings
     *
     * @param name
     *            the name of the resource to read.
     * @param clazz
     *            class shows name is associated with the resource's location.
     *
     * @return the List of String, on per line, contained in the resource.
     *
     * @throws IOException
     *             when the resource can not be read.
     */
    public static List<String> readResource(String name,
                                            Class<?> clazz) throws IOException {
        final List<String> results = new ArrayList<>();
        try (InputStream is = clazz.getResourceAsStream(name)) {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
                String line = br.readLine();
                while (null != line) {
                    final String trimmed = line.trim();
                    if (!"".equals(trimmed)) {
                        results.add(trimmed);
                    }
                    line = br.readLine();
                }
            }
        }
        return results;
    }

    // constructors

    /**
     * Use {@link Files} class to recursively remove directory tree. See
     * http://stackoverflow.com/questions/779519/delete-directories-recursively-
     * in-java
     *
     * @param path
     *            the path to the top directory.
     *
     * @throws IOException
     *             when there is a problem deleting the entire tree.
     */
    public static void removeRecursive(Path path) throws IOException {
        Files.walkFileTree(path,
                           new FileVisitor<Path>() {

                               @Override
                               public FileVisitResult postVisitDirectory(Path dir,
                                                                         IOException exc) throws IOException {
                                   if (exc == null) {
                                       Files.delete(dir);
                                       return FileVisitResult.CONTINUE;
                                   }
                                   /*
                                    * directory iteration failed; propagate exception
                                    */
                                   throw exc;
                               }

                               @Override
                               public FileVisitResult preVisitDirectory(Path dir,
                                                                        BasicFileAttributes attrs) {
                                   return FileVisitResult.CONTINUE;
                               }

                               @Override
                               public FileVisitResult visitFile(Path file,
                                                                BasicFileAttributes attrs) throws IOException {
                                   Files.delete(file);
                                   return FileVisitResult.CONTINUE;
                               }

                               @Override
                               public FileVisitResult visitFileFailed(Path file,
                                                                      IOException exc) throws IOException {
                                   /*
                                    * try to delete the file anyway, even if its attributes could not be read,
                                    * since delete-only access is theoretically possible.
                                    */
                                   Files.delete(file);
                                   return FileVisitResult.CONTINUE;
                               }
                           });
    }

    private static String selectFormat(String line) {
        if (null == line) {
            return (TRAITS.get(0)).format;
        }
        for (Trait trait : TRAITS) {
            if (line.startsWith(trait.preamble)) {
                return trait.format;
            }
        }
        return (TRAITS.get(0)).format;
    }

    // instance member method (alphabetic)

    private static File writeEnvFile(Path directory,
                                     Collection<EnvVar> environment) throws IOException {
        FileWriter envWriter = null;
        try {
            final File envFile = (Files.createFile(directory.resolve(INITIAL_ENV_FILENAME),
                                                   NO_FILE_ATTRIBUTES)).toFile();
            envWriter = new FileWriter(envFile);
            for (EnvVar envVar : environment) {
                envWriter.write(envVar.getName() + "=\""
                                + envVar.getValue()
                                + "\"\n");
            }
            envWriter.close();
            envWriter = null;
            return envFile;
        } finally {
            if (null != envWriter) {
                try {
                    envWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static File writeInputsFile(Path directory,
                                        List<String> inputs) throws IOException {
        FileWriter inputsWriter = null;
        try {
            final File inputsFile = (Files.createFile(directory.resolve(INPUTS_FILENAME),
                                                      NO_FILE_ATTRIBUTES)).toFile();
            inputsWriter = new FileWriter(inputsFile);
            for (String line : inputs) {
                inputsWriter.write(line + "\n");
            }
            inputsWriter.close();
            inputsWriter = null;
            return inputsFile;
        } finally {
            if (null != inputsWriter) {
                try {
                    inputsWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * The path of the directory in which to create work areas for execution of this
     * script.
     */
    private Path basePath;

    /**
     * The script to be executed.
     */
    private final List<String> lines;

    /**
     * The script trait used by this object.
     */
    private final Trait trait;

    /**
     * Creates a instance of this class.
     *
     * @param format
     *            the format of the script, in mime-type format.
     * @param script
     *            the file that contain the script
     *
     * @throws IOException
     *             when the script file can not be read.
     */
    public ScriptExecution(String format,
                           File script) throws IOException {
        this(format,
             Files.readAllLines(script.toPath()));
    }

    /**
     * Creates a instance of this class.
     *
     * @param format
     *            the format of the script, in mime-type format.
     * @param lines
     *            the lines that make up the script.
     */
    public ScriptExecution(String format,
                           List<String> lines) {

        final Iterator<Trait> iterator = TRAITS.iterator();
        Trait traitToUse = null;
        while (null == traitToUse && iterator.hasNext()) {
            final Trait traitToTest = iterator.next();
            if (traitToTest.format.equals(format)) {
                traitToUse = traitToTest;
            }
        }
        if (null == traitToUse) {
            throw new IllegalArgumentException("This class can not execute \"" + format
                                               + "\" scripts");
        }
        trait = traitToUse;
        this.lines = lines;
    }

    // static member methods (alphabetic)

    /**
     * Executes the script using the supplied inputs and filling in the outputs.
     *
     * @param inputData
     *            the {@link ScriptData} instance defining the execution environment
     *            in which to start the script.
     * @param outputData
     *            the {@link ScriptData} instance defining the execution environment
     *            in after to the script has executed.
     *
     * @throws ExecutionFailedException
     *             when the script can not be successfully executed.
     */
    public void execute(ScriptData inputData,
                        ScriptData outputData) throws ExecutionFailedException {
        if (null == lines || lines.isEmpty()) {
            return;
        }

        boolean preserve = (null != inputData.getEnvVar(NEST_PRESERVE_WORK));
        Path rootDir = null;
        File stdoutFile = null;
        File stderrFile = null;
        try {
            final Path basePathToUse = getBasePath();
            if (NO_BASE_DIR == basePathToUse) {
                rootDir = Files.createTempDirectory(DEFAULT_TMP_PREFIX,
                                                    NO_FILE_ATTRIBUTES);
            } else {
                rootDir = Files.createTempDirectory(basePathToUse,
                                                    DEFAULT_TMP_PREFIX,
                                                    NO_FILE_ATTRIBUTES);
            }
            final Path filesDir = Files.createDirectory(rootDir.resolve(FILES_DIR),
                                                        NO_FILE_ATTRIBUTES);
            final File runnerFile = writeScriptFile(filesDir,
                                                    RUNNER_NAME,
                                                    getRunnerLines(filesDir));
            final File scriptFile = writeScriptFile(filesDir,
                                                    SCRIPT_NAME,
                                                    lines);
            final Path workDir = (Files.createDirectory(rootDir.resolve(WORK_DIR),
                                                        NO_FILE_ATTRIBUTES));
            writeEnvFile(filesDir,
                         inputData.getEnv());
            writeInputsFile(filesDir,
                            inputData.getLines());
            final URI reference = getReference();
            final String[] cmdArray = createCmdArray(filesDir,
                                                     runnerFile.getName(),
                                                     scriptFile.getName(),
                                                     workDir,
                                                     INITIAL_ENV_FILENAME,
                                                     INPUTS_FILENAME,
                                                     OUTPUTS_FILENAME,
                                                     ERROR_FILENAME,
                                                     RESULTANT_ENV_FILENAME,
                                                     reference);
            final Command command = new Command(cmdArray);
            stdoutFile = (Files.createFile(filesDir.resolve(STDOUT_FILENAME),
                                           NO_FILE_ATTRIBUTES)).toFile();
            stderrFile = (Files.createFile(filesDir.resolve(STDERR_FILENAME),
                                           NO_FILE_ATTRIBUTES)).toFile();
            command.execute(stdoutFile,
                            stderrFile);
            final ExecutionFailedException cmdException = command.getCmdException();
            if (null != cmdException) {
                throw cmdException;
            }
            if (null != outputData) {
                final File resultantEnvFile = (filesDir.resolve(RESULTANT_ENV_FILENAME)).toFile();
                try (BufferedReader br = new BufferedReader(new FileReader(resultantEnvFile))) {
                    String line = br.readLine();
                    while (null != line) {
                        final String[] parts = line.split("=",
                                                          2);
                        if (2 == parts.length) {
                            outputData.updateEnvVar(parts[0],
                                                    parts[1]);
                        }
                        line = br.readLine();
                    }
                    br.close();
                }
                // TODO: Read back stdout
                preserve = (null != outputData.getEnvVar(NEST_PRESERVE_WORK));
            }
        } catch (ExecutionFailedException e) {
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != rootDir) {
                if (preserve) {
                    LOG.info("Preserved work area \"" + rootDir
                             + "\"");
                } else {
                    try {
                        removeRecursive(rootDir);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Executes an instance of this {@link Class}.
     *
     * @param inputData
     *            the {@link ScriptData} instance defining the execution environment
     *            in which to start the script.
     * @param outputData
     *            the {@link ScriptData} instance defining the execution environment
     *            in after to the script has executed.
     * @param preserve
     *            true if the work space in which the script executed shouldbe
     *            preserved.
     *
     * @throws ExecutionFailedException
     *             when the script can not be successfully executed.
     */
    public void execute(final ScriptData inputData,
                        final ScriptData outputData,
                        final boolean preserve) throws ExecutionFailedException {
        final ScriptData inputDataToUse;
        if (null == inputData) {
            inputDataToUse = new ScriptData();
        } else {
            inputDataToUse = inputData;
        }
        inputData.updateEnvVar(NEST_PRESERVE_WORK,
                               (Boolean.TRUE).toString());
        execute(inputDataToUse,
                outputData);
        if (null != outputData) {
            for (EnvVar variable : outputData.getEnv()) {
                LOG.info("  " + variable.getName()
                         + "="
                         + variable.getValue());
            }
        }
    }

    /**
     * Returns the directory in which to create work areas for execution of this
     * script.
     *
     * @return the directory in which to create work areas for execution of this
     *         script.
     */
    public Path getBasePath() {
        if (null == basePath) {
            try {
                final InputStream is = getClass().getResourceAsStream(CONFIG_DIR + "/"
                                                                      + SPI_NAME);
                if (null == is) {
                    basePath = NO_BASE_DIR;
                } else {
                    Properties props = new Properties();
                    props.load(is);
                    final String path = props.getProperty("base_dir");
                    if (null == path) {
                        basePath = NO_BASE_DIR;
                    } else {
                        basePath = Paths.get(path);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                basePath = NO_BASE_DIR;
            }
        }
        return basePath;
    }

    /**
     * Returns the URI, if any, used as the callback reference. Otherwise returns
     * <code>null</code>.
     *
     * @return the URI, if any, used as the callback reference. Otherwise returns
     *         <code>null</code>.
     */
    protected URI getReference() {
        return null;
    }

    /**
     * Returns the {@link List} of String that make up the runner script. This
     * method can be overwritten to create other files if necessary, so the
     * directory where the script files are being written is supplied.
     *
     * @param directory
     *            the directory where the script files are being written is
     *            supplied.
     *
     * @return tthe {@link List} of String that make up the runner script.
     */
    protected List<String> getRunnerLines(Path directory) {
        try {
            return readResource(RUNNER_RESOURCE,
                                getClass());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the script trait used by this object.
     *
     * @return the script trait used by this object.
     */
    protected Trait getTrait() {
        return trait;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    /**
     * Writes the supplied contents to a script file in the specified directory.
     *
     * @param directory
     *            the directory into which to write the script file.
     * @param name
     *            the name of the script file.
     * @param contents
     *            the content of the script file.
     *
     * @return the {@link File} instance of the script file.
     *
     * @throws IOException
     *             when the script file can not be written.
     */
    protected File writeScriptFile(Path directory,
                                   String name,
                                   List<String> contents) throws IOException {
        FileWriter writer = null;
        try {
            final File scriptFile = (Files.createFile(directory.resolve(name + trait.suffix),
                                                      NO_FILE_ATTRIBUTES)).toFile();
            writer = new FileWriter(scriptFile);
            final String firstLine = (contents.get(0)).trim();
            if (!firstLine.startsWith("#!")) {
                writer.write(trait.preamble + "\n");
            }
            for (String line : contents) {
                writer.write(line + "\n");
            }
            writer.close();
            writer = null;
            scriptFile.setExecutable(true);
            return scriptFile;
        } finally {
            if (null != writer) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
