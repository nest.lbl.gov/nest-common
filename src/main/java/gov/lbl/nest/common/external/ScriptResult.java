package gov.lbl.nest.common.external;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The class captures the result of an external Script execution.
 *
 * @author patton
 */
@XmlRootElement(name = "result")
@XmlType(propOrder = { "returnCode",
                       "cause",
                       "output" })
public class ScriptResult {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The cause, if any, of the scripts failure.
     */
    private String cause;

    /**
     * The output data from the Script.
     */
    private ScriptData output = new ScriptData();

    /**
     * The return code from the script.
     */
    private Integer returnCode;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ScriptResult() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param returnCode
     *            the return code from the script.
     * @param data
     *            the output data from the Script.
     */
    public ScriptResult(Integer returnCode,
                        ScriptData data) {
        this.output = data;
        this.returnCode = returnCode;
    }

    // instance member method (alphabetic)

    /**
     * Returns the cause, if any, of the scripts failure.
     *
     * @return the cause, if any, of the scripts failure.
     */
    @XmlElement
    public String getCause() {
        return cause;
    }

    /**
     * Returns the output data from the Script.
     *
     * @return the output data from the Script.
     */
    @XmlElement
    public ScriptData getOutput() {
        return output;
    }

    /**
     * Returns the return code from the script.
     *
     * @return the return code from the script.
     */
    @XmlElement(name = "return_code")
    public Integer getReturnCode() {
        return returnCode;
    }

    /**
     * Sets the cause, if any, of the scripts failure.
     *
     * @param cause
     *            the cause, if any, of the scripts failure.
     */
    protected void setCause(String cause) {
        this.cause = cause;
    }

    /**
     * Sets the output data from the Script.
     *
     * @param output
     *            the output data from the Script.
     */
    protected void setOutput(ScriptData output) {
        this.output = output;
    }

    /**
     * Sets the return code from the script.
     *
     * @param code
     *            the return code from the script.
     */
    protected void setReturnCode(Integer code) {
        this.returnCode = code;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
