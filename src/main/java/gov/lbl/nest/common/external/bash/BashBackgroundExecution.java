package gov.lbl.nest.common.external.bash;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.common.external.ScriptExecution;

/**
 * This class handles the asynchronous execution of a script as a background
 * Bash process.
 *
 * @author patton
 */
public class BashBackgroundExecution extends
                                     ScriptExecution {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The directory to use as the home directory for this tools.
     */
    private static final File BACKGROUND_HOME = new File(new File(System.getProperty("user.home")),
                                                         ".nest.bash.background");

    /**
     * The name of the file, without suffix, of the file contains the runner that
     * will execute the script.
     */
    private static final String CALLBACK_NAME = "callback";

    /**
     * The start of the line in the BACKGROUND_RUNNER_RESOURCE that assigns the path
     * to the external area to a local variable.
     */
    private static final String NONTEMP_ASSIGNMENT = "nontempDir=";

    /**
     * The start of the line in the BACKGROUND_RUNNER_RESOURCE that assigns the name
     * of the callback script to a local varaible.
     */
    private static final String CALLBACK_ASSIGNMENT = "callback=";

    // private static member data

    /**
     * Executes an instance of this object using the command line input are the
     * command to execute.
     *
     * @param args
     *            the arguments from the command line.
     *
     * @throws IOException
     *             when the command can not be completed because of an IO problem.
     * @throws InterruptedException
     *             when it is interrupted while waiting for the command to complete.
     *
     */
    public static void main(String args[]) throws IOException,
                                           InterruptedException {
        int begin = 0;
        final boolean preserve;
        if (args[begin].startsWith("-p")) {
            begin += 1;
            preserve = true;
        } else {
            preserve = false;
        }

        String[] arguments = new String[args.length - begin];
        System.arraycopy(args,
                         begin,
                         arguments,
                         0,
                         arguments.length);
        if (1 != arguments.length) {
            throw new IllegalArgumentException("Must specify one, and only one script file");
        }
        final File script = new File(arguments[0]);
        ScriptExecution scriptExecution;
        try {
            scriptExecution = new BashBackgroundExecution(script,
                                                          new URI("bash/background/test"),
                                                          ScriptExecution.readResource("bash.background.runner",
                                                                                       BashBackgroundExecution.class),
                                                          ScriptExecution.readResource("bash.background.callback",
                                                                                       BashBackgroundExecution.class));
            scriptExecution.execute(null,
                                    null,
                                    preserve);
        } catch (URISyntaxException
                 | ExecutionFailedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * The sequence of lines that make up the callback script.
     */
    private final List<String> callbackScript;

    /**
     * The URI, if any, used as the callback reference.
     */
    private final URI reference;

    // private instance member data

    // constructors

    /**
     * The sequence of lines that make up the template for the runner script.
     */
    private final List<String> runnerTemplate;

    // instance member method (alphabetic)

    /**
     * Creates a instance of this class.
     *
     * @param script
     *            the file containing the script to be executed in background.
     * @param reference
     *            the URI used as the callback reference.
     * @param runnerTemplate
     *            the sequence of lines that make up the template for the runner
     *            script.
     * @param callbackScript
     *            the sequence of lines that make up the callback script.
     *
     * @throws IOException
     *             when the script file can not be read.
     */
    public BashBackgroundExecution(File script,
                                   URI reference,
                                   List<String> runnerTemplate,
                                   List<String> callbackScript) throws IOException {
        super(ScriptExecution.APPLICATION_X_BSH,
              script);
        this.callbackScript = callbackScript;
        this.reference = reference;
        this.runnerTemplate = runnerTemplate;
    }

    @Override
    protected URI getReference() {
        return reference;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    @Override
    protected List<String> getRunnerLines(Path directory) {
        try {
            writeScriptFile(directory,
                            CALLBACK_NAME,
                            callbackScript);
            final String nontempDir = System.getProperty(BashBackgroundExecution.class.getCanonicalName() + ".nontemp",
                                                         BACKGROUND_HOME.getAbsolutePath());
            final File nontempPath = new File(nontempDir);
            final String nontempToUse = nontempPath.getAbsolutePath();
            final List<String> result = new ArrayList<>();
            for (String line : runnerTemplate) {
                if (line.startsWith(NONTEMP_ASSIGNMENT)) {
                    result.add(NONTEMP_ASSIGNMENT + nontempToUse);
                } else if (line.startsWith(CALLBACK_ASSIGNMENT)) {
                    result.add((CALLBACK_ASSIGNMENT + CALLBACK_NAME) + getTrait().suffix);
                } else {
                    result.add(line);
                }
            }
            return result;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
