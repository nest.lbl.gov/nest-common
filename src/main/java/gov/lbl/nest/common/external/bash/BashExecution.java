package gov.lbl.nest.common.external.bash;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.common.external.ScriptExecution;

/**
 * This class handles the asynchronous execution of a script as a background
 * Bash process.
 *
 * @author patton
 */
public class BashExecution extends
                           ScriptExecution {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The directory to use as the home directory for this tools.
     */
    private static final File BACKGROUND_HOME = new File(new File(System.getProperty("user.home")),
                                                         ".nest_external");

    /**
     * The name of the file that will contain the default "runner" script.
     */
    private static final String RUNNER_FILE_NAME = "background.runner";

    /**
     * The name of the resource contains the "runner" script.
     */
    private static final String RUNNER_RESOURCE_NAME = RUNNER_FILE_NAME;

    /**
     * The path to the work area for the background execution.
     */
    private static final String WORK_AREA_NAME = "background";

    // private static member data

    /**
     * This is used once per application to write the default "runner" script.
     */
    private static String runnerFilepath;

    /**
     * This is used once per application to contains the base work area.
     */
    private static String workAreaPath;

    // private instance member data

    // constructors

    private static void copyInputStreamToFile(Class<?> clazz,
                                              String resourceName,
                                              File file) {
        final InputStream resource = clazz.getResourceAsStream(resourceName);
        if (null == resource) {
            throw new RuntimeException(resourceName + " resource can not be found");
        }
        final InputStreamReader reader = new InputStreamReader(resource);
        try (FileWriter scriptWriter = new FileWriter(file)) {
            try (final BufferedReader br = new BufferedReader(reader)) {
                String line = br.readLine();
                while (null != line) {
                    scriptWriter.write(line + "\n");
                    line = br.readLine();
                }
                scriptWriter.close();
                file.setExecutable(true);
            }
        } catch (IOException e) {
            throw new RuntimeException(resourceName + " resource can not be copied to \""
                                       + file.getAbsolutePath()
                                       + "\"");
        }
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Returns the lines used to launch the "runner" script in background.
     */
    private static List<String> getBashBackgroundLauncher(File script,
                                                          URI reference) {
        if (null == script) {
            throw new NullPointerException("No script was specified");
        }
        if (null == reference) {
            throw new NullPointerException("No callback URI was specfied");
        }
        final String[] strings = new String[] { "#!/bin/bash",
                                                "# Make sure beginning in main work area",
                                                "cd " + getWorkArea(),
                                                "",
                                                "# Set up the results file",
                                                "mkdir -p results",
                                                "cd results",
                                                "tmp=`mktemp results.XXXXXXXX`",
                                                "results=`pwd`/${tmp}",
                                                "echo  \"" + reference.toString()
                                                                        + "\" > ${results}",
                                                "cd ..",
                                                "",
                                                "# Set up the area for script execution",
                                                "extdir=`mktemp -d external.XXXXXXXX`",
                                                "files=`pwd`/${extdir}/files",
                                                "mkdir -p ${files}",
                                                "wdir=`pwd`/${extdir}/work",
                                                "mkdir -p ${wdir}",
                                                "cp -rp ${1} ${files}/inputs",
                                                "touch ${files}/outputs",
                                                "cd ${wdir}",
                                                "nohup " + getRunnerFilepath()
                                                              + " "
                                                              + script.getAbsolutePath()
                                                              + " \"${files}/inputs\" \"${files}/outputs\" \""
                                                              + reference.toString()
                                                              + "\" \"${results}\" > ${files}/stdout 2> ${files}/stderr < /dev/null &" };
        final List<String> lines = new ArrayList<>(strings.length);
        for (String string : strings) {
            lines.add(string);
        }
        return lines;
    }

    /**
     * Returns the path the the "runner' script used to execute the script in
     * background.
     *
     * @return the path the the "runner' script used to execute the script in
     *         background.
     */
    private static String getRunnerFilepath() {
        if (null != runnerFilepath) {
            return runnerFilepath;
        }
        try {
            final String property = System.getProperty(BashExecution.class.getCanonicalName() + ".runner");
            final File runner;
            if (null == property) {
                if (!BACKGROUND_HOME.exists()) {
                    Files.createDirectories(BACKGROUND_HOME.toPath(),
                                            NO_FILE_ATTRIBUTES);
                }
                runner = new File(BACKGROUND_HOME,
                                  RUNNER_FILE_NAME);
            } else {
                runner = new File(property);
            }
            if (!runner.exists()) {
                copyInputStreamToFile(BashExecution.class,
                                      RUNNER_RESOURCE_NAME,
                                      runner);
            }
            runnerFilepath = runner.getAbsolutePath();
            return runnerFilepath;
        } catch (IOException e) {
            throw new RuntimeException(RUNNER_FILE_NAME + " can not be copied into \""
                                       + BACKGROUND_HOME.getAbsolutePath()
                                       + "\"");
        }
    }

    /**
     * Returns the path the the "work' script used to execute the script in
     * background.
     *
     * @return the path the the "runner' script used to execute the script in
     *         background.
     */
    private static String getWorkArea() {
        if (null != workAreaPath) {
            return workAreaPath;
        }
        try {
            final String property = System.getProperty(BashExecution.class.getCanonicalName() + ".workarea");
            final File workAreaDir;
            if (null == property) {
                if (!BACKGROUND_HOME.exists()) {
                    Files.createDirectories(BACKGROUND_HOME.toPath(),
                                            NO_FILE_ATTRIBUTES);
                }
                workAreaDir = new File(BACKGROUND_HOME,
                                       WORK_AREA_NAME);
            } else {
                workAreaDir = new File(property);
            }
            if (!workAreaDir.exists()) {
                Files.createDirectory(workAreaDir.toPath(),
                                      NO_FILE_ATTRIBUTES);
            }
            workAreaPath = workAreaDir.getAbsolutePath();
            return workAreaPath;
        } catch (IOException e) {
            throw new RuntimeException(WORK_AREA_NAME + " can not be created in \""
                                       + BACKGROUND_HOME.getAbsolutePath()
                                       + "\"");
        }
    }

    /**
     * Executes an instance of this object using the command line input are the
     * command to execute.
     *
     * @param args
     *            the arguments from the command line.
     *
     * @throws IOException
     *             when the command can not be completed because of an IO problem.
     * @throws InterruptedException
     *             when it is interrupted while waiting for the command to complete.
     *
     */
    public static void main(String args[]) throws IOException,
                                           InterruptedException {
        int begin = 0;
        final boolean preserve;
        if (args[begin].startsWith("-p")) {
            begin += 1;
            preserve = true;
        } else {
            preserve = false;
        }

        String[] arguments = new String[args.length - begin];
        System.arraycopy(args,
                         begin,
                         arguments,
                         0,
                         arguments.length);
        if (1 != arguments.length) {
            throw new IllegalArgumentException("Must specify one, and only one script file");
        }
        final File script = new File(arguments[0]);
        ScriptExecution scriptExecution;
        try {
            scriptExecution = new BashExecution(script,
                                                new URI("bash/background/test"));
            scriptExecution.execute(null,
                                    null,
                                    preserve);
        } catch (URISyntaxException
                 | ExecutionFailedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    /**
     * Creates a instance of this class.
     *
     * @param script
     *            the file containing the script to be executed in background.
     * @param reference
     *            the URI used as the callback reference.
     */
    public BashExecution(File script,
                         URI reference) {
        super(ScriptExecution.APPLICATION_X_BSH,
              getBashBackgroundLauncher(script,
                                        reference));
    }
}
