package gov.lbl.nest.common.tasks;

import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This interface is used by the {@link FoundationTask} class to create new
 * {@link Inspector} instances.
 *
 * @author patton
 *
 * @param <S>
 *            the type of class created by this object.
 */
public interface InspectorFactory<S extends Inspector> {

    /**
     * Create a new {@link Inspector} instance at the time the {@link Task} instance
     * is created.
     *
     * @param task
     *            the {@link Task} instance being created.
     *
     * @return the {@link Inspector} instance created at the time the {@link Task}
     *         instance is created.
     */
    S creatCreatingInspector(FoundationTask<S> task);

    /**
     * Create a new {@link Inspector} instance at the time the {@link Task} instance
     * is beginning execution.
     *
     * @param task
     *            the {@link Task} instance being created.
     *
     * @return the {@link Inspector} instance created at the time the {@link Task}
     *         instance is beginning execution.
     */
    S createBeginningInspector(FoundationTask<S> task);

    /**
     * Create a new {@link Inspector} instance at the time the {@link Task} instance
     * is ended execution.
     *
     * @param task
     *            the {@link Task} instance being created.
     *
     * @return the {@link Inspector} instance created at the time the {@link Task}
     *         instance is ended execution.
     */
    S createEndingInspector(FoundationTask<S> task);

}
