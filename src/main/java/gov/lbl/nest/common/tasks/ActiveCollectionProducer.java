package gov.lbl.nest.common.tasks;

import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This class is an implementation of the {@link Producer} interface that is a
 * FIFO. It does this by using the {@link ActiveDeque} class as the container
 * for {@link Task} instances.
 *
 * @author patton
 *
 * @param <E>
 *            the type of {@link Element} instances produced by this object.
 * @param <S>
 *            the type of class to be returned by the inspect() method of the E
 *            class.
 */
public class ActiveCollectionProducer<E extends ActiveTask<S>, S> implements
                                     Producer<E>,
                                     StatefulSupplier<E> {

    /**
     * The {@link Producer} instance used to feed {@link #consumer} object.
     */
    ActiveCollection<E, S> collection = new ActiveDeque<>();

    /**
     * The {@link Consumer} instance that is fed by the {@link #collection} field.
     */
    private Consumer consumer;

    /**
     * Creates an instance of this class
     *
     * @param collection
     *            the {@link Producer} instance used to feed {@link #consumer}
     *            object.
     */
    protected ActiveCollectionProducer(ActiveCollection<E, S> collection) {
        this.collection = collection;
    }

    /**
     * Returns the number of active element contained in this object.
     *
     * @return the number of active element contained in this object.
     */
    int activeSize() {
        return collection.activeSize();
    }

    /**
     * Adds the supplied {@link Task} instance to this object.
     *
     * @param task
     *            the {@link Task} instance to be added.
     *
     * @return <code>true</code> is the {@link Task} instance is successfully added.
     */
    @Override
    public boolean add(E task) {
        /*
         * Needs to synchronize on this Object to avoid another thread adding a Task
         * while `next()` is executing.
         */
        final boolean result;
        synchronized (this) {
            result = collection.add(task);
        }
        if (result) {
            consumer.addedTask();
        }
        return result;
    }

    @Override
    public void consumer(Consumer consumer) {
        this.consumer = consumer;
    }

    @Override
    public Supplier<E> getSupplier() {
        return this;
    }

    /**
     * Returns the number of inactive elements currently contained in this object.
     *
     * @return the number of inactive elements currently contained in this object.
     */
    int inactiveSize() {
        return collection.inactiveSize();
    }

    @Override
    public Task next() {
        /*
         * always called with this Object synchronized, so do not need to synchronize
         * inside this method.
         */
        return collection.next();
    }

    @Override
    public void reset() {
        final Object synchronizer = consumer.getSynchronizer();
        synchronized (synchronizer) {
            collection.reset();
            /*
             * The notifyAll allows any thread waiting on a change of state in the Tasks to
             * proceed.
             */
            synchronizer.notifyAll();
        }
    }

    @Override
    public void restart() {
        synchronized (this) {
            collection.reset();
            /*
             * The flush allows any thread waiting on a change of state in the Tasks to
             * proceed.
             */
            consumer.flush();
        }
    }

    @Override
    public int size() {
        return collection.size();
    }

}
