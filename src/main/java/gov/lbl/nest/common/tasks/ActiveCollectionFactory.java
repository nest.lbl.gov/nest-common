package gov.lbl.nest.common.tasks;

/**
 * This interface is used to create {@link ActiveCollection} instances.
 *
 * @author patton
 *
 * @param <E>
 *            the type of {@link Element} instances contained in the created
 *            collections
 * @param <S>
 *            the type of class to be returned by the inspect() method of the E
 *            class.
 */
public interface ActiveCollectionFactory<E extends Element<S>, S> {

    /**
     * Creates a new {@link ActiveCollection} instance.
     *
     * @return the created {@link ActiveCollection} instance.
     */
    ActiveCollection<E, S> createCollection();
}
