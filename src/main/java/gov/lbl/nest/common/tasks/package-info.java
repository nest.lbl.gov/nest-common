/**
 * This package provides the interfaces and classes that can be used to manage
 * the execution of task.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.util}</li>
 * <li>{@link org.slf4j}</li>
 * </ul>
 */
package gov.lbl.nest.common.tasks;
