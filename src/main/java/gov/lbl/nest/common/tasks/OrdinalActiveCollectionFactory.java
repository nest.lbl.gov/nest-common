package gov.lbl.nest.common.tasks;

/**
 * This class is used to create new instances of the
 * {@link OrdinalActiveCollection} class.
 *
 * @author patton
 *
 * @param <E>
 *            the type of Object contained in this container.
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public class OrdinalActiveCollectionFactory<E extends Element<S>, S> implements
                                           ActiveCollectionFactory<E, S> {

    /**
     * The {@link ActiveCollectionFactory} that instances created by this Object
     * will use.
     */
    private final ActiveCollectionFactory<E, S> factory;

    /**
     * The {@link OrdinalExtractor} that instances created by this Object will use.
     */
    private final OrdinalExtractor<E> ordinal;

    /**
     * Creates an instance of this class.
     *
     * @param factory
     *            the {@link ActiveCollectionFactory} that instances created by this
     *            Object will use.
     * @param ordinal
     *            the {@link OrdinalExtractor} that instances created by this Object
     *            will use.
     */
    public OrdinalActiveCollectionFactory(ActiveCollectionFactory<E, S> factory,
                                          OrdinalExtractor<E> ordinal) {
        this.factory = factory;
        this.ordinal = ordinal;
    }

    @Override
    public ActiveCollection<E, S> createCollection() {
        return new OrdinalActiveCollection<>(factory,
                                             ordinal);
    }

}
