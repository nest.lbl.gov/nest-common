package gov.lbl.nest.common.tasks;

import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This interface is used to extend the {@link Supplier} interface for
 * {@link Task} instances that maintain some sort of volatile internal state.
 * The volatile state of a {@link Task} instance is inspected by implementation
 * of this object and may not be supplied to the associated {@link Consumer}
 * instance if they fail a given criteria. If the {@link Task} fails a criteria,
 * it is not
 *
 * @author patton
 *
 * @param <T>
 *            the type of {@link Task} this class supplies
 */
public interface StatefulSupplier<T extends Task> extends
                                 Supplier<T> {

    /**
     * Resets this Object such that those {@link Task} instance what have failed to
     * pass this Object's criteria are once again available for supply to the
     * associated {@link Consumer} instance.
     */
    void reset();

    /**
     * Restarts this Object by executing the {@link #reset()} method of this call
     * and then executing the {@link Consumer#flush()} method for the associated
     * {@link Consumer} instance.
     */
    void restart();
}
