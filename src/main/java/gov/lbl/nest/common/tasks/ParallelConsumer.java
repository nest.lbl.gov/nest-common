package gov.lbl.nest.common.tasks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntBinaryOperator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class manages the execution of a sequence of
 * {@link gov.lbl.nest.common.tasks.Consumer.Task} instances that are supplied
 * by a {@link gov.lbl.nest.common.tasks.Consumer.Producer} instance. Up to the
 * "executionLimit" number of {@link gov.lbl.nest.common.tasks.Consumer.Task}
 * instance are executed in parallel. The next available
 * {@link gov.lbl.nest.common.tasks.Consumer.Task} instance from the
 * {@link gov.lbl.nest.common.tasks.Consumer.Producer} instance is pulled only
 * when it can immediately be executed.
 *
 * @author patton
 */
public class ParallelConsumer implements
                              Consumer {

    /**
     * This class is used to manage the execution of {@link Task} instances with a
     * single {@link Thread}.
     *
     * @author patton
     */
    private class Worker implements
                         Runnable {
        private boolean flushed = false;

        private final boolean revived;

        Worker(final boolean revived) {
            this.revived = revived;
        }

        /**
         * Called when this Object should terminate as it is no longer part of its
         * {@link Consumer} instance.
         */
        void flush() {
            flushed = true;
        }

        @Override
        public void run() {
            final String action;
            if (revived) {
                action = "Reviving";
            } else {
                action = "Starting up";
            }
            final int count;
            synchronized (workerCount) {
                count = workerCount.incrementAndGet();
                workerCount.notifyAll();
            }
            LOG.debug(action + " a ThreadWorker for \""
                      + getName()
                      + "\", there are "
                      + Integer.toString(count)
                      + " running ThreadWorkers now");
            while (!flushed && isRequired()) {
                Task task = null;
                if (paused.get()) {
                    synchronized (paused) {
                        while (paused.get()) {
                            try {
                                paused.wait();
                            } catch (InterruptedException e) {
                                // do nothing and try again
                                e.printStackTrace();
                            }
                        }
                    }
                }
                try {

                    /*
                     * `producer` can be accessed by multiple workers, so synchronize.
                     *
                     * Also remember `workersWaiting` is always protected by synchronization on the
                     * `producer`, that variable is can be a "native" integer.
                     */
                    synchronized (producer) {
                        task = producer.next();
                        int maxAttempts = pullAttempts;
                        int attempts = maxAttempts;
                        boolean waiting = (null == task && attempts > 0);
                        while (waiting) {
                            if (maxAttempts == attempts) {
                                ++workersWaiting;
                            }
                            --attempts;
                            producer.wait(pullWaitTime);
                            task = producer.next();
                            waiting = (null == task && attempts > 0);
                        }
                        if (maxAttempts != attempts) {
                            --workersWaiting;
                        }
                    }
                    if (null == task) {
                        if (flushed) {
                            LOG.debug("Flushing a Thread for \"" + getName()
                                      + "\"");
                            return;
                        }
                        decreaseWorkers();
                        final int remaining;
                        synchronized (workerCount) {
                            remaining = workerCount.decrementAndGet();
                            workerCount.notifyAll();
                        }
                        LOG.debug("A ThreadWorker (and its Thread) are terminating for \"" + getName()
                                  + "\", as it is not currently needed, there are "
                                  + Integer.toString(remaining)
                                  + " remaining ThreadWorkers running");
                        synchronized (executionLimit) {
                            workers.remove(this);
                        }
                        return;
                    }

                    /*
                     * `execution` now only access by this Thread, but implementation of the Task
                     * interface must make sure they are thread safe with respect to any interaction
                     * out the Task implementation.
                     */
                    task.beginExecution();
                    executingCount.incrementAndGet();
                    try {
                        task.run();
                    } finally {
                        synchronized (executionLimit) {
                            if (workers.contains(this)) {
                                executingCount.decrementAndGet();
                            }
                        }
                        task.endExecution();
                    }
                } catch (Throwable e) {
                    if (null == System.getProperty("gov.lbl.nest.common.suppressTracebacks")) {
                        LOG.error("Uncaught Throwable, an instance of the \"" + (e.getClass()).getName()
                                  + "\" class. It will be ignored and this thread will continue");
                        e.printStackTrace();
                    }
                }
            }
            if (flushed) {
                LOG.debug("Flushing a Thread for \"" + getName()
                          + "\"");
            }
            final int remaining;
            synchronized (workerCount) {
                remaining = workerCount.decrementAndGet();
                workerCount.notifyAll();
            }
            LOG.debug("A ThreadWorker (and its Thread) are shutting down \"" + getName()
                      + "\", as it is no longer required, there are "
                      + Integer.toString(remaining)
                      + " remaining ThreadWorkers running");
            synchronized (executionLimit) {
                workers.remove(this);
            }
        }

    }

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ParallelConsumer.class);

    /**
     * The default number of attempts to pull a new {@link Task} instance before a
     * {@link Worker} instance terminates.
     */
    private static final int DEFAULT_PULL_ATTEMPTS = 10;

    /**
     * The default time, in milliseconds, that a thread should wait between attempts
     * to pull a new {@link Task} instance.
     */
    private static final int DEFAULT_PULL_WAIT_TIME = 1000;

    /**
     * The number of {@link Task} instances that are currently in their
     * {@link Task#run()} method.
     */
    private final AtomicInteger executingCount = new AtomicInteger();

    /**
     * The maximum number of {@link Task} instances that can be executing at the
     * same time. Minimum value is 0.
     */
    private final AtomicInteger executionLimit = new AtomicInteger();

    /**
     * The {@link Executor} instance used by this Object.
     */
    private final Executor executor;

    /**
     * True when the consumption of tasks has been paused.
     */
    private final AtomicBoolean paused = new AtomicBoolean();

    /**
     * The name used to identify this Object in the log files.
     */
    private final String name;

    /**
     * The {@link Producer} instance to be used by this Object.
     */
    private final Producer<?> producer;

    /**
     * The number of attempts to pull a new {@link Task} instance before a
     * {@link Worker} instance terminates.
     */
    private int pullAttempts = DEFAULT_PULL_ATTEMPTS;

    /**
     * The default time, in milliseconds, that a thread should wait between attempts
     * to pull a new {@link Task} instance.
     */
    private int pullWaitTime = DEFAULT_PULL_WAIT_TIME;

    /**
     * The number of {@link Worker} that have been started, as opposed to be
     * revived.
     */
    private int started;

    /**
     * The requested number of executing {@Task} instances.
     */
    private int threads;

    /**
     * The number of active {@link Worker} instances that are currently executing.
     * Note: This is less or equal to the {@link #workers} size as it's value is
     * manipulated by the {@link Worker} instance itself.
     */
    private final AtomicInteger workerCount = new AtomicInteger();

    /**
     * The collection of currently active {@link Worker} instances.
     */
    private Collection<Worker> workers = new ArrayList<>();

    /**
     * The number of {@link Worker} instances waiting for a new {@link Task}
     * instance.
     *
     * This field is always synchronized on the {@link #producer} field.
     */
    private int workersWaiting;

    /**
     * Creates and instance of this class.
     *
     * @param name
     *            the name used to identify this Object in the log files.
     * @param initialCount
     *            the initial number of
     *            {@link gov.lbl.nest.common.tasks.Consumer.Task} instances that can
     *            be executing at the same time.
     * @param producer
     *            the {@link gov.lbl.nest.common.tasks.Consumer.Producer} instance
     *            to be used by this Object.
     * @param executor
     *            the {@link Executor} instance used by this Object.
     */
    public ParallelConsumer(String name,
                            int initialCount,
                            Producer<?> producer,
                            Executor executor) {
        this.executionLimit.getAndSet(initialCount);
        this.executor = executor;
        this.name = name;
        this.producer = producer;
        producer.consumer(this);
        start();
    }

    @Override
    public boolean addedTask() {
        final boolean noAvailableWorker;
        synchronized (producer) {
            /*
             * "Captures" whether there is at least one Worker waiting in the loop that gets
             * the next Task.
             */
            noAvailableWorker = (0 == workersWaiting);
            /*
             * This interrupts any Worker that may be waiting in the loop, so they can pick
             * up the new Task when this block ends.
             */
            producer.notifyAll();
        }
        if (noAvailableWorker) {
            /*
             * If there were no Workers waiting then a new one may be created if that does
             * not go over the executionLimit of this Object.
             */
            return increaseWorkers();
        }
        return false;
    }

    /**
     * Increased by one the maximum number of
     * {@link gov.lbl.nest.common.tasks.Consumer.Task} instances that can be
     * executing at the same time.
     *
     * @return the resulting the maximum number of
     *         {@link gov.lbl.nest.common.tasks.Consumer.Task} instances that can be
     *         executing at the same time.
     */
    public Integer addThread() {
        final Integer result = executionLimit.incrementAndGet();
        // Start added thread, will wither an die if not immediately required.
        increaseWorkers();
        return result;
    }

    /**
     * Decreased the number {@link Worker} instances in this Object by one.
     */
    private void decreaseWorkers() {
        /*
         * All changes to `threads` are synchronized on `executionLimit`
         */
        synchronized (executionLimit) {
            if (threads > 0) {
                --threads;
            }
        }
    }

    @Override
    public void flush() {
        /*
         * All changes to `threads` are synchronized on `executionLimit`
         */
        synchronized (executionLimit) {
            synchronized (producer) {
                for (Worker worker : workers) {
                    worker.flush();
                }
                producer.notifyAll();
            }
            synchronized (workerCount) {
                workerCount.set(0);
                workerCount.notifyAll();
            }

            started = 0;
            threads = 0;
            workers.clear();
            executingCount.set(0);
            LOG.debug("Flushing existing ThreadWorkers, if any, for " + getName()
                      + "\" and restarting a new set");
            start();
        }
    }

    /**
     *
     * Returns the number of active {@link Worker} instances that are currently
     * executing.
     *
     * @return the number of active {@link Worker} instances that are currently
     *         executing.
     */
    public AtomicInteger getActiveCount() {
        return workerCount;
    }

    @Override
    public AtomicInteger getExecutingCount() {
        return executingCount;
    }

    @Override
    public int getExecutionLimit() {
        return executionLimit.get();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Supplier<?> getSupplier() {
        return producer.getSupplier();
    }

    @Override
    public Object getSynchronizer() {
        return producer;
    }

    /**
     * Increased the number {@link Worker} instances in this Object by one if
     * appropriate.
     *
     * @return <true> if the number of workers has been increased by one.
     */
    private boolean increaseWorkers() {
        /*
         * All changes to `threads` are synchronized on `executionLimit`
         */
        synchronized (executionLimit) {
            if (executionLimit.get() > threads) {
                final boolean revived;
                if (started == threads) {
                    ++started;
                    revived = false;
                } else {
                    revived = true;
                }
                ++threads;
                final Worker worker = new Worker(revived);
                workers.add(worker);
                executor.execute(worker);
                return true;
            }
            return false;
        }
    }

    private boolean isRequired() {
        /*
         * All changes to `threads` are synchronized on `executionLimit`
         */
        synchronized (executionLimit) {
            if (threads > executionLimit.get()) {
                --threads;
                if (started > threads) {
                    started = threads;
                }
                return false;
            }
            return true;
        }
    }

    @Override
    public boolean pause() {
        if (!paused.get()) {
            synchronized (paused) {
                paused.set(true);
                paused.notifyAll();
            }
            return paused.get();
        }
        return true;
    }

    @Override
    public boolean proceed() {
        if (paused.get()) {
            synchronized (paused) {
                paused.set(false);
                paused.notifyAll();
            }
            return paused.get();
        }
        return false;
    }

    /**
     * Decreased by one the maximum number of
     * {@link gov.lbl.nest.common.tasks.Consumer.Task} instances that can be
     * executing at the same time. If the number of executing
     * {@link gov.lbl.nest.common.tasks.Consumer.Task} instances is already at the
     * original the maximum number the n the next
     * {@link gov.lbl.nest.common.tasks.Consumer.Task} instance to complete is not
     * be replaced by starting the execution of a new one.
     *
     * @return the resulting the maximum number of
     *         {@link gov.lbl.nest.common.tasks.Consumer.Task} instances that can be
     *         executing at the same time.
     */
    public Integer removeThread() {
        return executionLimit.accumulateAndGet(1,
                                               new IntBinaryOperator() {

                                                   @Override
                                                   public int applyAsInt(int left,
                                                                         int right) {
                                                       if (0 == left) {
                                                           return 0;
                                                       }
                                                       return left - right;
                                                   }
                                               });
    }

    /**
     * The number of attempts to pull a new
     * {@link gov.lbl.nest.common.tasks.Consumer.Task} instance before a
     * {@link Worker} instance terminates.
     *
     * @param attempts
     *            the number of attempts to pull a new
     *            {@link gov.lbl.nest.common.tasks.Consumer.Task} instance before a
     *            {@link Worker} instance terminates.
     */
    public void setPullWaitAttempts(int attempts) {

    }

    /**
     * Sets the default time, in milliseconds, that a thread should wait between
     * attempts to pull a new {@link gov.lbl.nest.common.tasks.Consumer.Task}
     * instance.
     *
     * @param millis
     *            the default time, in milliseconds, that a thread should wait
     *            between attempts to pull a new
     *            {@link gov.lbl.nest.common.tasks.Consumer.Task} instance.
     */
    public void setPullWaitTime(int millis) {
        pullWaitTime = millis;
    }

    /**
     * Launch all available threads as the number of Tasks in the Producers is
     * unknown. Unused ThreadWorkers will be quickly pruned.
     */
    void start() {
        final int finished = executionLimit.intValue();
        for (int launch = 0;
             finished != launch;
             ++launch) {
            increaseWorkers();
        }
    }

}
