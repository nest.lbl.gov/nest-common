package gov.lbl.nest.common.tasks.ordinal;

import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.tasks.FoundationTask;
import gov.lbl.nest.common.tasks.Inspector;
import gov.lbl.nest.common.tasks.InspectorFactory;

/**
 * This class extends the {@link FoundationTask} class to include the
 * {@link PriorityElement} and {@link RankedElement} behaviours.
 *
 * @author patton
 *
 * @param <S>
 *            the type of class to be returned by the {@link #inspect()} method.
 */
public abstract class PriorityRankedTask<S extends Inspector> extends
                                        FoundationTask<S> implements
                                        PriorityRankedElement<S> {

    /**
     * The "priority" of this element, where object with are higher priority a
     * preferred.
     */
    private final Integer priority;

    /**
     * The "rank" of this element, where object with a higher rank are preferred.
     */
    private final Integer rank;

    /**
     * Creates an instance of this class.
     *
     * @param factory
     *            the {@link InspectorFactory} used to create new {@link Inspector}
     *            instances.
     * @param suspendable
     *            the {@link Suspendable} instance used by this object to know
     *            whether it is active or not.
     * @param rank
     *            the "rank" of this element, where object with a higher rank are
     *            preferred.
     * @param priority
     *            the "priority" of this element, where object with are higher
     *            priority a preferred.
     */
    protected PriorityRankedTask(InspectorFactory<S> factory,
                                 Suspendable suspendable,
                                 Integer rank,
                                 Integer priority) {
        super(factory,
              suspendable);
        this.priority = priority;
        this.rank = rank;
    }

    @Override
    public Integer getPriority() {
        return priority;
    }

    @Override
    public Integer getRank() {
        return rank;
    }
}
