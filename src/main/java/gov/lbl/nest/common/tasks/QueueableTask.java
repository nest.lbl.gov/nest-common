package gov.lbl.nest.common.tasks;

import gov.lbl.nest.common.queued.Queueable;
import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * The interface that all {@link Object} instances to be consumed and
 * instrumented must implement.
 *
 * @author patton
 */
public interface QueueableTask extends
                               Queueable,
                               Task {

}
