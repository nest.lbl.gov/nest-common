package gov.lbl.nest.common.tasks;

import java.util.Date;

import gov.lbl.nest.common.queued.QueueableImpl;
import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This class is used to capture timing data able of {@link Task} instance as it
 * is executed.
 *
 * @author patton
 *
 */
public class TimedTask extends
                       QueueableImpl implements
                       QueueableTask {

    /**
     * The {@link Task} to be timed.
     */
    private final Task task;

    /**
     * Creates an instance of this class.
     *
     * @param task
     *            the {@link Task} instance whose timing will be captured by this
     *            Object.
     */
    public TimedTask(Task task) {
        this.task = task;
    }

    @Override
    public void beginExecution() {
        setWhenStarted(new Date());
        task.beginExecution();
    }

    @Override
    public void endExecution() {
        task.endExecution();
        setWhenFinished(new Date());
    }

    @Override
    public void run() {
        task.run();
    }

}
