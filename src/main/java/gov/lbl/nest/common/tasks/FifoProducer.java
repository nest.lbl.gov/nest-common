package gov.lbl.nest.common.tasks;

import java.util.ArrayDeque;
import java.util.Deque;

import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This class is an implementation of the {@link Producer} interface that simple
 * produces {@link Task} instances in a first-in first-out (FIFO) ordering.
 *
 * @author patton
 *
 * @param <T>
 *            the type of {@link Task} this object returns from its
 *            {@link #next()} method.
 */
public class FifoProducer<T extends Task> extends
                         ContainerProducer<T> {

    /**
     * The concrete container instance in which to hold {@link Task} instances.
     */
    private final Deque<Task> fifo = new ArrayDeque<>();

    @Override
    public Supplier<T> getSupplier() {
        return this;
    }

    @Override
    protected boolean isEmpty() {
        return fifo.isEmpty();
    }

    @Override
    protected Task retrieve() {
        return fifo.pollFirst();
    }

    @Override
    public int size() {
        return fifo.size();
    }

    @Override
    protected boolean store(Task task) {
        return fifo.add(task);
    }

}
