package gov.lbl.nest.common.tasks.ordinal;

import gov.lbl.nest.common.tasks.Element;
import gov.lbl.nest.common.tasks.OrdinalExtractor;

/**
 * This class implements the {@link OrdinalExtractor} interface to return the
 * priority of a {@link PriorityElement} instance.
 *
 * @author patton
 *
 * @param <E>
 *            the type of Object contained in this container.
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public class PriorityOrdinal<E extends PriorityElement<S>, S> implements
                            OrdinalExtractor<E> {

    @Override
    public int getOrdinal(E e) {
        return e.getPriority();
    }
}
