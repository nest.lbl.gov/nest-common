package gov.lbl.nest.common.tasks;

/**
 * This interface defines what a class must implement in order to be added to an
 * {@link ActiveCollection} instance.
 *
 * @author patton
 *
 * @param <S>
 *            the type of Object returned by the {@link #inspect()} method.
 */
public interface Element<S> {

    /**
     * Returns the "inspection" that results from the inspecting of this Object.
     *
     * @return the "inspection" that results from the inspecting of this Object.
     */
    S inspect();

    /**
     * Returns <code>true</code> if this Object is "active".
     *
     * @return <code>true</code> if this Object is "active".
     */
    boolean isActive();
}
