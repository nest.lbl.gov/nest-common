package gov.lbl.nest.common.tasks;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * This class implements the {@link ActiveCollection} interface where the
 * contained elements of type E are returned first with respect to their ordinal
 * and then with respect to the implementation returned by an
 * {@link ActiveCollectionFactory}.
 * </p>
 *
 * <p>
 * Each ordinal maintains an {@link ActiveCollection} of elements and when one
 * list is empty the next lowest ordinal list is used. The Ordinal of an element
 * is determined by the {@link OrdinalExtractor} instance provide in the
 * constructor, thus instances of this class can have different ordering for the
 * same type of element. The {@link ActiveCollection} instances used by an
 * instance of this class are created by the supplied
 * {@link ActiveCollectionFactory}.
 * </p>
 *
 * <p>
 * All methods are synchronized as every method affects the state of the
 * instance. Even apparently non-modifying methods, such as {@link #isEmpty()},
 * may change the state, as inactive {@link Element} instances can be skipped.
 * </p>
 *
 * @author patton
 *
 * @param <E>
 *            the type of Object contained in this container.
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public class OrdinalActiveCollection<E extends Element<S>, S> implements
                                    ActiveCollection<E, S> {

    /**
     * The {@link ActiveCollectionFactory} instance used by this object.
     */
    private final ActiveCollectionFactory<E, S> factory;

    /**
     * The currently highest rank that has active {@link Element} instance.
     *
     * Note: this field is only access and modified from within a synchronized block
     * using the {@link #waiting} field, so this field can be modified without any
     * further synchronization.
     */
    private int highWater = -1;

    /**
     * The {@link OrdinalExtractor} instance used by this Object.
     */
    private final OrdinalExtractor<E> extractor;

    /**
     * The collection of {@link ActiveCollection} instances where each
     * {@link ActiveT} contains all the elements of a given rank. .
     */
    private final List<ActiveCollection<E, S>> waiting = new ArrayList<>();

    /**
     * Creates a instance of this class
     *
     * @param factory
     *            the {@link ActiveCollectionFactory} instance this instance will
     *            use to build the {@link ActiveCollection} instances it needs.
     * @param extractor
     *            the {@link OrdinalExtractor} instance to use to get an element's
     *            ordinal.
     */
    public OrdinalActiveCollection(ActiveCollectionFactory<E, S> factory,
                                   OrdinalExtractor<E> extractor) {
        this.factory = factory;
        this.extractor = extractor;
    }

    @Override
    public synchronized int activeSize() {
        int result = 0;
        for (ActiveCollection<E, S> collection : waiting) {
            result += collection.activeSize();
        }
        return result;
    }

    @Override
    public synchronized boolean add(E e) {
        final int ordinal = extractor.getOrdinal(e);

        if (waiting.size() <= ordinal) {
            for (int index = waiting.size();
                 index <= ordinal;
                 ++index) {
                waiting.add(factory.createCollection());
            }
        }
        final ActiveCollection<E, S> ordinalCollection = waiting.get(ordinal);
        boolean result = ordinalCollection.add(e);
        if (result && ordinal > highWater) {
            highWater = ordinal;
        }
        return result;
    }

    @Override
    public synchronized int inactiveSize() {
        int result = 0;
        for (ActiveCollection<E, S> collection : waiting) {
            result += collection.inactiveSize();
        }
        return result;
    }

    @Override
    public synchronized List<S> inspectActive() {
        final List<S> result = new ArrayList<>();
        for (int index = highWater;
             0 <= index;
             --index) {
            result.addAll((waiting.get(index)).inspectActive());
        }
        return result;
    }

    @Override
    public synchronized List<S> inspectAll() {
        final List<S> result = new ArrayList<>();
        for (int index = waiting.size() - 1;
             0 <= index;
             --index) {
            result.addAll((waiting.get(index)).inspectAll());
        }
        return result;
    }

    @Override
    public synchronized boolean isEmpty() {
        for (ActiveCollection<E, S> collection : waiting) {
            if (!collection.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public synchronized E next() {
        if (0 > highWater) {
            return null;
        }

        if ((waiting.get(highWater)).isEmpty()) {
            --highWater;
            while (highWater >= 0 && (waiting.get(highWater)).isEmpty()) {
                --highWater;
            }
            if (0 > highWater) {
                return null;
            }
        }
        return (waiting.get(highWater)).next();
    }

    @Override
    public synchronized void reset() {
        for (ActiveCollection<E, S> collection : waiting) {
            collection.reset();
        }
        highWater = waiting.size() - 1;
    }

    @Override
    public synchronized int size() {
        int result = 0;
        for (ActiveCollection<E, S> collection : waiting) {
            result += collection.size();
        }
        return result;
    }
}
