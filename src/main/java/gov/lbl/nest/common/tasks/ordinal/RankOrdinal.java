package gov.lbl.nest.common.tasks.ordinal;

import gov.lbl.nest.common.tasks.Element;
import gov.lbl.nest.common.tasks.OrdinalExtractor;

/**
 * This class implements the {@link OrdinalExtractor} interface to return the
 * rank of a {@link RankedElement} instance.
 *
 * @author patton
 *
 * @param <E>
 *            the type of Object contained in this container.
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public class RankOrdinal<E extends RankedElement<S>, S> implements
                        OrdinalExtractor<E> {

    @Override
    public int getOrdinal(E e) {
        return e.getRank();
    }
}
