package gov.lbl.nest.common.tasks;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * This interface is used to manage the execution of a sequence of {@link Task}
 * instances that are supplied by a {@link Producer} instance.
 *
 * @author patton
 */
public interface Consumer {

    /**
     * This interface is used by a {@link Consumer} instance in order to get the
     * next {@link Task} instance to consume.
     *
     * @author patton
     *
     * @param <T>
     *            the type of {@link Task} this object returns from its
     *            {@link #next()} method.
     */
    public interface Producer<T extends Task> {

        /**
         * The callback from the {@link Consumer} instance consuming this Object, so
         * this Object can invoke the consumer's `addedTask` method.
         *
         * @param consumer
         *            the {@link Consumer} instance consuming {@link Task} instances
         *            from the Object.
         */
        void consumer(Consumer consumer);

        /**
         * Returns the primary {@link Supplier} instance that is suppling this Object.
         *
         * @return the primary {@link Supplier} instance that is suppling this Object.
         */
        Supplier<T> getSupplier();

        /**
         * Returns the next {@link Task} instance to be executed.
         *
         * This is always called with this Object synchronized so implementations do
         * should not have to worry about synchronization implementing this method, but
         * other methods that change this Object, e.g. an add method, will need to also
         * synchronize on this Object.
         *
         * @return the next {@link Task} instance to be executed.
         */
        Task next();

        /**
         * Returns the number of {@link Task} instance currently contains by this
         * Object.
         *
         * @return the number of {@link Task} instance currently contains by this
         *         Object.
         */
        int size();

    }

    /**
     * The interface that all {@link Object} instances to be consumed must
     * implement.
     *
     * @author patton
     */
    public interface Task extends
                          Runnable {

        /**
         * Called when this Object is about begin its execution.
         */
        void beginExecution();

        /**
         * Called when this Object has ended execution.
         */
        void endExecution();
    }

    /**
     * Called when a {@link Task} instance is added to the {@link Producer}
     * instance. This can start the execution of the added instance if maximum
     * number of {@link Task} instances that can be executing at the same time has
     * not been reached.
     *
     * @return <code>true</code> if the added {@link Task} instance will be
     *         immediately executed.
     */
    boolean addedTask();

    /**
     * Flush all the current {@link Thread} instances by letting them finish their
     * current {@link Task} instance and stop, while restarted a new set of
     * {@link Thread} instances to continue {@link Task} instance execution.
     */
    void flush();

    /**
     * Returns the number of {@link Task} instances that are currently in their
     * {@link Task#run()} method. This does <em>not</em> include any {@link Task}
     * instances that were flushed by a call to {@link #flush()}.
     *
     * @return the number of {@link Task} instances that are currently in their
     *         {@link Task#run()} method.
     */
    AtomicInteger getExecutingCount();

    /**
     * Returns the maximum number of {@link Task} instances that can be executing at
     * the same time.
     *
     * @return the maximum number of {@link Task} instances that can be executing at
     *         the same time.
     */
    int getExecutionLimit();

    /**
     * Returns the name used to identify this Object in the log files.
     *
     * @return the name used to identify this Object in the log files.
     */
    String getName();

    /**
     * Returns the primary {@link Supplier} instance that is suppling the
     * {@link Producer} instance.
     *
     * @return the primary {@link Supplier} instance that is suppling the
     *         {@link Producer} instance.
     */
    Supplier<?> getSupplier();

    /**
     * Returns the {@link Object} instance that is used to synchronize the
     * consumption of {@link Task} instances. This should be user to in as the
     * argument for synchronized block in any method that may change one or
     * more{@link Task} instances.
     *
     * @return the {@link Object} instance that is used to synchronize the
     *         consumption of {@link Task} instances.
     *
     * @see ActiveCollectionProducer
     */
    Object getSynchronizer();

    /**
     * Temporarily stop the consumption of tasks.
     *
     * @return true when the consumption of tasks has been paused.
     */
    boolean pause();

    /**
     * Continue the consumption of tasks after it has been temporarily stopped.
     *
     * @return true when the consumption of tasks is no longer paused.
     */
    boolean proceed();
}
