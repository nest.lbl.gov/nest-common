package gov.lbl.nest.common.tasks;

/**
 * This interface is used to extract the ordinal value used by the
 * {@link OrdinalActiveCollection} class to order Objects of class E.
 *
 * @param <E>
 *            the type of the Object whose integer value is being extracted.
 *
 * @author patton
 *
 */
public interface OrdinalExtractor<E> {

    /**
     * Returns the ordinal value of the supplied element that is used by the
     * {@link OrdinalActiveCollection} class to order Objects of class E. creates.
     *
     * @param e
     *            the element whose ordinal should be returned.
     *
     * @return the {@link Integer} value of ordinal for the supplied element.
     */
    int getOrdinal(E e);
}
