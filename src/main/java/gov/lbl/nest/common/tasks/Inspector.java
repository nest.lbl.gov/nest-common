package gov.lbl.nest.common.tasks;

/**
 * This interface is used to the basis for create a new class that inspects the
 * {@link FoundationTask} class.
 *
 * @author patton
 *
 */
public interface Inspector {
    // This is a labeling interface.
}
