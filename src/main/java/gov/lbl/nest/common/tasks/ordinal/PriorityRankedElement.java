package gov.lbl.nest.common.tasks.ordinal;

/**
 * This interface combines the {@link RankedElement} and {@link PriorityElement}
 * intefaces into a single one.
 *
 * @author patton
 *
 * @param <S>
 *            the type of Object returned by the {@link #inspect()} method.
 */
public interface PriorityRankedElement<S> extends
                                      RankedElement<S>,
                                      PriorityElement<S> {

}
