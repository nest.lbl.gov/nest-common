package gov.lbl.nest.common.tasks.ordinal;

import gov.lbl.nest.common.tasks.ActiveCollection;
import gov.lbl.nest.common.tasks.ActiveCollectionFactory;
import gov.lbl.nest.common.tasks.Element;

/**
 * This class is used to create new instances of the
 * {@link RankedActiveCollection} class.
 *
 * @author patton
 *
 * @param <E>
 *            the type of Object contained in this container.
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public class RankedActiveCollectionFactory<E extends RankedElement<S>, S> implements
                                          ActiveCollectionFactory<E, S> {

    @Override
    public ActiveCollection<E, S> createCollection() {
        return new RankedActiveCollection<>();
    }

}
