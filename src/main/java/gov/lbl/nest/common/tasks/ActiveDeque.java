package gov.lbl.nest.common.tasks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * This class is an implementation of the {@link ActiveCollection} interface
 * that behaves as a "first in first out", FIFO, collection of {@link Element}
 * instances. Any inactive element encountered is simple skipped, effective
 * moving the beginning of the collection to the next element. The
 * {@link #reset()} method can be seen as resetting the beginning of the
 * collection back to the first inactive element, if there is one.
 * </p>
 * <p>
 * All methods are synchronized as every method affects the state of the
 * instance. Even apparently non-modifying methods, such as {@link #isEmpty()},
 * may change the state, as inactive {@link Element} instances can be skipped.
 * </p>
 *
 * @author patton
 *
 * @param <E>
 *            the type of Object contained in this container.
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public class ActiveDeque<E extends Element<S>, S> implements
                        ActiveCollection<E, S> {

    /**
     * The collection of {@link Element} instances contained in this Object.
     */
    private final List<E> elements = new ArrayList<>();

    /**
     * The index, before which, the elements have been designated as inactive.
     */
    private int firstActive = 0;

    @Override
    public synchronized int activeSize() {
        int result = 0;
        final int finished = elements.size();
        for (int index = firstActive;
             finished != index;
             ++index) {
            if ((elements.get(index)).isActive()) {
                ++result;
            }
        }
        return result;
    }

    @Override
    public boolean add(E e) {
        return elements.add(e);
    }

    @Override
    public synchronized int inactiveSize() {
        return size() - activeSize();
    }

    @Override
    public synchronized List<S> inspectActive() {
        final List<S> result = new ArrayList<>();
        for (E element : (elements.subList(firstActive,
                                           elements.size()))) {
            if (element.isActive()) {
                result.add(element.inspect());
            }
        }
        return result;
    }

    @Override
    public synchronized List<S> inspectAll() {
        final List<S> result = new ArrayList<>();
        final Iterator<E> iterator = elements.iterator();
        while (iterator.hasNext()) {
            result.add((iterator.next()).inspect());
        }
        return result;
    }

    @Override
    public synchronized boolean isEmpty() {
        while (elements.size() != firstActive && !((elements.get(firstActive)).isActive())) {
            ++firstActive;
        }
        if (elements.size() == firstActive) {
            return true;
        }
        return false;
    }

    @Override
    public synchronized E next() {
        if (isEmpty()) {
            return null;
        }
        return elements.remove(firstActive);
    }

    @Override
    public synchronized void reset() {
        firstActive = 0;
    }

    @Override
    public synchronized int size() {
        return elements.size();
    }

}
