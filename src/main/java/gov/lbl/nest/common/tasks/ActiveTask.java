package gov.lbl.nest.common.tasks;

import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This interface what a class must implement in order to be managed by a
 * {@link Producer} that contains a {@link ActiveCollection} implementation.
 *
 * @author patton
 *
 * @param <S>
 *            the type of Object returned by the {@link #inspect()} method.
 */
public interface ActiveTask<S> extends
                           Element<S>,
                           Task {
}
