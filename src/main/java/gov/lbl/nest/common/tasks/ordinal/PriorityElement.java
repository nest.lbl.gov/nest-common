package gov.lbl.nest.common.tasks.ordinal;

import gov.lbl.nest.common.tasks.Element;

/**
 * This interface is used by the {@link PriorityOrdinal} class to return the
 * priority of this Object.
 *
 * @author patton
 *
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public interface PriorityElement<S> extends
                                Element<S> {

    /**
     * Returns the "priority" of this element, where object with are higher priority
     * a preferred.
     *
     * @return the "priority" of this element, where object with are higher priority
     *         a preferred.
     */
    Integer getPriority();
}
