package gov.lbl.nest.common.tasks;

import java.util.List;

/**
 * <p>
 * This interface manages a collection of Objects of type E that can return an S
 * type object when inspected and can be interrogated about whether they are
 * "active" or not. The {@link #next()} method of this interface returns the
 * next active element while skipping any inactive elements. Once an element has
 * been skipped it will not be considered again until the {@link #reset()}
 * method is called, at which point in time the {@link #next()} method moves the
 * be beginning of the list to determine which elements are "active" or not. The
 * ordering of the elements returned by the {@link #next()} method is
 * implementation dependent.
 * </p>
 * <p>
 * The {@link #inspectAll()} and {@link #inspectActive()} methods return a
 * collection of Objects of type S each of which is a summary that results from
 * inspecting each {@link Element}.
 * </p>
 * <p>
 * <b>Note</b>: this class should only be accessed and modified from within a
 * synchronized block, therefore this class does not do its own lock management.
 * </p>
 *
 * @author patton
 *
 * @param <E>
 *            the type of Object contained in this container.
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public interface ActiveCollection<E extends Element<S>, S> {

    /**
     * Returns the number of active element contained in this object.
     *
     * @return the number of active element contained in this object.
     */
    int activeSize();

    /**
     * Adds the supplied element to this object.
     *
     * @param e
     *            the element to add.
     *
     * @return <code>true</code> if the specified element is appended and collection
     *         changes.
     */
    boolean add(E e);

    /**
     * Returns the number of inactive elements currently contained in this object.
     *
     * @return the number of inactive elements currently contained in this object.
     */
    int inactiveSize();

    /**
     * Returns the sequence of "inspections", one for each element, that result from
     * inspect the current contents of this Object. The sequence is ordered by order
     * of currently active elements.
     *
     * @return the sequence of objects that can be used to inspect the current
     *         contents of this Object.
     */
    List<S> inspectActive();

    /**
     * Returns the sequence of "inspections", on for each element, that result from
     * inspect the current contents of this Object. The sequence is ordered by order
     * of both inactive and active elements.
     *
     * @return the sequence of objects that can be used to inspect the current
     *         contents of this Object.
     */
    List<S> inspectAll();

    /**
     * Returns <code>true</code> if this Object contains no active elements.
     *
     * Note: The activity of any remaining elements will be accessed during this
     * call.
     *
     * @return <code>true</code> if this Object contains no active elements.
     */
    boolean isEmpty();

    /**
     * Returns the next element determined by the implementation's policy. If this
     * Object is empty, <code>null</code> is returned.
     *
     * @return the next element determined by the implementation's policy. If this
     *         Object is empty, <code>null</code> is returned.
     */
    E next();

    /**
     * Resets this Object such that those Elements marked as inactive with be
     * reassessed and may be returned by the {@link #next()} method if appropriate.
     */
    void reset();

    /**
     * Returns the total number of active and inactive elements contained in this
     * object.
     *
     * @return the total number of active and inactive elements contained in this
     *         object.
     */
    int size();
}