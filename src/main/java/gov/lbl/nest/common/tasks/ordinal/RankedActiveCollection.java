package gov.lbl.nest.common.tasks.ordinal;

import gov.lbl.nest.common.tasks.ActiveCollection;
import gov.lbl.nest.common.tasks.ActiveDequeFactory;
import gov.lbl.nest.common.tasks.Element;
import gov.lbl.nest.common.tasks.OrdinalActiveCollection;

/**
 * This class implements the {@link ActiveCollection} interface so that the
 * Object returned from the {@link #next()} method is the oldest active Object
 * that has the highest rank.
 *
 * @author patton
 *
 * @param <E>
 *            the type of Object contained in this container.
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public class RankedActiveCollection<E extends RankedElement<S>, S> extends
                                   OrdinalActiveCollection<E, S> {

    /**
     * Creates an instance of this class.
     */
    public RankedActiveCollection() {
        super(new ActiveDequeFactory<E, S>(),
              new RankOrdinal<E, S>());
    }
}
