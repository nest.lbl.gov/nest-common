package gov.lbl.nest.common.tasks;

import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This class creates two {@link Producer} instances, built from two existing
 * ones. One of the new Producers will be able to produce task from both
 * existing Producers to be consumed by a single {@link Consumer} instance,
 * while the other new Producers will feed only one existing Producer to
 * different Consumer. The effect of this that the second existing Producer's
 * Tasks can be consumed by either Consumer, while the first Producer's Tasks
 * are consumer in preference to the Consumer consuming both Producers.
 *
 * @author patton
 *
 * @param <T>
 *            the type of {@link Task} this object returns from its next method.
 */
public class OpportunisticProducers<T extends Task> {

    /**
     * This class implements the {@link Consumer} interface in order to decorate the
     * singleConsumer instance so that its {@link #addedTask()} method will caal
     * both {@link Consumer} instances. capture the {@link #addedTask()} call to the
     * doubleConsumer instance to order to call both existing {@link Consumer}
     * instances.
     *
     * @author patton
     */
    class ConsumerDecorator implements
                            Consumer {

        @Override
        public boolean addedTask() {
            /*
             * Let the singleConsumer take the task first, as that is the "normal" case.
             */

            /*
             * Needs to synchronize on this Object to avoid another thread adding a Task
             * while `next()` is executing.
             */
            final boolean acceptedBySingleConsumer;
            if (null != singleConsumer) {
                synchronized (splitProducer) {
                    acceptedBySingleConsumer = singleConsumer.addedTask();
                }
            } else {
                acceptedBySingleConsumer = false;
            }
            if (!acceptedBySingleConsumer) {
                synchronized (mergeProducer) {
                    return doubleConsumer.addedTask();
                }
            }
            return acceptedBySingleConsumer;
        }

        @Override
        public void flush() {
            singleConsumer.flush();
        }

        @Override
        public AtomicInteger getExecutingCount() {
            return singleConsumer.getExecutingCount();
        }

        @Override
        public int getExecutionLimit() {
            return singleConsumer.getExecutionLimit();
        }

        @Override
        public String getName() {
            return singleConsumer.getName();
        }

        @Override
        public Supplier<T> getSupplier() {
            return splitProducer.getSupplier();
        }

        @Override
        public Object getSynchronizer() {
            return singleConsumer.getSynchronizer();
        }

        @Override
        public boolean pause() {
            return singleConsumer.pause();
        }

        @Override
        public boolean proceed() {
            return singleConsumer.proceed();
        }

    }

    class MergeProducer implements
                        Producer<T> {

        @Override
        public void consumer(Consumer consumer) {
            doubleConsumer = consumer;
            if (null != singleInbound) {
                singleInbound.consumer(doubleConsumer);
            }
            if (null == consumerDecorator) {
                consumerDecorator = new ConsumerDecorator();
                if (null != doubleInbound) {
                    doubleInbound.consumer(consumerDecorator);
                }
            }
        }

        @Override
        public Supplier<T> getSupplier() {
            if (null != singleInbound) {
                return singleInbound.getSupplier();
            }
            return doubleInbound.getSupplier();
        }

        @Override
        public Task next() {
            /*
             * always called with this Object synchronized, but need to synchronize on
             * original Producers.
             */
            if (null != doubleInbound && (null == singleInbound || 0 == singleInbound.size())) {
                synchronized (doubleInbound) {
                    return doubleInbound.next();
                }
            }
            synchronized (singleInbound) {
                return singleInbound.next();
            }
        }

        @Override
        public int size() {
            if (null == singleInbound) {
                return 0;
            }
            return singleInbound.size();
        }
    }

    class SplitProducer implements
                        Producer<T> {

        @Override
        public void consumer(Consumer consumer) {
            singleConsumer = consumer;
            if (null == consumerDecorator) {
                consumerDecorator = new ConsumerDecorator();
                doubleInbound.consumer(consumerDecorator);
            }
        }

        @Override
        public Supplier<T> getSupplier() {
            return doubleInbound.getSupplier();
        }

        @Override
        public Task next() {
            /*
             * always called with this Object synchronized, but need to synchronize on
             * original Producer.
             */
            synchronized (doubleInbound) {
                return doubleInbound.next();
            }
        }

        @Override
        public int size() {
            return doubleInbound.size();
        }
    }

    /**
     * The {@link Consumer} instance that will take Tasks of both existing
     * {@link Producer} instances.
     */
    private Consumer doubleConsumer;

    /**
     * The existing {@link Producer} instance that will feed two {@link Consumer}
     * instances.
     */
    private Producer<T> doubleInbound;

    /**
     * The created {@link Producer} instance that will feed two {@link Consumer}
     * instances.
     */
    private Producer<T> mergeProducer = new MergeProducer();

    /**
     * The {@link ConsumerDecorator} instance that decorates the
     * {@link #singleConsumer} instance.
     */
    private ConsumerDecorator consumerDecorator;

    /**
     * The {@link Consumer} instance that will take Tasks of one existing
     * {@link Producer} instance.
     */
    private Consumer singleConsumer;

    /**
     * The existing {@link Producer} instance that will feed a single
     * {@link Consumer} instance.
     */
    private Producer<T> singleInbound;

    /**
     * The created {@link Producer} instance that will feed a single
     * {@link Consumer} instance.
     */
    private Producer<T> splitProducer = new SplitProducer();

    /**
     * Creates an instance of this class.
     *
     * @param singleInbound
     *            the existing {@link Producer} instance that will feed two
     *            {@link Consumer} instances.
     * @param doubleInbound
     *            the existing {@link Producer} instance that will feed a single
     *            {@link Consumer} instance.
     */
    public OpportunisticProducers(Producer<T> doubleInbound,
                                  Producer<T> singleInbound) {
        this.doubleInbound = doubleInbound;
        this.singleInbound = singleInbound;
    }

    /**
     * Returns the {@link Producer} instance that should be used to construct the
     * {@link Consumer} instance that will consumer from both existing
     * {@link Producer} instances.
     *
     * @return the {@link Producer} instance that should be used to construct the
     *         {@link Consumer} instance that will consumer from both existing
     *         {@link Producer} instances.
     */
    public Producer<T> getDoubleProducer() {
        return mergeProducer;
    }

    /**
     * Returns the {@link Producer} instance that should be used to construct the
     * {@link Consumer} instance that will only consumer from one of the existing
     * {@link Producer} instances.
     *
     * @return the {@link Producer} instance that should be used to construct the
     *         {@link Consumer} instance that will only consumer from one of the
     *         existing {@link Producer} instances.
     */
    public Producer<T> getSingleProducer() {
        return splitProducer;
    }
}
