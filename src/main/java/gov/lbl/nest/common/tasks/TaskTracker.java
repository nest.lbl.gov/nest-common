package gov.lbl.nest.common.tasks;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This class provides an external mechanism for tracking the progress of
 * {@link Task} instances.
 *
 * @author patton
 *
 * @param <T>
 *            The Type of Task that is being tracked.
 */
public class TaskTracker<T extends Task> {

    /**
     * This interface defines the set of called that can be made when a Task changes
     * state.
     *
     * @author patton
     *
     * @param <T>
     *            The Type of Task whose state has been changed.
     */
    public interface StateChange<T> {

        /**
         * This is called when a Task instance has ended execution. If the TaskTracker's
         * sequence of terminated instances is not being cleared by and external call to
         * {@link TaskTracker#getTerminated()} then this should always be false.
         *
         * @param task
         *            the Task instance that ended execution.
         *
         * @return <code>true</code> if the specified task should be included in the
         *         sequence of terminated {@link Task} instances.
         */
        boolean finished(T task);

        /**
         * This is called when a Task instance has been queued for execution.
         *
         * @param task
         *            the Task instance that has been queued.
         */
        void queued(T task);

        /**
         * This is called just before a Task instance starts execution.
         *
         * @param task
         *            the Task instance starting execution.
         */
        void starting(T task);
    }

    private class TrackedTaskHandler implements
                                     InvocationHandler {

        /**
         * The {@link Task} being wrapped.
         */
        private final T task;

        /**
         * Creates an instance of this class.
         *
         * @param task
         *            the {@link Task} being wrapped.
         */
        private TrackedTaskHandler(T task) {
            this.task = task;
            stateChange.queued(task);
            synchronized (waiting) {
                waiting.add(this);
            }
        }

        private void beginExecution() {
            synchronized (waiting) {
                if (waiting.remove(this)) {
                    synchronized (executing) {
                        executing.add(this);
                    }
                }
            }
            stateChange.starting(task);
            task.beginExecution();
        }

        private void endExecution() {
            task.endExecution();
            final boolean keep = stateChange.finished(task);
            synchronized (executing) {
                if (executing.remove(this)) {
                    if (keep) {
                        synchronized (terminated) {
                            terminated.add(this);
                        }
                    }
                }
            }
        }

        private T getWrappedTask() {
            return task;
        }

        @Override
        public Object invoke(Object proxy,
                             Method method,
                             Object[] args) throws Throwable {
            if (Void.TYPE != method.getReturnType()) {
                return method.invoke(task,
                                     args);
            }
            if ("beginExecution".equals(method.getName())) {
                beginExecution();
            } else if ("endExecution".equals(method.getName())) {
                endExecution();
            } else if ("run".equals(method.getName())) {
                task.run();
            } else {
                method.invoke(task,
                              args);
            }
            return null;
        }
    }

    /**
     * The sequence of executing {@link Task} instances ordered by when they
     * started.
     */
    private final List<TrackedTaskHandler> executing = new ArrayList<>();

    /**
     * The sequence of terminated {@link Task} instances ordered by when they
     * finished.
     */
    private final List<TrackedTaskHandler> terminated = new ArrayList<>();

    /**
     * The {@link StateChange} instance, if any, to run when a Task has ended.
     */
    private final StateChange<T> stateChange;

    /**
     * The sequence of waiting {@link Task} instances ordered by when they were
     * queued.
     */
    private final List<TrackedTaskHandler> waiting = new ArrayList<>();

    /**
     * Creates an instance of this class.
     *
     * A object created with this method will not put instances in the sequence of
     * terminated {@link Task} instances.
     */
    public TaskTracker() {
        stateChange = new StateChange<>() {

            @Override
            public boolean finished(T task) {
                return false;
            }

            @Override
            public void queued(T task) {
            }

            @Override
            public void starting(T task) {
            }
        };
    }

    /**
     * Creates an instance of this class.
     *
     * A object created with this method will put instances in the sequence of
     * terminated {@link Task} instances only then that instance returns
     * <code>true</code> for the {@link StateChange#finished(Object)} method.
     *
     * @param stateChange
     *            the {@link StateChange} instance, if any, to run when a Task has
     *            ended.
     */
    public TaskTracker(StateChange<T> stateChange) {
        this.stateChange = stateChange;
    }

    /**
     * Returns the sequence of executing {@link Task} instances ordered by when they
     * started.
     *
     * @return the sequence of executing {@link Task} instances ordered by when they
     *         started.
     */
    public List<T> getExecuting() {
        synchronized (executing) {
            final List<T> result = new ArrayList<>();
            executing.forEach((task) -> result.add(task.getWrappedTask()));
            return result;
        }
    }

    /**
     * Returns the {@link StateChange} instance, if any, being used by this Object.
     *
     * @return the {@link StateChange} instance, if any, being used by this Object.
     */
    public StateChange<T> getStateChange() {
        return stateChange;
    }

    /**
     * Returns the sequence of terminated {@link Task} instances ordered by when
     * they finished. This method also clears the sequence of terminated
     * {@link Task} instances, so it is the equivalent of:
     *
     * <pre>
     * List terminated = getTerminated(true);
     * </pre>
     *
     * @return the sequence of terminated {@link Task} instances ordered by when
     *         they finished.
     */
    public List<T> getTerminated() {
        return getTerminated(true);
    }

    /**
     * Returns the sequence of terminated {@link Task} instances ordered by when
     * they finished.. This method also clears the sequence of terminated
     * {@link Task} instances.
     *
     * @param cleared
     *            <code>true</code> if the sequence of terminated {@link Task}
     *            instances should be cleared.
     *
     * @return the sequence of terminated {@link Task} instances ordered by when
     *         they finished.
     */
    public List<T> getTerminated(boolean cleared) {
        synchronized (terminated) {
            final List<T> result = new ArrayList<>();
            terminated.forEach((task) -> result.add(task.getWrappedTask()));
            if (cleared) {
                terminated.clear();
            }
            return result;
        }
    }

    /**
     * Returns the sequence of waiting {@link Task} instances ordered by when they
     * were queued.
     *
     * @return the sequence of waiting {@link Task} instances ordered by when they
     *         were queued.
     */
    public List<T> getWaiting() {
        synchronized (waiting) {
            final List<T> result = new ArrayList<>();
            waiting.forEach((task) -> result.add(task.getWrappedTask()));
            return result;
        }
    }

    /**
     * Returns a {@link Task} instance that will be used in place of the supplied
     * instance.
     *
     * @param <U>
     *            The Type of object to be returned.
     *
     * @param task
     *            the {@link Task} instance to be tracked.
     * @param clazz
     *            the class of the {@link Task} instance to be returned.
     *
     * @return a {@link Task} instance that will be used in place of the supplied
     *         instance.
     */
    public <U extends Task> U trackTask(T task,
                                        Class<U> clazz) {
        if (null == task) {
            throw new NullPointerException("task can not be null");
        }
        final InvocationHandler handler = new TrackedTaskHandler(task);
        @SuppressWarnings("unchecked")
        U result = (U) Proxy.newProxyInstance((task.getClass()).getClassLoader(),
                                              new Class[] { clazz },
                                              handler);
        return result;
    }
}
