package gov.lbl.nest.common.tasks;

import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This class enables a {@link Producer} instance to be created by adapting a
 * simple container class to hold {@link Task} instances while they are awaiting
 * execution by a {@link Consumer} instance.
 *
 * @author patton
 *
 * @param <T>
 *            the type of {@link Task} this object returns from its
 *            {@link #next()} method.
 */
public abstract class ContainerProducer<T extends Task> implements
                                       Producer<T>,
                                       Supplier<T> {

    /**
     * The {@link Consumer} instance that is using this Object.
     */
    private Consumer consumer;

    @Override
    public final boolean add(Task task) {
        return addTask(task);
    }

    /**
     * Signals to the {@link Consumer} instance that is using this Object that a new
     * {@link Task} instance has been added to its parent Object.
     *
     * @param task
     *            the {@link Task} instance being added
     *
     * @return <code>true</code> if the {@link Task} instance has been successfully
     *         added.
     */
    boolean addTask(Task task) {
        /*
         * Needs to synchronize on this Object to avoid another thread adding a Task
         * while `next()` is executing.
         */
        final boolean result;
        synchronized (this) {
            result = store(task);
        }
        consumer.addedTask();
        return result;
    }

    @Override
    public void consumer(Consumer consumer) {
        this.consumer = consumer;
    }

    @Override
    public Supplier<T> getSupplier() {
        return this;
    }

    /**
     * Returns <code>true</code> if this Object does not currently contain any
     * {@link Task} instances.
     *
     * @return <code>true</code> if this Object does not currently contain any
     *         {@link Task} instances.
     */
    protected abstract boolean isEmpty();

    @Override
    public Task next() {
        /*
         * always called with this Object synchronized, so do not need to synchronize
         * inside this method.
         */
        if (isEmpty()) {
            return null;
        }
        return retrieve();
    }

    /**
     * Returns the next {@link Task} instance that should be executed, and removes
     * it from this Object.
     *
     * @return the next {@link Task} instance that should be executed, and removes
     *         it from this Object.
     */
    protected abstract Task retrieve();

    /**
     * Stores the supplied {@link Task} instance within this Object.
     *
     * @param task
     *            the task to be stored.
     *
     * @return <code>true</code> if the {@link Task} instance has been successfully
     *         stored.
     */
    protected abstract boolean store(Task task);
}
