package gov.lbl.nest.common.tasks;

import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.queued.Queueable;
import gov.lbl.nest.common.queued.QueueableImpl;
import gov.lbl.nest.common.queued.Queued;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * The class implements the {@link ActiveTask}, {@link Queueable} and
 * {@link Queued} interfaces to provide a foundation upon which new {@link Task}
 * instances can be built.
 *
 * @author patton
 *
 * @param <S>
 *            the type of class to be returned by the {@link #inspect()} method.
 */
public abstract class FoundationTask<S extends Inspector> extends
                                    QueueableImpl implements
                                    ActiveTask<S> {

    /**
     * The {@link InspectorFactory} used to create new {@link Inspector} instances.
     */
    private final InspectorFactory<S> factory;

    /**
     * A reference to the {@link Inspector} instance describing this object.
     */
    private final AtomicReference<S> inspector = new AtomicReference<>();

    /**
     * The {@link Suspendable} instance used by this object to know whether it is
     * active or not.
     */
    private final Suspendable suspendable;

    /**
     * Creates an instance of this class.
     *
     * @param factory
     *            the {@link InspectorFactory} used to create new {@link Inspector}
     *            instances.
     * @param suspendable
     *            the {@link Suspendable} instance used by this object to know
     *            whether it is active or not.
     */
    protected FoundationTask(InspectorFactory<S> factory,
                             Suspendable suspendable) {
        this.factory = factory;
        inspector.set(factory.creatCreatingInspector(this));
        this.suspendable = suspendable;
    }

    @Override
    public void beginExecution() {
        setWhenStarted(new Date());
        inspector.set(factory.createBeginningInspector(this));
    }

    @Override
    public void endExecution() {
        setWhenFinished(new Date());
        inspector.set(factory.createEndingInspector(this));
    }

    @Override
    public final S inspect() {
        return inspector.get();
    }

    @Override
    public final boolean isActive() {
        if (null == suspendable) {
            return true;
        }
        try {
            return !suspendable.isSuspended();
        } catch (InitializingException e) {
            throw new RuntimeException("How did the code get here?",
                                       e);
        }
    }
}
