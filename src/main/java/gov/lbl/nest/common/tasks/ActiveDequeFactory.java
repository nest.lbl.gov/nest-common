package gov.lbl.nest.common.tasks;

/**
 * This class is used to create new instances of the {@link ActiveDeque} class.
 *
 * @author patton
 *
 * @param <E>
 *            the type of Object contained in this container.
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */

public class ActiveDequeFactory<E extends Element<S>, S> implements
                               ActiveCollectionFactory<E, S> {

    @Override
    public ActiveCollection<E, S> createCollection() {
        return new ActiveDeque<>();
    }
}
