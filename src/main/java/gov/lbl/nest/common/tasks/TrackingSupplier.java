package gov.lbl.nest.common.tasks;

import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This class adds tracking information to any {@link Supplier} instance.
 *
 * @author patton
 *
 * @param <T>
 *            the type of {@link Task} this class supplies
 */
public class TrackingSupplier<T extends Task> implements
                             Supplier<T> {

    /**
     * The class of the {@link Task} instance to be returned.
     */
    private final Class<T> clazz;

    /**
     * The {@link Supplier} to which tracking information is being added.
     */
    private final Supplier<T> supplier;

    /**
     * The {@link TaskTracker} instance capturing the added tracking information.
     */
    private final TaskTracker<T> tracker;

    /**
     * Creates an instance of this class.
     *
     * @param supplier
     *            the {@link Supplier} to which tracking information is being added.
     * @param tracker
     *            the {@link TaskTracker} instance capturing the added tracking
     *            information.
     * @param clazz
     *            the class of the {@link Task} instance to be returned.
     */
    public TrackingSupplier(Supplier<T> supplier,
                            TaskTracker<T> tracker,
                            Class<T> clazz) {
        this.clazz = clazz;
        this.supplier = supplier;
        this.tracker = tracker;
    }

    @Override
    public boolean add(T task) {
        final T queuedTask = tracker.trackTask(task,
                                               clazz);
        return supplier.add(queuedTask);
    }

    @Override
    public int size() {
        return supplier.size();
    }

}
