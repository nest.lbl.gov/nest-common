package gov.lbl.nest.common.tasks.ordinal;

import gov.lbl.nest.common.tasks.Element;

/**
 * This interface is used by the {@link RankOrdinal} class to return the rank of
 * this Object.
 *
 * @author patton
 *
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public interface RankedElement<S> extends
                              Element<S> {

    /**
     * Returns the "rank" of this element, where object with a higher rank are
     * preferred.
     *
     * @return the "rank" of this element, where object with a higher rank are
     *         preferred.
     */
    Integer getRank();
}
