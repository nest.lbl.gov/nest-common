package gov.lbl.nest.common.tasks;

import gov.lbl.nest.common.tasks.Consumer.Task;

/**
 * This interface is used to supply new {@link Task} instances for execution.
 *
 * @author patton
 *
 * @param <T>
 *            the type of {@link Task} this class supplies
 */
public interface Supplier<T extends Task> {

    /**
     * Adds a new {@link Task} instance to this Object.
     *
     * @param task
     *            the {@link Task} instance to be added.
     *
     * @return <code>true</code> if the {@link Task} instance has been successfully
     *         added.
     */
    boolean add(T task);

    /**
     * Returns the number of {@link Task} instance currently contains by this
     * Object.
     *
     * @return the number of {@link Task} instance currently contains by this
     *         Object.
     */
    int size();
}
