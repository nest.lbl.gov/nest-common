package gov.lbl.nest.common.tasks.ordinal;

import gov.lbl.nest.common.tasks.Element;
import gov.lbl.nest.common.tasks.OrdinalActiveCollection;

/**
 * This class extends the {@link OrdinalActiveCollection} class so that the
 * Object returned from the {@link #next()} method is the Object with that
 * highest priority and is the oldest active one that has the highest rank.
 *
 * @author patton
 *
 * @param <E>
 *            the type of Object contained in this container.
 * @param <S>
 *            the type of Object returned by the {@link Element#inspect()}
 *            method.
 */
public class PriorityRankedActiveCollection<E extends PriorityRankedElement<S>, S> extends
                                           OrdinalActiveCollection<E, S> {

    /**
     * Creates an instance of this class.
     */
    public PriorityRankedActiveCollection() {
        super(new RankedActiveCollectionFactory<E, S>(),
              new PriorityOrdinal<E, S>());
    }
}
