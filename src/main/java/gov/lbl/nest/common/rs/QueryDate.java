package gov.lbl.nest.common.rs;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class is a utility class to handle date provided as part of a URL query.
 *
 * @author patton
 *
 */
public class QueryDate {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The format for date queries.
     */
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * The formatter for date supplied to this class as Strings.
     */
    private static final DateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    /**
     * The format for date and time queries.
     */
    private static final String DATE_AND_TIME_FORMAT = DATE_FORMAT + "'T'HH:mm:ss";

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    private static final DateFormat DATE_AND_TIME_FORMATTER = new SimpleDateFormat(DATE_AND_TIME_FORMAT);

    /**
     * The format for date and time queries.
     */
    private static final String LONG_DATE_AND_TIME_FORMAT = DATE_FORMAT + "'T'HH:mm:ss.SSS";

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    private static final DateFormat LONG_DATE_AND_TIME_FORMATTER = new SimpleDateFormat(LONG_DATE_AND_TIME_FORMAT);

    /**
     * The format for date and time queries.
     */
    private static final String ZONED_FORMAT = DATE_AND_TIME_FORMAT + "Z";

    /**
     * The formatter to use to create a for date and time (up to seconds) with time
     * zone for a query element.
     */
    public static final DateFormat ZONED_FORMATTER = new SimpleDateFormat(ZONED_FORMAT);

    /**
     * The format for date and time queries.
     */
    private static final String LONG_ZONED_FORMAT = LONG_DATE_AND_TIME_FORMAT + "Z";

    /**
     * The formatter to use to create a for date and time (up to milliseconds) with
     * time zone for a query element.
     */
    public static final DateFormat LONG_ZONED_FORMATTER = new SimpleDateFormat(LONG_ZONED_FORMAT);

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Parses a query date and/or time into a {@link Date} instance.
     *
     * @param dateTime
     *            the date and/or time to parse
     *
     * @return the {@link Date} instance.
     *
     * @throws ParseException
     *             when the date and/or time can not be parsed.
     */
    public static Date parseQueryDate(final String dateTime) throws ParseException {
        if (null == dateTime) {
            return null;
        }

        if (-1 == dateTime.indexOf('.')) {
            if (DATE_FORMAT.length() == dateTime.length()) {
                return DATE_FORMATTER.parse(dateTime);
            }
            if ((DATE_AND_TIME_FORMAT.length() - 2) == dateTime.length()) {
                return DATE_AND_TIME_FORMATTER.parse(dateTime);
            }
            return ZONED_FORMATTER.parse(dateTime);
        }
        if ((LONG_DATE_AND_TIME_FORMAT.length() - 2) == dateTime.length()) {
            return LONG_DATE_AND_TIME_FORMATTER.parse(dateTime);
        }
        return LONG_ZONED_FORMATTER.parse(dateTime);
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
