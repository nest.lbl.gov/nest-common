package gov.lbl.nest.common.rs;

import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.Digests;

/**
 * This class contains the media types used by the RESTful interface.
 *
 * @author patton
 *
 */
public class NestType {

    /**
     * The media type for the XML representation of an {@link Digest} and
     * {@link Digests} instances.
     */
    public static final String DIGESTS_XML = "application/gov.lbl.nest.common.rs.Digests+xml";

    /**
     * The media type for the XML representation of an {@link Digest} and
     * {@link Digests} instances.
     */
    public static final String DIGESTS_JSON = "application/gov.lbl.nest.common.rs.Digests+json";

}
