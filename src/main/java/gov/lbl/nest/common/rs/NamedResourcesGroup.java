package gov.lbl.nest.common.rs;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, a group of
 * {@link NamedResource} instances.
 *
 * @author patton
 */
@XmlType(propOrder = { "name",
                       "description" })
public class NamedResourcesGroup {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The short description of the group.
     */
    private String description;

    /**
     * The name by which the group should be references.
     */
    private String name;

    /**
     * The {@link NamedResource} instances that are in this group.
     */
    private List<? extends NamedResource> resources;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected NamedResourcesGroup() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name by which the group should be references.
     * @param description
     *            the short description of the group.
     * @param resources
     *            the {@link NamedResource} instances that are in this group.
     */
    public NamedResourcesGroup(String name,
                               String description,
                               List<? extends NamedResource> resources) {
        setDescription(description);
        setName(name);
        setResources(resources);
    }

    // instance member method (alphabetic)

    /**
     * Returns the short description of the resource.
     *
     * @return the short description of the resource.
     */
    @XmlElement
    public String getDescription() {
        return description;
    }

    /**
     * Returns the name by which the resource should be references.
     *
     * @return the name by which the resource should be references.
     */
    @XmlElement
    public String getName() {
        return name;
    }

    /**
     * Returns the {@link NamedResource} instances that are in this group.
     *
     * Note: This is not an XmlElement as the subclasses are expected to tailor this
     * element names.
     *
     * @return the {@link NamedResource} instances that are in this group.
     */
    protected List<? extends NamedResource> getResources() {
        return resources;
    }

    /**
     * Sets the short description of the resource.
     *
     * @param description
     *            the short description of the resource.
     */
    protected void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the name by which the resource should be references.
     *
     * @param name
     *            the name by which the resource should be references.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the {@link NamedResource} instances that are in this group.
     *
     * @param resources
     *            the {@link NamedResource} instances that are in this group.
     */
    protected void setResources(List<? extends NamedResource> resources) {
        this.resources = resources;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
