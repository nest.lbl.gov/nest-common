/**
 * This package provides classes that help with the RESTful interface an
 * application.
 *
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.net}</li>
 * <li>{@link java.text}</li>
 * <li>{@link java.util}</li>
 * <li>{@link javax.xml.bind}</li>
 * <li>{@link gov.lbl.nest.common.watching}</li>
 * </ul>
 *
 * @author patton
 */
package gov.lbl.nest.common.rs;