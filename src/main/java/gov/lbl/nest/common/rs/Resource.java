package gov.lbl.nest.common.rs;

import java.net.URI;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, the URI of a
 * resource.
 *
 * @author patton
 */
@XmlType(propOrder = { "uri" })
public class Resource {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The URI of this resource.
     */
    private URI uri;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Resource() {
    }

    /**
     * Creates an instance of this class
     *
     * @param uri
     *            the URI of this resource.
     */
    protected Resource(URI uri) {
        setUri(uri);
    }

    // instance member method (alphabetic)

    /**
     * Returns the URI of this resource.
     *
     * @return the URI of this resource.
     */
    @XmlElement(name = "uri")
    protected URI getUri() {
        return uri;
    }

    /**
     * Sets the URI of this resource.
     *
     * @param uri
     *            the URI of this resource.
     */
    protected void setUri(URI uri) {
        this.uri = uri;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}