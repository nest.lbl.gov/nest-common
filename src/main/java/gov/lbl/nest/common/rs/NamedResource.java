package gov.lbl.nest.common.rs;

import java.net.URI;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, the URI of a
 * named resource.
 *
 * @author patton
 */
@XmlType(propOrder = { "name",
                       "description" })
public class NamedResource extends
                           Resource {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The short description of the resource.
     */
    private String description;

    /**
     * The name by which the resource should be references.
     */
    private String name;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected NamedResource() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI with which to access the resource.
     * @param name
     *            the name by which the resource should be references.
     * @param description
     *            the short description of the resource.
     */
    public NamedResource(URI uri,
                         String name,
                         String description) {
        super(uri);
        setDescription(description);
        setName(name);
    }

    // instance member method (alphabetic)

    /**
     * Returns the short description of the resource.
     *
     * @return the short description of the resource.
     */
    @XmlElement
    public String getDescription() {
        return description;
    }

    /**
     * Returns the name by which the resource should be references.
     *
     * @return the name by which the resource should be references.
     */
    @XmlElement
    public String getName() {
        return name;
    }

    /**
     * Sets the short description of the resource.
     *
     * @param description
     *            the short description of the resource.
     */
    private void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the name by which the resource should be references.
     *
     * @param name
     *            the name by which the resource should be references.
     */
    private void setName(String name) {
        this.name = name;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
