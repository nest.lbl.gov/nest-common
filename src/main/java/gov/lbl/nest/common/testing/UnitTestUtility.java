package gov.lbl.nest.common.testing;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * This class provides a number of utilities that can be used in unit tests.
 *
 * @author patton
 */
public class UnitTestUtility {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The size to dimension byte arrays.
     */
    private static final int BYTE_ARRAY_SIZE = 1024;

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Creates a test file from a resource. The resource name is take to be the same
     * as the traget file name.
     *
     * @param clazz
     *            the class that is acting as the "parent" of this resource.
     * @param file
     *            the file to create.
     *
     * @throws IOException
     *             when the file can not be created.
     */
    public static void createFileFromResource(Class<?> clazz,
                                              File file) throws IOException {
        createFileFromResource(clazz,
                               file.getName(),
                               file);
    }

    /**
     * Creates a test file from a resource.
     *
     * @param clazz
     *            the class that is acting as the "parent" of this resource.
     * @param resource
     *            the name of the resource from which to create the file.
     * @param file
     *            the file to create.
     *
     * @throws IOException
     *             when the file can not be created.
     */
    public static void createFileFromResource(Class<?> clazz,
                                              String resource,
                                              File file) throws IOException {
        final File directory = file.getParentFile();
        if (null != directory && !directory.exists()) {
            throw new FileNotFoundException("Parent directory \"" + directory.getPath()
                                            + "\" does not exist");
        }
        try (FileOutputStream os = new FileOutputStream(file)) {
            final InputStream is = getResourceAsStream(clazz,
                                                       resource);
            final byte[] buffer = new byte[128];
            int count = is.read(buffer);
            while (count >= 0) {
                os.write(buffer,
                         0,
                         count);
                count = is.read(buffer);
            }
        }
    }

    /**
     * Deletes all the files in the supplied array
     *
     * @param files
     *            the array of {@link File} instances to be deleted.
     */
    public static void deleteFiles(File[] files) {
        for (File file : files) {
            if (file.exists()) {
                if (!file.delete()) {
                    throw new IllegalStateException("Can not clear test file \"" + file.getPath()
                                                    + "\"");
                }
            }
        }
    }

    /**
     * Deletes this specified file and, if it is a directory, all files it contains.
     *
     * @param file
     *            the root of the tree to be deleted.
     */
    public static void deleteTree(final File file) {
        if (!file.exists()) {
            return;
        }
        if (file.isDirectory()) {
            final File[] contents = file.listFiles();
            if (null != contents) {
                for (File content : contents) {
                    deleteTree(content);
                }
            }
        }
        file.delete();
        // System.out.println(file.getPath());
    }

    /**
     * Opens a resource as an {@link InputStream} instance or throws an exception.
     *
     * @param clazz
     *            the class that is acting as the "parent" of this resource.
     * @param resource
     *            the name of the resource return as a {@link InputStream} instance.
     *
     * @return the {@link InputStream} from the resource.
     *
     * @throws FileNotFoundException
     *             when the resource can not be found.
     */
    public static InputStream getResourceAsStream(Class<?> clazz,
                                                  String resource) throws FileNotFoundException {
        final InputStream is = clazz.getResourceAsStream(resource);
        if (null == is) {
            throw new FileNotFoundException("Resource " + resource
                                            + " can not be found");
        }
        return is;
    }

    /**
     * Opens a resource as an {@link String} instance or throws an exception.
     *
     * @param clazz
     *            the class that is acting as the "parent" of this resource.
     * @param resource
     *            the name of the resource to return as a {@link String} instance.
     *
     * @return the {@link InputStream} from the resource.
     *
     * @throws FileNotFoundException
     *             when the resource can not be found.
     * @throws IOException
     *             when the resource can not be read.
     */
    public static String getResourceAsString(Class<?> clazz,
                                             String resource) throws FileNotFoundException,
                                                              IOException {
        try (final InputStream is = getResourceAsStream(clazz,
                                                        resource)) {
            try (final Scanner scanner = new Scanner(is).useDelimiter("\\A")) {
                return scanner.next();
            }
        }
    }

    /**
     * Return true if the resource and the file have identical content.
     *
     * @param clazz
     *            the class that is acting as the "parent" of this resource.
     * @param resource
     *            the name of the resource to test.
     * @param file
     *            the {@link File} to test.
     *
     * @return true if the resource and the file have identical content.
     *
     * @throws IOException
     *             when either of the the resource or file can not be read.
     */
    public static boolean identical(Class<?> clazz,
                                    String resource,
                                    File file) throws IOException {
        final InputStream lhs = getResourceAsStream(clazz,
                                                    resource);
        final InputStream rhs = new FileInputStream(file);
        return identical(lhs,
                         rhs);
    }

    /**
     * Return true if the resource and the file have identical content.
     *
     * @param clazz
     *            the class that is acting as the "parent" of this resource.
     * @param resource
     *            the name of the resource to test.
     * @param lines
     *            the list of {@link String} instances to compare.
     *
     * @return true if the resource and the file have identical content.
     *
     * @throws IOException
     *             when either of the resource can not be read.
     */
    public static boolean identical(Class<?> clazz,
                                    String resource,
                                    List<String> lines) throws IOException {
        final InputStream lhs = getResourceAsStream(clazz,
                                                    resource);
        final StringBuilder sb = new StringBuilder();
        for (String line : lines) {
            sb.append(line + "\n");
        }
        final String string = sb.toString();
        final InputStream rhs = new ByteArrayInputStream(string.getBytes());
        return identical(lhs,
                         rhs);
    }

    /**
     * Return true if the {@link InputStream} instances are identical.
     *
     * @param lhs
     *            one {@link InputStream} to test.
     * @param rhs
     *            the other {@link InputStream} to test.
     *
     * @return true if the {@link InputStream} instances are identical.
     *
     * @throws IOException
     *             when either of the two streams can not be read.
     */
    public static boolean identical(InputStream lhs,
                                    InputStream rhs) throws IOException {
        final byte[] lbytes = new byte[BYTE_ARRAY_SIZE];
        final byte[] rbytes = new byte[BYTE_ARRAY_SIZE];
        boolean leof;
        boolean reof;
        boolean identical;
        do {
            leof = readBytesFromStream(lhs,
                                       lbytes);
            reof = readBytesFromStream(rhs,
                                       rbytes);
            identical = Arrays.equals(lbytes,
                                      rbytes);
        } while (identical && !(leof || reof));
        return identical && leof
               && reof;
    }

    /**
     * Reads {@link InputStream} into byte array either filling the array or reading
     * up until the EOF.
     *
     * @param is
     *            the {@link InputStream} to read.
     * @param b
     *            the byte array to fill.
     *
     * @return true if EOF has been found.
     *
     * @throws IOException
     *             when the {@link InputStream} can not be read.
     */
    private static boolean readBytesFromStream(InputStream is,
                                               byte[] b) throws IOException {
        int begin = 0;
        int remaining = b.length;
        boolean eof = false;
        do {
            int added = is.read(b,
                                begin,
                                remaining);
            if (-1 == added) {
                eof = true;
            } else {
                begin += added;
                remaining -= added;
            }
        } while (!eof && 0 != remaining);
        return eof;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
