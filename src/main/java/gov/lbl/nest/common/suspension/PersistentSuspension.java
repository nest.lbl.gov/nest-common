package gov.lbl.nest.common.suspension;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This implements the {@link Suspendable} interface such that the suspension
 * state is stored by the absence or presence if a particular file in the file
 * system.
 *
 * @author patton
 *
 */
public class PersistentSuspension implements
                                  Suspendable {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PersistentSuspension.class);

    // private static member data

    // private instance member data

    /**
     * The file whose existence signals that the element being controlled by this
     * Object is suspended.
     */
    private final File file;

    /**
     * The name of the element being controlled by this Object.
     */
    private final String name;

    /**
     * True when the element being controlled by this Object is suspended.
     */
    private final AtomicBoolean suspended = new AtomicBoolean();

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of the element being controlled by this Object.
     * @param file
     *            the file whose existence signals that the element being controlled
     *            by this Object is suspended.
     */
    public PersistentSuspension(final String name,
                                final File file) {
        this.file = file;
        this.name = name;
        synchronized (suspended) {
            suspended.set(file.exists());
        }
    }

    // instance member method (alphabetic)

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isSuspended() {
        return suspended.get();
    }

    @Override
    public boolean setSuspended(boolean suspend) {
        if (suspend == isSuspended()) {
            return false;
        }
        synchronized (suspended) {
            if (suspend == isSuspended()) {
                return false;
            }

            try {
                if (suspend) {
                    file.createNewFile();
                } else {
                    if (!file.delete()) {
                        throw new IOException("Could not delete file \"" + file.getAbsolutePath()
                                              + "\"");
                    }
                }
            } catch (IOException e) {
                LOG.warn("Could not persist the suspended/resumed state of the Application");
            }
            suspended.set(suspend);
            return true;
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
