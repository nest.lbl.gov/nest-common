package gov.lbl.nest.common.suspension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import gov.lbl.nest.common.configure.InitializingException;

/**
 * This class is a base implementation of the {@link SuspendableCollection}
 * interface.
 *
 * If enforces the following conditions:
 *
 * <ul>
 * <li>When the collection transitions from running to suspended <b>all</b>
 * elements in the collection are suspended.</li>
 * <li>When the collection transitions from suspended to running only those
 * elements running at the previous transition to suspended are resumed.</li>
 * </ul>
 *
 * If the "suspended while running" state of the elements is persisted, those
 * states must be read in at construction time. Therefore those elements must be
 * instantiated in the appropriate state. This means that they (a) need to be
 * instantiated after this class and (b) this class can return their (correct?!)
 * state when the element is not in existence.
 *
 * Any subclass of this class must make sure that when an element is added it
 * state it is in the correct state. That is to say:
 *
 * <ul>
 * <li>suspended when either this object is suspended or the element is
 * suspended while running.</li>
 * <li>running when this object running and the element is not suspended while
 * running.</li>
 * </ul>
 *
 * @author patton
 */
public abstract class SuspendableCollectionImpl implements
                                                SuspendableCollection {

    /**
     * This interface is used to track which elements to resume when the
     * {@link SuspendableCollectionImpl} instance resumes.
     *
     * @author patton
     */
    public interface SuspendedWhileRunning {

        /**
         * Adds the specified {@link CollectableSuspendable} instance to this Object.
         *
         * @param collectable
         *            the {@link CollectableSuspendable} instance to add
         */
        void add(CollectableSuspendable collectable);

        /**
         * Returns true if the specified {@link CollectableSuspendable} instance is in
         * this Object.
         *
         * @param collectable
         *            to test.
         *
         * @return true if the specified {@link CollectableSuspendable} instance is in
         *         this Object.
         */
        boolean contains(CollectableSuspendable collectable);

        /**
         * Returns true if the named {@link CollectableSuspendable} instance is in this
         * Object.
         *
         * @param collectable
         *            to test.
         *
         * @return true if the named {@link CollectableSuspendable} instance is in this
         *         Object.
         */
        boolean contains(String collectable);

        /**
         * Removes the specified {@link CollectableSuspendable} instance to this Object.
         *
         * @param collectable
         *            the {@link CollectableSuspendable} instance to remove
         */
        void remove(CollectableSuspendable collectable);
    }

    /**
     * This class implements the {@link SuspendedWhileRunning} interface in memory.
     *
     * In the case when the {@link SuspendableCollectionImpl} instance is created as
     * being suspended, then all of the elements will be resumed when the
     * {@link SuspendableCollectionImpl} instance resumes, therefore this object is
     * empty.
     *
     * In the case when the {@link SuspendableCollectionImpl} instance is created as
     * running, then all of the elements will be running, therefore this object is
     * empty.
     *
     * <b>Conclusion</b> This object should always be created as empty.
     *
     * @author patton
     */
    public static class SuspendedWhileRunningImpl implements
                                                  SuspendedWhileRunning {

        /**
         * The collection of names of {@link CollectableSuspendable} instances that are
         * suspended while the collection is running.
         */
        private final List<String> list = new ArrayList<>();

        @Override
        public void add(CollectableSuspendable collectable) {
            list.add(collectable.getName());
        }

        @Override
        public boolean contains(CollectableSuspendable collectable) {
            return list.contains(collectable.getName());
        }

        @Override
        public boolean contains(String name) {
            return list.contains(name);
        }

        /**
         *
         * Returns the collection of names of {@link CollectableSuspendable} instances
         * that are suspended while the collection is running.
         *
         * @return the collection of names of {@link CollectableSuspendable} instances
         *         that are suspended while the collection is running.
         */
        protected List<String> getList() {
            return list;
        }

        @Override
        public void remove(CollectableSuspendable collectable) {
            list.remove(collectable.getName());
        }

    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link SuspendableCollection} instance to which this Object belongs.
     */
    private SuspendableCollection collection;

    /**
     * The {@link Suspendable} instance that contains this Object's state.
     */
    private final Suspendable suspendable;

    /**
     * The set of {@link CollectableSuspendable} instances that should not resume
     * when this Object resumes.
     */
    private SuspendedWhileRunning suspendedWhileRunning;

    /**
     * True when this Object is switching the {@link CollectableSuspendable}
     * instances it controls. This acts as a guard to
     * {@link #update(CollectableSuspendable)}.
     */
    private Boolean switching = Boolean.FALSE;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of the set of executions this instance is controlling.
     * @param suspend
     *            True when execution of this object should be suspended at
     *            creation, <code>null</code> means that Object will be suspended at
     *            creation.
     */
    public SuspendableCollectionImpl(String name,
                                     Boolean suspend) {
        this(new SuspendableImpl(name,
                                 suspend));
    }

    /**
     * Creates an instance of this class.
     *
     * @param suspendable
     *            the external {@link Suspendable} instance that contains this
     *            Object's state.
     */
    SuspendableCollectionImpl(Suspendable suspendable) {
        this(suspendable,
             new SuspendedWhileRunningImpl());
    }

    /**
     * Creates an instance of this class.
     *
     * @param suspendable
     *            the external {@link Suspendable} instance that contains this
     *            Object's state.
     * @param whileRunning
     *            the {@link SuspendedWhileRunning} implementation this Object
     *            should use.
     */
    protected SuspendableCollectionImpl(Suspendable suspendable,
                                        SuspendedWhileRunning whileRunning) {
        this.suspendable = suspendable;
        suspendedWhileRunning = whileRunning;
    }

    // instance member method (alphabetic)

    @Override
    public String getName() {
        if (null == suspendable) {
            return null;
        }
        return suspendable.getName();
    }

    @Override
    public boolean isSuspended() throws InitializingException {
        if (null == suspendable) {
            return false;
        }
        return suspendable.isSuspended();
    }

    @Override
    public boolean isSuspended(String name) throws InitializingException {
        if (isSuspended() || suspendedWhileRunning.contains(name)) {
            return true;
        }
        return false;
    }

    @Override
    public void setCollection(SuspendableCollection collection) {
        this.collection = collection;
    }

    @Override
    public boolean setSuspended(boolean suspend) throws InitializingException {
        if (null == suspendable) {
            throw new IllegalStateException("No Suspendable was set for this collection");
        }

        // If state is not changed, return false
        if (isSuspended() == suspend) {
            return false;
        }
        synchronized (suspendable) {
            // Re-check status after lock has been obtained.
            if (isSuspended() == suspend) {
                return false;
            }

            switchCollectableSuspendables(suspend);

            // Propagate this change to any containing collection.
            if (null != collection) {
                collection.update(this);
            }

            return true;
        }
    }

    /**
     * Switches the {@link CollectableSuspendable} instance belonging to this Object
     * to the desired state. This is only ever called from inside a synchronized
     * block.
     *
     * @param suspend
     *            true when the element being controlled by this Object is
     *            suspended.
     *
     * @throws InitializingException
     *             when this Object are still initializing.
     */
    private void switchCollectableSuspendables(boolean suspend) throws InitializingException {
        switching = Boolean.TRUE;
        final Collection<? extends CollectableSuspendable> suspendables = getCollectableSuspendables();
        if (suspend) {
            for (CollectableSuspendable element : suspendables) {
                element.setSuspended(true);
            }
            suspendable.setSuspended(true);
        } else {
            for (CollectableSuspendable element : suspendables) {
                if (!suspendedWhileRunning.contains(element)) {
                    element.setSuspended(false);
                } else if (!element.isSuspended()) {
                    suspendedWhileRunning.remove(element);
                }
            }
            suspendable.setSuspended(false);
        }
        switching = Boolean.FALSE;
    }

    @Override
    public void update(CollectableSuspendable collectable) throws InitializingException {
        if (isSuspended() || switching) {
            return;
        }
        synchronized (suspendable) {
            // Re-check status after lock has been obtained.
            if (isSuspended() || switching) {
                return;
            }

            /*
             * If the collectable was suspended but is now running remove it from the list
             * of collectables that are suspended while this Object is running.
             */
            if (suspendedWhileRunning.contains(collectable) && !collectable.isSuspended()) {
                suspendedWhileRunning.remove(collectable);
                return;
            }

            /*
             * If the collectable was running but is now suspended add it to the list of
             * collectables that are suspended while this Object is running.
             */
            if (collectable.isSuspended()) {
                suspendedWhileRunning.add(collectable);
            }

            // Propagate this change to any containing collection.
            if (null != collection) {
                collection.update(this);
            }
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
