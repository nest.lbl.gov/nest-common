/**
 * This package provides interface and classes that can be combined to enable
 * the execution of a component of a system to be suspended or resumed on
 * command.
 *
 * <h2>Interfaces</h2>
 *
 * <p>
 * The most basic element is the
 * {@link gov.lbl.nest.common.suspension.Suspendable} interface, that must be
 * implemented by the Object controlling a component's execution. This interface
 * simply allows the state of execution to be queried and set.
 * </p>
 *
 * <p>
 * Components can also be grouped together so that they can controlled as a
 * single unit. In this case each Object controlling a component's execution
 * should implement the
 * {@link gov.lbl.nest.common.suspension.CollectableSuspendable} interface that
 * extends the {@link gov.lbl.nest.common.suspension.Suspendable} one by adding
 * a method by which the collecting object is supplied to the Objects
 * controlling the execution of the collected components.
 * </p>
 *
 * <p>
 * The {@link gov.lbl.nest.common.suspension.CollectableSuspendable} objects are
 * managed by an implementation of the
 * {@link gov.lbl.nest.common.suspension.SuspendableCollection} interface, which
 * is , at the same time, an extension of the
 * {@link gov.lbl.nest.common.suspension.CollectableSuspendable} interface
 * allowing for a hierarchy of suspendable components and groups.
 * </p>
 *
 *
 * <h2>Implementations</h2>
 *
 * <p>
 * The {@link gov.lbl.nest.common.suspension.SuspendableImpl} class is a simple
 * implementation of the
 * {@link gov.lbl.nest.common.suspension.CollectableSuspendable} interface
 * (though it does not, of course, need be used in a collection). It is
 * initialized with a <code>name</code> that is used to identify the component
 * whose execution it is managing and the initial <code>suspend</code> state of
 * the component. The default state, i.e. value for <code>null</code>, is
 * "suspended".
 * </p>
 *
 * <p>
 * The {@link gov.lbl.nest.common.suspension.SuspendableCollectionImpl} abstract
 * class, whose
 * {@link gov.lbl.nest.common.suspension.SuspendableCollection#getCollectableSuspendables()}
 * method needs to be implemented, provides a base implementation of a grouping
 * of components that has the following behaviour.
 * </p>
 *
 * <ul>
 * <li>When the collection is suspended, all components are suspended.</li>
 *
 * <li>When the collection is resumed, all components that were running when it
 * was suspended will be resumed.</li>
 *
 * <li>Any component that had been individually resumed and was running when the
 * collection is resumed will remain running.</li>
 * </ul>
 *
 * <p>
 * This behaviour avoids having individually suspended components resuming
 * automatically when the collection is resumed.
 * </p>
 *
 * @author patton
 */
package gov.lbl.nest.common.suspension;