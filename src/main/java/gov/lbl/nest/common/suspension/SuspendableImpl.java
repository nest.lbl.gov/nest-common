package gov.lbl.nest.common.suspension;

import java.util.concurrent.atomic.AtomicBoolean;

import gov.lbl.nest.common.configure.InitializingException;

/**
 * This class is a simple implementation of the {@link Suspendable} interface
 * (and also the {@link CollectableSuspendable} interface) in a thread safe
 * manner.
 *
 * @author patton
 */
public class SuspendableImpl implements
                             CollectableSuspendable {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link SuspendableCollection} instance, if any, which this Object
     * belongs.
     */
    private SuspendableCollection collection;

    /**
     * The name of the set of executions this instance is controlling.
     */
    private final String name;

    /**
     * True is the set of executions is suspended.
     */
    private final AtomicBoolean suspended;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of the set of executions this instance is controlling.
     * @param suspend
     *            True when execution of this object should be suspended at
     *            creation, <code>null</code> means that Object will be suspended at
     *            creation.
     */
    public SuspendableImpl(final String name,
                           final Boolean suspend) {
        this.name = name;
        if (null == suspend) {
            suspended = new AtomicBoolean(true);

        } else {
            suspended = new AtomicBoolean(suspend);
        }
    }

    // instance member method (alphabetic)

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isSuspended() {
        return suspended.get();
    }

    @Override
    public void setCollection(SuspendableCollection collection) {
        this.collection = collection;
    }

    @Override
    public boolean setSuspended(boolean suspend) throws InitializingException {
        final boolean original = suspended.getAndSet(suspend);
        if (original == suspend) {
            return false;
        }
        if (null != collection) {
            collection.update(this);
        }
        return true;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
