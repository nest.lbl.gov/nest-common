package gov.lbl.nest.common.suspension;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.suspension.SuspendableCollectionImpl.SuspendedWhileRunning;

/**
 * This class implements the {@link SuspendedWhileRunning} interface using a
 * file as its backing store.
 *
 * @author patton
 */
public class FileSuspendedWhileRunning extends
                                       SuspendableCollectionImpl.SuspendedWhileRunningImpl {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(FileSuspendedWhileRunning.class);

    // private static member data

    // private instance member data

    /**
     * The file, if any, containing the set of names of suspended
     * {@link CollectableSuspendable} instances.
     */
    private File file;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param file
     *            the file, if any, containing the set of names of suspended
     *            {@link CollectableSuspendable} instances.
     */
    public FileSuspendedWhileRunning(File file) {
        final List<String> list = getList();
        this.file = file;
        if (file.exists()) {
            try (final BufferedReader reader = new BufferedReader(new FileReader(file))) {
                String line = reader.readLine();
                while (line != null) {
                    list.add(line);
                    line = reader.readLine();
                }
            } catch (IOException e) {
                LOG.warn("Could not persist the suspended/resumed state of the Activities");
            }

        }
    }

    // instance member method (alphabetic)

    @Override
    public void add(CollectableSuspendable collectable) {
        super.add(collectable);
        save();
    }

    @Override
    public boolean contains(CollectableSuspendable collectable) {
        return super.contains(collectable);
    }

    @Override
    public boolean contains(String name) {
        return super.contains(name);
    }

    @Override
    public void remove(CollectableSuspendable collectable) {
        super.remove(collectable);
        save();
    }

    private synchronized void save() {
        final List<String> list = getList();
        if (list.isEmpty()) {
            file.delete();
        } else {
            final File tmpFile = new File(file.getParentFile(),
                                          file.getName() + ".tmp");
            try (final BufferedWriter writer = new BufferedWriter(new FileWriter(tmpFile))) {
                for (String element : list) {
                    writer.write(element);
                    writer.newLine();
                }
                tmpFile.renameTo(file);
            } catch (IOException e) {
                LOG.warn("Could not persist the suspended/resumed state of the Activities");
            }
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
