package gov.lbl.nest.common.suspension;

import java.util.Collection;

import gov.lbl.nest.common.configure.InitializingException;

/**
 * This interface is used to control whether the execution of a collection of
 * elements of an application is suspended or not.
 *
 * @author patton
 */
public interface SuspendableCollection extends
                                       CollectableSuspendable {

    /**
     * Returns the set of {@link Suspendable} instances that are the sets of
     * executions making up the collection of {@link Suspendable} instances.
     *
     * @return the set of {@link Suspendable} instances that are the sets of
     *         executions making up the collection of {@link Suspendable} instances.
     */
    Collection<? extends CollectableSuspendable> getCollectableSuspendables();

    /**
     * Returns true when the named element being controlled by this Object is
     * suspended.
     *
     * @param name
     *            the name of the element whose suspension state should be returned.
     *
     * @return true when the named element being controlled by this Object is
     *         suspended.
     *
     * @throws InitializingException
     *             when this Object are still initializing.
     */
    boolean isSuspended(String name) throws InitializingException;

    /**
     * This method is called then the specified {@link CollectableSuspendable}
     * instance's state has been changed.
     *
     * @param suspendable
     *            the {@link CollectableSuspendable} instance whose state has
     *            changed.
     *
     * @throws InitializingException
     *             when this Object are still initializing.
     */
    void update(CollectableSuspendable suspendable) throws InitializingException;
}
