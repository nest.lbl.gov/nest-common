package gov.lbl.nest.common.suspension;

import gov.lbl.nest.common.configure.InitializingException;

/**
 * This interface is used to control whether the execution of some element of an
 * application is suspended or not.
 *
 * @author patton
 */
public interface Suspendable {

    /**
     * Returns the name of the element being controlled by this Object.
     *
     * @return the name of the element being controlled by this Object.
     */
    String getName();

    /**
     * Returns true when the element being controlled by this Object is suspended.
     *
     * @return true when the element being controlled by this Object is suspended.
     *
     * @throws InitializingException
     *             when this Object are still initializing.
     */
    boolean isSuspended() throws InitializingException;

    /**
     * Sets whether the element being controlled by this Object should suspended or
     * not.
     *
     * @param suspend
     *            True when the element being controlled by this Object should
     *            suspended.
     *
     * @return True if the state of the element being controlled by this Object has
     *         changed.
     *
     * @throws InitializingException
     *             when this Object are still initializing.
     */
    boolean setSuspended(final boolean suspend) throws InitializingException;
}
