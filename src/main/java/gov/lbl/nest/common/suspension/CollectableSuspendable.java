package gov.lbl.nest.common.suspension;

/**
 * This interface is used to control whether the execution of some element of an
 * application, which is part of a large collection of elements, is suspended or
 * not.
 *
 * @author patton
 */
public interface CollectableSuspendable extends
                                        Suspendable {

    /**
     * Sets the {@link SuspendableCollection} instance to which this Object belongs.
     *
     * @param collection
     *            the {@link SuspendableCollection} instance to which this Object
     *            belongs.
     */
    void setCollection(SuspendableCollection collection);
}
