package gov.lbl.nest.common.suspension;

/**
 * This Exception is thrown when a request for an action can not be completed as
 * the associated part of the application is suspended.
 *
 * @author patton
 */
public class SuspendedException extends
                                Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    public SuspendedException() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message describing why this object was thrown.
     */
    public SuspendedException(final String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message describing why this object was thrown.
     * @param cause
     *            the {@link Throwable} that caused this object was thrown.
     */
    public SuspendedException(final String message,
                              final Throwable cause) {
        super(message,
              cause);
    }

    /**
     * Creates an instance of this class.
     *
     * @param cause
     *            the Exception that caused this object was thrown.
     */
    public SuspendedException(final Throwable cause) {
        super(cause);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
