# Welcome to `nest-common` #

## Overview ##

[nest-common](http://nest.lbl.gov.gitlab.io/nest-common/) contains
classes that depend on the JSE API and that are common among the NEST
projects.

Currently this project includes the classes dealing with the following
topics:

*    External command execution: A set of classes that simplify the
execution of a system command from within Java.

*    Digest: A set of classes used to provide a summary of changes to a
set of "Watched" items.

*    Instrumentation: A set of classes used to provide instrumentation
of NEST projects.


Also included in this project as example of Patterns used throughout
NEST projects. These include:

*    Configuration: classes used to manage configurations of
applications.

*    RESTful interface: classes used to support the RESTful interfaces
of NEST projects. 

